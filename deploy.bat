@echo off
set hh=%time:~-11,2%
set /a hh=%hh%+100
set hh=%hh:~1%
set dateseed=%date:~6,4%%date:~3,2%%date:~0,2%.%hh%%time:~3,2%

if not exist \\zoo\share\software\tasktracker\resources goto offline

if not exist \\zoo\share\software\tasktracker\resources\tasks3.dll goto copyfile
move \\zoo\share\software\TaskTracker\Resources\Tasks3.dll \\stingray\software\tasktracker\Old\tasks3.%dateseed%.dll
if errorlevel 1 goto die
echo Archived old to \\zoo\share\software\tasktracker\oldversions\tasks3.%dateseed%.dll

:copyfile
move /y bin\debug\tasks3.exe \\zoo\share\software\tasktracker\resources\tasks3.dll

echo Done!

goto exit

:offline
echo Unable to access zoo\share
goto exit

:die
echo failed

:exit
