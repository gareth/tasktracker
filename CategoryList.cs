using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Tasks3
{
  // No longer used - preserved only to enable upgrades.
  public class CategoryList
  {
    public CategoryList()
    {
      if (mInstance == null)
      {
        mInstance = this;
      }
    }

    public Category this [string xiCode]
    {
      get
      {
        return (Category)mCategories[xiCode];
      }      
    }

    public void Add(Category xiCategory)
    {
      mCategories.Add(xiCategory.Code, xiCategory);
    }

    public Category[] List
    {
      get
      {
        Category[] lRet = new Category[mCategories.Count];
        mCategories.Values.CopyTo(lRet, 0);
        return lRet;
      }
      set
      {
        mCategories.Clear();
        foreach (Category lCat in value)
        {
          mCategories.Add(lCat.Code, lCat);
        }
      }
    }

    public static CategoryList Categories
    {
      get
      {
        if (mInstance != null)
        {
          return mInstance;
        }
        else
        {
          return new CategoryList();
        }
      }
      set
      {
        mInstance = value;
      }
    }

    private static CategoryList mInstance;

    private HybridDictionary mCategories = new HybridDictionary();
  }
}
