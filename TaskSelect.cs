﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace Tasks3
{
  public partial class TaskSelect : PositionPersistingForm
  {
    private const string FILTER_HELP = "Start typing the name of a new or existing task...";

    public TaskSelect(TopForm topForm) : base(false, topForm)
    {
      InitializeComponent();
      this.taskNameBox.TopForm = topForm;

      SortCombo.SelectedIndex = TaskList.IntData.ContainsKey("SortType") ? TaskList.IntData["SortType"] : 0;
      RepopulateTree();
    }

    public void PositionAndShow()
    {
      StartPosition = FormStartPosition.Manual;
      Top = TopForm.Top + 24;
      Left = Math.Min(TopForm.Left + 12, Screen.GetWorkingArea(TopForm).Width - Width);
      RepopulateTree();
      taskNameBox.Text = FILTER_HELP;
      taskNameBox.ForeColor = SystemColors.GrayText;
      
      Show();
      taskNameBox.Focus();
    }

    private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
    {
    }

    private void treeView1_MouseLeave(object sender, EventArgs e)
    {
      //Close();
    }

    private void treeView1_NodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
    {
      
    }

    private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
    {
      
    }

    private void PopulateTreeRecurse(Task xiParent, TreeNodeCollection xiNodeCollection)
    {
      IEnumerable<Task> lTasks = SortCombo.SelectedIndex == 2 ? xiParent.AllChildTasks : xiParent.Children;

      foreach (Task lTask in lTasks.Where(Task => MatchesFilter(Task)).OrderBy(Task => GetSortKey(Task)))
      {
        TreeNode lNode = new TreeNode(SortCombo.SelectedIndex == DESCRIPTION ? lTask.Description : lTask.QualifiedName);
        lNode.Tag = lTask;
        lNode.ImageIndex = lNode.SelectedImageIndex = GetImageIndex(lTask.TopLevelTask.Colour, lTask.Deleted, lTask.Complete, false);

        if (!String.IsNullOrEmpty(lTask.Notes))
        {
          lNode.ToolTipText = lTask.Notes;
        }

        if (lTask.Deleted)
        {
          lNode.ForeColor = Color.Gray;
          lNode.Text += " (Deleted)";
        }
        else if (lTask.Complete)
        {
          lNode.ForeColor = Color.DarkGreen;
          lNode.Text += " (Complete)";
        }

        if (lTask == Timer.ActiveTask)
        {
          lNode.NodeFont = new Font(TasksTree.Font, FontStyle.Bold);
        }
        else
        {
          lNode.NodeFont = null;
        }
             

        xiNodeCollection.Add(lNode);

        if (lTask == Timer.ActiveTask && (TasksTree.SelectedNode == null || TasksTree.SelectedNode.Tag == lTask) && !Config.FocusRecentList)
        {
          TasksTree.SelectedNode = lNode;          
        }
        else if (mDragNode != null && mDragNode.Tag == lTask)
        {
          TasksTree.SelectedNode = lNode;
        }

        if (SortCombo.SelectedIndex != 2)
        {
          PopulateTreeRecurse(lTask, lNode.Nodes);
        }

        if (lTask.ExpandInTreeView)
        {
          lNode.Expand();
        }
        if (lNode.Parent != null && TaskNameFilter.Length > 3 && !lNode.Parent.IsExpanded)
        {
          lNode.Parent.Expand();
        }
      }
    }

    private void TasksTree_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
    {
      TopForm.SetActiveTask((Task)e.Node.Tag);
      Hide();
    }

    private int GetImageIndex(Color xiColor, bool xiDeleted, bool xiComplete, bool xiRecent)
    {
      string lKey = String.Format("{0}.{1}.{2}.{3}.{4}.{5}", xiColor.R, xiColor.G, xiColor.B, xiComplete, xiDeleted, xiRecent);

      if (xiColor == null)
      {
        return 0;
      }
      else if (!mImageMap.ContainsKey(lKey))
      {        
        Bitmap lBitmap = new Bitmap(16, 16);

          using (Graphics lGraphics = Graphics.FromImage(lBitmap))
          {
            Pen lPen = new Pen(Color.Black);

            if (!xiRecent)
            {

              lGraphics.FillEllipse(new SolidBrush(xiColor), 1, 1, 14, 14);
              lGraphics.DrawEllipse(lPen, 1, 1, 14, 14);

              if (xiComplete)
              {
                lGraphics.DrawLine(lPen, 3, 13, 13, 3);
              }
              if (xiDeleted)
              {
                lGraphics.DrawLine(lPen, 3, 3, 12, 12);
              }
            }
            else
            {
              lGraphics.FillEllipse(new SolidBrush(xiColor), 3, 3, 11, 11);
              lGraphics.DrawEllipse(lPen, 3, 3, 11, 11);

              if (xiComplete)
              {
                lGraphics.DrawLine(lPen, 5, 11, 11, 5);
              }
              if (xiDeleted)
              {
                lGraphics.DrawLine(lPen, 5, 5, 12, 12);
              }
            }
          }
        imageList1.Images.Add(lBitmap);
        mImageMap.Add(lKey, imageList1.Images.Count - 1);
      }

      return mImageMap[lKey ];        
    }

    private void TreeItemDrag(object sender, ItemDragEventArgs e)
    {
      mDragNode = (TreeNode)e.Item;

      if (!IsSpecialNode(mDragNode))
      {
        DoDragDrop(e.Item, DragDropEffects.Move);
      }
    }

    // Define the event that occurs while the dragging happens
    private void TreeDragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(typeof(TreeNode)))
      {
        e.Effect = DragDropEffects.Move;
      }
      else
      {
        e.Effect = DragDropEffects.None;
      }
    }

    // Determine what node in the tree we are dropping on to (target),
    // copy the drag source (sourceNode), make the new node and delete
    // the old one.
    private void TreeDragDrop(object sender, DragEventArgs e)
    {
      Point pos = TasksTree.PointToClient(new Point(e.X, e.Y));
      TreeNode targetNode = TasksTree.GetNodeAt(pos);

      Task lMoveTask = (Task)mDragNode.Tag;
      Task lTargetTask = (Task)targetNode.Tag;
      Task lParent = lTargetTask;

      // Scan for loops - you can't set the parent of a task to a the task itself or a child
      while (lParent != null)
      {
        if (lParent == lMoveTask)
        {
          return;
        }
        else
        {
          lParent = lParent.Parent;
        }
      }

      if (SortCombo.SelectedIndex != PRIORITY)
      {
        TreeNode lOldParent = mDragNode.Parent;

        lMoveTask.Parent = lTargetTask;
        if (lOldParent == null)
        {
          RepopulateBranches(targetNode);
          mDragNode.Remove();
        }
        else
        {
          RepopulateBranches(targetNode, lOldParent);
        }
      }
      else
      {                
        Task[] lTasks = TaskList.AllChildTasks.Where(Task => Task.Priority <= lTargetTask.Priority && Task != lMoveTask).OrderBy(Task => -Task.Priority).ToArray();

        if (lTasks.Length > 1 && lTasks[0].Priority - lTasks[1].Priority > 3)
        {
          lMoveTask.Priority = (lTasks[0].Priority + lTasks[1].Priority) / 2;
        }
        else
        {
          lMoveTask.Priority = lTasks[0].Priority - 1000;
          lTasks[0] = lMoveTask;

          for (int ii = 0; ii < lTasks.Length - 1; ++ii)
          {
            if (lTasks[ii].Priority - lTasks[ii + 1].Priority < 1000)
            {
              lTasks[ii + 1].Priority = lTasks[ii].Priority - 1000;
            }
            else
            {
              break;
            }
          }
        }

        RepopulateTree();
      }

      mDragNode = null;
    }

    public void RepopulateTree()
    {
      sendToBottomToolStripMenuItem.Visible =
        sendToTopToolStripMenuItem.Visible =
        SortCombo.SelectedIndex == PRIORITY;

      TasksTree.BeginUpdate();
      // todo - reuse
      TasksTree.Nodes.Clear();
      if (Config.AllowNoTaskAssignment)
      {
          TasksTree.Nodes.Add(new TreeNode("(No task)"));
      }
      mRecentNode = TasksTree.Nodes.Add("Recent");
      PopulateRecent();
      PopulateTreeRecurse(TaskList, TasksTree.Nodes);

      if (TasksTree.TopNode != null && TasksTree.SelectedNode != null &&
        TasksTree.TopNode.Bounds.Top > TasksTree.SelectedNode.Bounds.Top)
      {
        
        TasksTree.TopNode = TasksTree.SelectedNode.Parent ?? TasksTree.SelectedNode;
      }

      TasksTree.EndUpdate();
    }

    private void PopulateRecent()
    {
      mRecentNode.Nodes.Clear();

      foreach (Guid lGuid in TaskList.Recent)
      {
        Task lRecentTask = TaskList.Find(lGuid);

        if (lRecentTask == null || !MatchesTaskNameFilter(lRecentTask))
        {
          continue;
        }
        
        TreeNode lRecent = mRecentNode.Nodes.Add(lRecentTask.QualifiedName);
        lRecent.Tag = lRecentTask;
        lRecent.ImageIndex = GetImageIndex(lRecentTask.Colour, lRecentTask.Deleted, lRecentTask.Complete, true);
        lRecent.SelectedImageIndex = GetImageIndex(lRecentTask.Colour, lRecentTask.Deleted, lRecentTask.Complete, true);


        if (!String.IsNullOrEmpty(lRecentTask.Notes))
        {
          lRecent.ToolTipText = lRecentTask.Notes;
        }

        if (lRecentTask.Deleted)
        {
          lRecent.ForeColor = Color.Gray;
          lRecent.Text += " (Deleted)";
        }
        else if (lRecentTask.Complete)
        {
          lRecent.ForeColor = Color.DarkGreen;
          lRecent.Text += " (Complete)";
        }

        if (lRecentTask == Timer.ActiveTask)
        {
          lRecent.NodeFont = new Font(TasksTree.Font, FontStyle.Bold);
        }
        else
        {
          lRecent.NodeFont = null;
        }

        if (lRecentTask == Timer.ActiveTask && (TasksTree.SelectedNode == null || TasksTree.SelectedNode.Tag == lRecentTask) && Config.FocusRecentList)
        {          
          TasksTree.SelectedNode = lRecent;
          TasksTree.TopNode = mRecentNode;
          lTop = mRecentNode;
        }
      }

      if (mExpanded.ContainsKey(mRecentNode.Text) && mExpanded[mRecentNode.Text])
      {
        mRecentNode.Expand();
      }

      if (mRecentNode.Nodes.Count == 0)
      {
        mRecentNode.Remove();
      }
    }

    private TreeNode lTop;


    private Dictionary<string, bool> mExpanded = new Dictionary<string, bool>();

    private bool IsSpecialNode(TreeNode xiNode)
    {
      return (GetTopNode(xiNode) == mRecentNode);      
    }

    private TreeNode GetTopNode(TreeNode xiNode)
    {
      TreeNode lNode = xiNode;

      while (lNode.Parent != null)
      {
        lNode = lNode.Parent;
      }

      return lNode;
    }

    private TreeNode mRecentNode;

    private void RepopulateBranches(params TreeNode[] xiParents)
    {
      TasksTree.BeginUpdate();

      foreach (TreeNode lNode in xiParents)
      {
        lNode.Nodes.Clear();
        PopulateTreeRecurse((Task)lNode.Tag, lNode.Nodes);
      }

      TasksTree.EndUpdate();
    }

    private Dictionary<string, int> mImageMap = new Dictionary<string, int>();
    private TreeNode mDragNode;

    private void NewTaskButton_Click(object sender, EventArgs e)
    {
      Hide();
      new AddTask(true, TopForm).ShowDialog(TopForm);
    }

    private void UpdateView(object sender, EventArgs e)
    {
      TaskList.IntData["SortType"] = SortCombo.SelectedIndex;
      RepopulateTree();
    }

    private bool MatchesFilter(Task xiTask)
    {
      return
        (ArchivedCheckBox.Checked || !xiTask.Archived || TaskNameFilter.Length > 2)
        && (ShowCompleteCheckbox.Checked || !xiTask.Complete)
        && (ShowDeletedCheckBox.Checked || !xiTask.Deleted)
        && MatchesTaskNameFilter(xiTask);
    }

    private bool MatchesTaskNameFilter(Task xiTask)
    {
      if (!string.IsNullOrEmpty(TaskNameFilter))
      {
        return
          xiTask.AsArray().Concat(xiTask.Ancestors).Concat(xiTask.AllChildTasks)
          .Where(T => T.Matches(taskNameBox.Text)).Any();          
      }
      return true;
    }

    private string TaskNameFilter
    {
      get
      {
        return taskNameBox.Text != FILTER_HELP ? taskNameBox.Text : "";
      }
    }

    private string GetSortKey(Task xiTask)
    {
      switch (SortCombo.SelectedIndex)
      {
        case DESCRIPTION:
          return xiTask.Description;
        case DATEADDED:
          return (DateTime.Today.AddMonths(1).Ticks - xiTask.CreationDate.Ticks).ToString();
        case PRIORITY:
          return (long.MaxValue - xiTask.Priority).ToString("00000000000000000000000000000000000000000000000");
        default:
          throw new Exception();
      }
    }

    const int DESCRIPTION = 0;
    const int DATEADDED = 1;
    const int PRIORITY = 2;

    private void Cancel_Click(object sender, EventArgs e)
    {
      Hide();
      TopForm.RefreshTask();
    }

    private void DetailsMenuItem_Click(object sender, EventArgs e)
    {
      if (mContextMenuTask != null)
      {
        new EditTask(mContextMenuTask, TopForm).ShowDialog();
      }
    }

    private void TasksTree_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Right)
      {
        // Point where the mouse is clicked.
        Point p = new Point(e.X, e.Y);

        // Get the node that the user has clicked.
        TreeNode lNode = TasksTree.GetNodeAt(p);

        if (lNode != null && lNode.Tag != null)
        {
          mContextMenuTask = (Task)lNode.Tag;
          ContextMenu.Show(TasksTree, p);
        }
      }
    }

    private Task mContextMenuTask;

    private void SetActiveMenuItem_Click(object sender, EventArgs e)
    {
      TopForm.SetActiveTask(mContextMenuTask);
      Hide();
    }

    private void SetCompleteMenuItem_Click(object sender, EventArgs e)
    {
      if (mContextMenuTask != null)
      {
        mContextMenuTask.Complete = !mContextMenuTask.Complete;
        RepopulateTree();
      }
    }

    private void DeleteMenuItem_Click(object sender, EventArgs e)
    {
      if (mContextMenuTask != null)
      {
        mContextMenuTask.Deleted = !mContextMenuTask.Deleted;
        RepopulateTree();
      }
    }

    public class CustomTreeView : TreeView
    {
      protected override void DefWndProc(ref Message m)
      {
        if (m.Msg != 0x203)
        {
          base.DefWndProc(ref m);
        }
      }
    }

    private void TasksTree_AfterCollapse(object sender, TreeViewEventArgs e)
    {
      if (IsSpecialNode(e.Node))
      {
        mExpanded[GetTopNode(e.Node).Text] = false;
      }
      else if (e.Node.Tag != null)
      {
        ((Task)e.Node.Tag).ExpandInTreeView = false;
      }
    }

    private void TasksTree_AfterExpand(object sender, TreeViewEventArgs e)
    {
      if (IsSpecialNode(e.Node))
      {
        mExpanded[GetTopNode(e.Node).Text] = true;
      }
      else if (e.Node.Tag != null)
      {
        ((Task)e.Node.Tag).ExpandInTreeView = true;
      }
    }

    private void sendToTopToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (mContextMenuTask != null)
      {
        mContextMenuTask.SendToTop();
        RepopulateTree();
      }
    }

    private void sendToBottomToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (mContextMenuTask != null)
      {
        mContextMenuTask.SendToBottom();
        RepopulateTree();
      }
    }

    private void TasksTree_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == (char)Keys.Enter && this.TasksTree.SelectedNode != null)
      {
        TopForm.SetActiveTask((Task)TasksTree.SelectedNode.Tag);
        Hide();
      }
    }

    private void taskNameBox_TextChanged(object sender, EventArgs e)
    {
      taskNameBox.ForeColor = SystemColors.ControlText;
      RepopulateTree();

      if (string.IsNullOrEmpty(taskNameBox.Text.Trim()))
      {
        BestMatchLabel.Text = "";
      }
    }

    private void taskNameBox_Enter(object sender, EventArgs e)
    {
      if (taskNameBox.Text == FILTER_HELP)
      {
        taskNameBox.Text = "";
      }
    }

    private void taskNameBox_ActionCompleted(bool complete)
    {
      if (complete)
      {
        Hide();
      }
    }

    private void taskNameBox_BestMatchChanged(string obj)
    {
      if (obj == null)
      {
        BestMatchLabel.Text = "No match. Press [enter] to create new task";
      }
      else
      {
        BestMatchLabel.Text = "Press [enter] for '" + obj + "' or [ctrl+enter] for new task";
      }
    }

    private void BestMatchLabel_Click(object sender, EventArgs e)
    {

    }
  }
}
