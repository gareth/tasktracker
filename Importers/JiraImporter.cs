﻿using System;
using System.Net;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Atlassian.Jira;
using Tasks3.Config;

namespace Tasks3
{
  /// <summary>
  /// Summary description for Jira.
  /// </summary>
  public class JiraImporter : Tasks3.ITaskImporter
  {
    private static object lockable = new object();

    private ConfigItem<string> jiraUser;
    private ConfigItem<SecureString> jiraPassword;

    const string STATUS_ACTIVE = "Active";
    const string STATUS_CLOSED = "Closed";

    public TopForm TopForm { get; set; }

    private TimeSpan ErrorImportFrequency = TimeSpan.FromHours(8);
    private DateTime LastFailure = DateTime.MinValue;
    private string OldJiraUser;
    private string OldJiraPassword;

    private IErrorLogger errorLogger;

    public JiraImporter(ConfigItem<string> jiraUser, ConfigItem<SecureString> jiraPassword, IErrorLogger errorLogger)
    {
      this.jiraUser = jiraUser;
      this.jiraPassword = jiraPassword;
      this.errorLogger = errorLogger;
    }

    public Tasks3.Config.Config Config
    {
      get { return TopForm.Config; }
    }

    public bool Enabled
    {
      get
      {
        return Config.EnableJiraIntegration && jiraUser.Value != "";
      }
    }

    public TimeSpan ImportFrequency
    {
      get { return Config.JiraImportFrequency; }
    }

    public void ImportTasks(TaskList taskList)
    {
      System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(ImportTasksInternal), taskList);
    }

    private void ImportTasksInternal(object state)
    {
      var taskList = (TaskList)state;

      lock (lockable)
      {
        if (DateTime.Now < LastFailure + ErrorImportFrequency && configUnchanged())
        {
          return;
        }

        try
        {

          var jira = new Jira("https://jira.softwire.com/jira", jiraUser, (string)jiraPassword.Value);
          jira.MaxIssuesPerRequest = 1000;
          var issues = jira.Issues.Where(i => i.Assignee == jiraUser && i.Status != "Closed" && i.Status != "Resolved");
          var list = issues.ToList();

          ImportTasks(issues, "Jira", taskList);
        }
        catch (Exception e)
        {
          errorLogger.NotifyUserError("Jira import failed. Check your credentials on the options screen.",e);
          LastFailure = DateTime.Now;
          OldJiraUser = jiraUser.Value;
          OldJiraPassword = jiraPassword.Value;
        }
      }
    }

    private bool configUnchanged()
    {
      return jiraUser.Value == OldJiraUser && jiraPassword.Value == OldJiraPassword;
    }

    private WebRequest getRequest(string url)
    {
      var cookies = new CookieContainer();
      WebRequest lRequest = WebRequest.Create(url);
      ((HttpWebRequest)lRequest).CookieContainer = cookies;
      return lRequest;
    }

    // Correct old bug which created many JIRA dupes
    private void removeDupes(string sourceName, TaskList taskList)
    {
      var existingTasks = new List<string>();
      var toDelete = new List<Task>();

      foreach (Task task in taskList.AllChildTasks.Where(Task => Task.Source == sourceName))
      {
        var key = task.Properties["JiraKey"];

        if (existingTasks.Contains(key))
        {
          toDelete.Add(task);
        }
        else
        {
          existingTasks.Add(key);
        }
      }

      foreach (Task task in toDelete)
      {
        task.Parent.Children.Remove(task);
      }
    }

    private bool ImportTasks(IEnumerable<Issue> issues, string sourceName, TaskList taskList)
    {
      bool changed = false;

      removeDupes(sourceName, taskList);

      Task parent = taskList.EnsureParentTask(sourceName);

      ListDictionary existingTasks = new ListDictionary();

      foreach (Task task in taskList.AllChildTasks.Where(Task => Task.Source == sourceName))
      {
        existingTasks[task.Properties["JiraKey"]] = task;
      }

      foreach (Issue issue in issues)
      {
        if (existingTasks.Contains(issue.Key.Value))
        {
          Task oldTask = ((Task)existingTasks[issue.Key.Value]);
          if (oldTask.Complete)
          {
            oldTask.Complete = false;
            TopForm.ShowMessage(false, "Reopened " + oldTask.Description);
            changed = true;
          }
          existingTasks.Remove(issue.Key.Value);
        }
        else
        {
          Task newTask = new Task();
          newTask.Notes = issue.Description;
          newTask.Properties["JiraKey"] = issue.Key.Value;
          newTask.Description = issue.Key + " - " + issue.Summary;
          newTask.Parent = parent;
          newTask.LinkId = "https://jira.softwire.com/jira/browse/" + issue.Key;
          newTask.Source = sourceName;
          newTask.Parent = parent;
          newTask.TimesheetCode = "JIRA:" + issue.Key;
          changed = true;
          TopForm.ShowMessage(false, "Added " + newTask.Description);
        }
      }


      foreach (DictionaryEntry lEntry in existingTasks)
      {
        Task oldTask = (Task)lEntry.Value;

        if (!oldTask.Complete)
        {
          oldTask.Complete = true;
          changed = true;

          if (TopForm.Timer.ActiveTask == oldTask)
          {
            TopForm.SetActiveTask(null);
          }

          TopForm.ShowMessage(false, "Closed " + oldTask.Description);
        }
      }


      return changed;
    }
  }
}
