﻿using System;
using System.Net;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;

namespace Tasks3
{
  /// <summary>
  /// Summary description for Bugzilla.
  /// </summary>
  public class BugzillaImporter : Tasks3.ITaskImporter
  {
    const string ASSIGNED_TO_URL = "https://{0}/buglist.cgi?ctype=rdf&query_format=advanced&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=Awaiting%20Feedback&bug_status=In%20Progress&emailassigned_to1=1&emailtype1=substring&email1={1}&Bugzilla_login={1}&Bugzilla_password={2}";
    const string REVIEWS_URL = "https://{0}/buglist.cgi?ctype=rdf&query_format=advanced&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=Awaiting%20Feedback&bug_status=In%20Progress&field0-0-0=requestees.login_name&type0-0-0=equals&value0-0-0={1}&Bugzilla_login={1}&Bugzilla_password={2}";

    const string BUGS_URL = "https://{0}/show_bug.cgi?ctype=xml&id={1}&excludefield=attachment&excludefield=long_desc&Bugzilla_login={2}&Bugzilla_password={3}";

    const string STATUS_ACTIVE = "Active";
    const string STATUS_CLOSED = "Closed";

    public TopForm TopForm { get; set; }

    public Tasks3.Config.Config Config
    {
      get { return TopForm.Config; }
    }

    public BugzillaImporter()
    {
    }

    public bool Enabled
    {
      get
      {
        return Config.EnableBugzillaIntegration && Config.BugzillaUser != "";
      }
    }

    public TimeSpan ImportFrequency
    {
      get { return Config.BugzillaImportFrequency; }
    }

    public void ImportTasks(TaskList taskList)
    {
      System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(ImportTasksInternal), taskList);
    }

    private void ImportTasksInternal(object state)
    {
      var taskList = (TaskList)state;

      bool lChanged = ImportTasks(
        String.Format(ASSIGNED_TO_URL, "bugzilla.softwire.com", Config.BugzillaUser, Config.BugzillaPassword),
        "Bugs",
        "Bugs",
        "Bug",
        taskList);

      lChanged |= ImportTasks(
        String.Format(REVIEWS_URL, "bugzilla.softwire.com", Config.BugzillaUser, Config.BugzillaPassword),
        "Reviews",
        "Reviews",
        "Review",
        taskList);

      lChanged |= ImportTasks(
        String.Format(ASSIGNED_TO_URL, "support.softwire.com", Config.BugzillaUser, Config.BugzillaPassword),
        "Incidents",
        "Incidents",
        "Incident",
        taskList);

      //checkReviewFlags();
    }


  /*  private static void checkReviewFlags()
    {
      foreach (Task lTask in taskList.AllChildTasks.Where(Task => Task.Source == "Bugs"))
      {
        var id = lTask.Properties["BugId"];

        using (WebResponse lResponse = getRequest(string.Format(BUGS_URL, "bugzilla.zoo.lan/bugzilla", id, Config.BugzillaUser, Config.BugzillaPassword)).GetResponse())
        {
            if (lResponse.ContentLength == 0 || ((HttpWebResponse)lResponse).StatusCode != HttpStatusCode.OK)
            {
              continue;
            }

            XmlTextReader lReader = new XmlTextReader(lResponse.GetResponseStream());
            XmlDocument lDom = new XmlDocument();
            lDom.Load(lReader);


            foreach (XmlNode flagNode in lDom.SelectNodes(@"//flag[@setter='"+Config.BugzillaUser+"']"))
            {
              

        }
      }
      
    }*/

    private WebRequest getRequest(string url)
    {
      var cookies = new CookieContainer();
      WebRequest lRequest = WebRequest.Create(url);
      ((HttpWebRequest)lRequest).CookieContainer = cookies;
      return lRequest;
    }


    private bool ImportTasks(string xiUrl, string xiSourceName, string xiCategory, string xiLabel, TaskList taskList)
    {
      lock (typeof(BugzillaImporter))
      {
        bool lChanged = false;

        try
        {
          Task lParent = taskList.EnsureParentTask(xiCategory);

          //"http://prince.zoo.lan/bugzilla/buglist.cgi?cmdtype=runnamed&namedcmd=Reviews&ctype=rdf&Bugzilla_login=gareth.edwards@softwire.co.uk&Bugzilla_password=jellyfish"

          

          using (WebResponse lResponse = getRequest(xiUrl).GetResponse())
          {
            if (lResponse.ContentLength == 0 || ((HttpWebResponse)lResponse).StatusCode != HttpStatusCode.OK)
            {
              return false;
            }

            XmlTextReader lReader = new XmlTextReader(lResponse.GetResponseStream());
            XmlDocument lDom = new XmlDocument();
            lDom.Load(lReader);

            ListDictionary lExistingTasks = new ListDictionary();

            foreach (Task lTask in taskList.AllChildTasks.Where(Task => Task.Source == xiSourceName))
            {
              lExistingTasks[lTask.Properties["BugId"]] = lTask;
            }

            XmlNamespaceManager lManager = new XmlNamespaceManager(lDom.NameTable);
            lManager.AddNamespace("bz", "http://www.bugzilla.org/rdf#");

            foreach (XmlNode lBugNode in lDom.SelectNodes(@"//bz:bug", lManager))
            {
              string lId = lBugNode.SelectNodes(@"bz:id", lManager)[0].InnerText;

              if (lExistingTasks.Contains(lId))
              {
                Task lOldTask = ((Task)lExistingTasks[lId]);
                //===============================================================
                // Store the completed status in two places so we can distinguish
                // bugs which are reopened, from bugs which are closed by the 
                // user independently of bugzilla
                //===============================================================
                if ((string)lOldTask.Properties["BugzillaStatus"] == STATUS_CLOSED)
                {
                  lOldTask.Properties["BugzillaStatus"] = STATUS_ACTIVE;
                  lOldTask.Complete = false;
                  lChanged = true;
                  lOldTask.SendToTop();
                  TopForm.ShowMessage(false, "Reopened " + lOldTask.Description);
                }
                lExistingTasks.Remove(lId);
              }
              else
              {
                string lDescription;

                if (lBugNode.SelectNodes(@"bz:short_short_desc", lManager).Count > 0)
                {
                    lDescription = lBugNode.SelectNodes(@"bz:short_short_desc", lManager)[0].InnerText;
                }
                else
                {
                    lDescription = lBugNode.SelectNodes(@"bz:short_desc", lManager)[0].InnerText;
                }

                Task lNewTask = new Task();
                lNewTask.Notes = lDescription;
                lNewTask.Properties["BugId"] = lId;
                lNewTask.Properties["BugzillaStatus"] = STATUS_ACTIVE;
                lNewTask.Description = xiLabel + " " + lId + " - " + lDescription;
                lNewTask.Parent = lParent;
                lNewTask.LinkId = lBugNode.Attributes["about", "http://www.w3.org/1999/02/22-rdf-syntax-ns#"].InnerText;
                lNewTask.Source = xiSourceName;
                lNewTask.Parent = lParent;
                lNewTask.SendToTop();
                lChanged = true;
                TopForm.ShowMessage(false, "Added " + lNewTask.Description);
              }
            }


            foreach (DictionaryEntry lEntry in lExistingTasks)
            {
              Task lOldTask = (Task)lEntry.Value;
              lOldTask.Properties["BugzillaStatus"] = STATUS_CLOSED;

              if (!lOldTask.Complete)
              {
                lOldTask.Complete = true;
                lChanged = true;

                if (TopForm.Timer.ActiveTask == lOldTask)
                {
                  TopForm.SetActiveTask(null);
                }

                TopForm.ShowMessage(false, "Closed " + lOldTask.Description);
              }
            }
          }
        }
        catch (Exception e)
        {
          Utils.LogError(e);
        }

        return lChanged;
      }
    }
  }
}
