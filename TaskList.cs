using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.Linq;

namespace Tasks3
{
  public class TaskList : Task
  {
    private const int MAX_RECENT = 25;

    #region Member data

    protected string mFileName;
    protected Task mSelectedTask;
    protected bool mSaveQueued;
    
    private bool mInitialised;
    private List<Guid> mRecent = new List<Guid>();
    private ITaskListPersist backingStore;

    #endregion

    #region Fields

/*    public Point TasksLocation;
    public Rectangle EditTaskLocation;
    public Rectangle EditCategoriesLocation;
    public Rectangle VersionHistoryLocation;
    public Rectangle TimeDetailsLocation;
    public bool TimeDetailsByTime;
    public bool TimeDetailsIncludeUntracked;
    public bool TimeDetailsDecimalTimes;*/
    public string FileFormat;
    public SerializableDictionary<String, Rectangle> LocationData = new SerializableDictionary<string, Rectangle>();
    public SerializableDictionary<String, int> IntData = new SerializableDictionary<string, int>();
    public DateTime LastKnownVersion;

    #endregion

    #region Constructors

    public TaskList()
    {
    }

    public TaskList(Tasks3.Config.Config config, ITaskListPersist backingStore)
    {
      this.config = config;
      this.backingStore = backingStore;
    }

    private Tasks3.Config.Config config;

    #endregion

    protected override Tasks3.Config.Config Config
    {
      get
      {
        return config;
      }
    }

    #region Properties

    protected override bool Initialized
    {
      get { return mInitialised; }
    }

    public List<Guid> Recent
    {
      get { return mRecent; }
      set { mRecent = value; }
    }

    public IEnumerable<Task> RecentTasks
    {
      get
      {
        return Recent.Select(r => Find(r)).Where(t => t != null);
      }
    }

    public override string ToString()
    {
      return "Top-level task";
    }

    public Task SelectedTask
    {
      get { return mSelectedTask; }
      set { mSelectedTask = value; }
    }

    public string FileName
    {
      get { return mFileName; }
      set
      {
        mFileName = value;
        QueueSave();
      }
    }

    public List<DailyData> DailyData
    {
      get { return mDailyData; }
      set { mDailyData = value; }
    }

    private List<DailyData> mDailyData = new List<DailyData>();

    public DailyData GetOrCreateDailyData(DateTime xiDate)
    {
      var lRet = DailyData.Where(d => d.Date.Date == xiDate.Date).FirstOrDefault();

      if (lRet == null)
      {
        lRet = new DailyData() { Date = xiDate.Date };
        DailyData.Add(lRet);
      }

      return lRet;
    }

    // No longer used - preserved only to enable upgrades.
    public CategoryList Categories
    {
      get { return CategoryList.Categories; }
      set { CategoryList.Categories = value; }
    }

    // No longer used - preserved only to enable upgrades.
    public Task[] Tasks
    {
      get;
      set;
    }

    #endregion

    #region Public methods

    public Task EnsureParentTask(string xiParentName)
    {
      Task lRet = FindBySource(xiParentName + "__parent");
      if (lRet == null)
      {
        foreach (Task lTask in Children)
        {
          if (string.Compare(lTask.Description, xiParentName, true) == 0)
          {
            lTask.Source = xiParentName + "__parent";
            return lTask;
          }
        }

        lRet = new Task();
        lRet.Description = xiParentName;
        lRet.Source = xiParentName + "__parent";
        AddChild(lRet);
      }

      return lRet;
    }

    public void AddQuickTask(Task newTask)
    {
      System.Diagnostics.Debug.Assert(newTask.Parent == null);

      var nameToUse = newTask.TimesheetCode ?? newTask.Description;
      Task parent;

      if (nameToUse.Contains(":"))
      {
        var prefix = nameToUse.Split(':'.AsArray())[0];

        parent =
          Children.Where(
            t => !t.Deleted && !t.Complete && t.Description.Equals(prefix, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

        if (parent == null)
        {
          var commonParents =
            AllChildTasks.Where(t => t.TimesheetCode != null && t.TimesheetCode.StartsWith(prefix + ":")).Select(t => t.Parent).
              Distinct();

          if (commonParents.Count() == 1)
          {
            parent = commonParents.First();
          }
        }

        if (parent == null)
        {
          parent = new Task();
          parent.Description = prefix;
          this.AddChild(parent);
        }
      }
      else
      {
        parent = EnsureParentTask("Quick Tasks");
      }

      parent.AddChild(newTask);
    }

    public void AddRecent(Task xiTask)
    {
      if (mRecent.Contains(xiTask.Guid))
      {
        mRecent.Remove(xiTask.Guid);        
      }

      mRecent.Insert(0, xiTask.Guid);

      while (mRecent.Count > MAX_RECENT)
      {
        mRecent.RemoveAt(mRecent.Count - 1);
      }    
    }

    public void Save()
    {
      SaveInternal(null);
    }

    public void QueueSave()
    {
      lock (this)
      {

        if (!mSaveQueued && mInitialised)
        {
          lock (this)
          {
            if (!mSaveQueued)
            {
              mSaveQueued = true;
              System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(SaveInternal));
            }
          }
        }
      }
    }

    public static TaskList LoadFromFile(Tasks3.Config.Config config, TaskList.ITaskListPersist backingStore)
    {
      long lStart = DateTime.Now.Ticks;

      var xiFileName = config.FileLocation;
      TaskList ret;

      try
      {
        ret = backingStore.Load(config);
        ret.backingStore = backingStore;
      }
      catch
      {
        ret = new TaskList(config, backingStore);
      }

      ret.mFileName = xiFileName;

      ret.Upgrade();
      //mInstance.PersistProperties(true);      

      foreach (Task lTask in ret.AllChildTasks)
      {
        if (lTask.Priority >= lStart)
        {
          lTask.Priority = lTask.CreationDate.Ticks;
        }

        foreach (TimePeriod lPeriod in lTask.TimePeriods)
        {
          lPeriod.Task = lTask;
        }
      }

      lock (ret)
      {
        ret.mInitialised = true;
      }

      ret.config = config;

      return ret;
    }

    #endregion

    #region Private helper methods

    protected void Upgrade()
    {
      if (FileFormat == null)
      {
        // Old style format - 2 level hierarchy with Categories at top, tasks below. 
        // Upgrade categories to top level tasks, everything else second level.

        foreach (Category lCat in Categories.List)
        {
          this.AddChild(lCat);

          foreach (Task lTask in Tasks)
          {
            if (lTask.CategoryCodes.Length == 1)
            {
              if (CategoryList.Categories[lTask.CategoryCodes[0]] == lCat)
              {
                lTask.Parent = lCat;
                lTask.InheritColour = true;
              }
            }
            else
            {
              throw new Exception("Only 1 cat per task please!");
            }
          }          
        }

        CategoryList.Categories.List = new Category[0];
        this.Tasks = new Task[0];
        this.FileFormat = "V3.0";
        foreach (Task lTask in this.AllChildTasks)
        {
          lTask.CategoryCodes = new string[0];
          
          if (lTask.TimePeriods.Count > 0)
          {
            lTask.CreationDate = lTask.TimePeriods[0].Start;
          }
        }
      }
    }

    public override void OnChange(bool xiRedraw)
    {
      base.OnChange(xiRedraw);

      QueueSave();
    }



    protected void SaveInternal(object xiNull)
    {
      for (int ii = 0; ii < 2; ++ii)
      {
        try
        {
          ToFile();
          return;
        }
        catch (Exception e)
        {
          Utils.LogError(e);
        }
      }
    }

    protected void ToFile()
    {
      lock (this)
      {
        try
        {
          if (backingStore != null)
          {
            backingStore.Persist(this);
          }         
        }
        finally
        {
          mSaveQueued = false;
        }
      }
    }

    #endregion

    public Task Match(string textToMatch)
    {
      return AllChildTasks.OrderByDescending(t => t.SearchPriority).Where(t => t.Matches(textToMatch)).FirstOrDefault();
    }

    public interface ITaskListPersist
    {
      void Persist(TaskList tasklist);
      TaskList Load(Tasks3.Config.Config config);
    }
  }

}