﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Tasks3
{
  public class Timer : Tasks3.ITimer
  {
    public enum eTimerMessage
    {
    }

    /*private enum eTriggeredEvent
    {
      Reminder,
      AutoSave,
      Pause
    }

    private enum eRecordedEvent
    {
      Activity,
      TaskStart,
      TaskEnd
    }

    public struct TriggeredEvent : IComparable<TriggeredEvent>
    {
      public eTriggeredEventType Type;
      public DateTime Due;
  
      public int CompareTo(TriggeredEvent other)
      {
 	      return Due.CompareTo(other.Due);
      }
    }

    public List
     */

    public Timer(DateTime startTime, TaskList taskList, Tasks3.Config.Config config, ITaskImporter[] importers)
    {
      mLastAutoSave = startTime;
      mLastNoTaskReminder = startTime;
      mLastKeyPress = startTime;
      mActivityStart = startTime;      
      mLastNag = startTime;
      mStartOfDay = startTime;
      this.taskList = taskList;
      this.config = config;
      this.importers = importers;

    }

    private Tasks3.Config.Config config;
    private ITaskImporter[] importers;

    public event ShowMessageHandler ShowMessage;
    public delegate void ShowMessageHandler(bool persist, string xiMessage);

    private void RaiseShowMessage(bool persist, string xiMessage, params object[] xiSubstitutions)
    {
      if (ShowMessage != null)
      {
        ShowMessage(persist, string.Format(xiMessage, xiSubstitutions));
      }    
    }

    public event EventHandler ActiveTaskOrStatusChange;

    private void RaiseActiveTaskOrStatusChange()
    {
      if (ActiveTaskOrStatusChange != null)
      {
        ActiveTaskOrStatusChange(this, EventArgs.Empty);
      }
    }

    public event EventHandler NagDue;

    private void RaiseNagDue()
    {
      if (NagDue != null)
      {
        NagDue(this, EventArgs.Empty);
      }
    }

    //public event 

    public Task ActiveTask
    {
      get { return mActiveTask; }
    }

    public TimeSpan GetElapsedTime(DateTime xiNow)
    {
      return (xiNow - mTimerStart);
    }

    public bool Paused
    {
      get { return mPaused; }
    }

    private bool mPaused;

    public void Pause(DateTime xiNow)
    {
      if (!mPaused && mActiveTask != null)
      {
        mActiveTask.AddTimePeriod(mTimerStart, xiNow);
        mPaused = true;
        RaiseActiveTaskOrStatusChange();
      }
    }

    public void PauseResume(DateTime now)
    {
      if (mActiveTask != null)
      {
        if (!mPaused)
        {
          Pause(now);
        }
        else
        {
          Resume(now);
        }
      }
    }

    private void Resume(DateTime now)
    {
      if (mPaused && mActiveTask != null)
      {
        mTimerStart = now;
        mLastTimeReminder = now;
        OnKeyPress(now);
        mPaused = false;
        RaiseActiveTaskOrStatusChange();
      }
    }

    public void SetActiveTask(Task task, DateTime now)
    {
      lock (this)
      {
        if (task != mActiveTask)
        {
          if (mActiveTask != null && !mPaused)
          {
            var lPeriod = mActiveTask.AddTimePeriod(mTimerStart, now);

            if (lPeriod != null)
            {
              mPreviousPeriod = lPeriod;
            }
          }
          mActiveTask = task;
          mTimerStart = now;
          mLastTimeReminder = now;
          mPaused = false;
        }
        else if (mPaused)
        {
          Resume(now);
        }
        OnKeyPress(now);

      }
    }

    private TimePeriod mPreviousPeriod;

    public TimePeriod GetPreviousPeriod(DateTime xiNow)
    {
      if (mPreviousPeriod == null ||
        mPreviousPeriod.End.Date != xiNow.Date)
      {
        return null;
      }

      return mPreviousPeriod;
    }

    public DateTime? StartOfDay
    {
      get
      {
        return mStartOfDay;
      }
    }

    private DateTime? mStartOfDay;

    public void Tick(DateTime now)
    {
      if (mActiveTask != null && !mPaused)
      {
        mActiveTask.AddTimePeriod(mTimerStart, now);

        if (now - config.ReminderInterval > mLastTimeReminder)
        {
          mLastTimeReminder = now;
          TimeSpan lTime = now - mTimerStart;
          if (lTime.TotalHours > 1)
          {
            RaiseShowMessage(false, "{0} hour(s), {1} minutes on current task", lTime.Hours, lTime.Minutes);
          }
          else
          {
            RaiseShowMessage(false, "{0} minutes on current task", lTime.Minutes);
          }
        }

        if (now - config.AutoPauseInterval > mLastKeyPress)
        {
          mActiveTask.AddTimePeriod(mTimerStart, mLastKeyPress);
          mPaused = true;
          mUserIsActive = false;
          RaiseActiveTaskOrStatusChange();
        }

        mLastNoTaskReminder = now;
      }
      else
      {
        if (now - config.NoTaskReminderInterval > mLastNoTaskReminder)
        {
          mLastNoTaskReminder = now;
          RaiseShowMessage(false, mPaused ? "Task is paused" : "No task selected!");
        }
      }

      if (now - config.AutoSaveInterval > mLastAutoSave)
      {
        taskList.GetOrCreateDailyData(mLastKeyPress.Date).GetOrCreateEvent(eEventType.EndOfDay).Time = mLastKeyPress;
        taskList.QueueSave();
        mLastAutoSave = now;
      }

      if (config.NagInterval > TimeSpan.Zero &&
        now - config.NagInterval > mLastNag &&
        now - config.NagInterval > mTimerStart &&
        now - mActivityStart > TimeSpan.FromSeconds(30) &&
        now - mLastKeyPress < TimeSpan.FromSeconds(60) &&
        now - mLastKeyPress > TimeSpan.FromSeconds(10)
        )
      {
        mLastNag = now;
        RaiseNagDue();
      }

      foreach (ITaskImporter importer in importers)
      {
        if (importer.Enabled && 
          (
            (!lastImportTimes.ContainsKey(importer) && now - mActivityStart > TimeSpan.FromSeconds(5)) ||
            (lastImportTimes.ContainsKey(importer) && now - lastImportTimes[importer] > importer.ImportFrequency)
          )
        )
        {
          importer.ImportTasks(taskList);
          lastImportTimes[importer] = now;
        }
      }

      if (mLastUpdateReminder.AddHours(24) < now && mLastUpdateCheck.AddHours(1) < now)
      {
        mLastUpdateCheck = now;
        ThreadPool.QueueUserWorkItem(new WaitCallback(CheckForUpdates));
      }
    }

    /*  public void OnKeyPress(Kennedy.ManagedHooks.KeyboardEvents kEvent, Keys key)
      {
        OnMouseActivity();
      }
  */
    private bool mInited = false;

    public void OnKeyPress(DateTime xiNow)
    {
      if (!mInited)
      {
        mInited = true;
        if (taskList.GetOrCreateDailyData(xiNow.EffectiveDate()).GetEvent(eEventType.StartOfDay) == null)
        {
          taskList.GetOrCreateDailyData(xiNow.EffectiveDate()).GetOrCreateEvent(eEventType.StartOfDay).Time = xiNow;
        }
      }

      if (mActiveTask == null && xiNow - config.NoTaskReminderInterval > mLastKeyPress)
      {
        RaiseShowMessage(false, mPaused ? "Task is paused" : "No task selected!");
      }

      if (!mUserIsActive)
      {
        mUserIsActive = true;

        if (xiNow.EffectiveDate() != mLastKeyPress.EffectiveDate())
        {
          // todo custom date class to handle all this
          mStartOfDay = xiNow;
          taskList.GetOrCreateDailyData(xiNow.EffectiveDate()).GetOrCreateEvent(eEventType.StartOfDay).Time = xiNow;
          taskList.GetOrCreateDailyData(mLastKeyPress.EffectiveDate()).GetOrCreateEvent(eEventType.EndOfDay).Time = mLastKeyPress;
        }

        mActivityStart = xiNow;
      }

      mLastKeyPress = xiNow;
    }

    private void CheckForUpdates(object xiNow)
    {
      try
      {
        string lVersion = File.ReadAllText(config.VersionFileLocation);
        DateTime lLatest = DateTime.Parse(lVersion.Trim());

        if (lLatest > config.VERSION)
        {
          mLastUpdateReminder = (DateTime)xiNow;
          RaiseShowMessage(true, "New version at " + Path.GetDirectoryName(config.VersionFileLocation));
        }
      }
      catch
      {
      }
    }

    public void ResetLastNag(DateTime xiNow)
    {
      OnKeyPress(xiNow); 
      mLastNag = xiNow;

    }

    public void ReassignRecent(TimeSpan duration, DateTime now)
    {
      var start = now.Subtract(duration);
      var end = now;      
      var tp = mActiveTask.AddTimePeriod(start, end);
      // if reassigning time which is less than that already tracked, we don't change anything
      mTimerStart = tp.Start;

      foreach (Task task in taskList.AllChildTasks.Where(t => t != mActiveTask))
      {
        List<TimePeriod> toDelete = new List<TimePeriod>();

        foreach (TimePeriod period in task.TimePeriods)
        {
          if (period.Start > start)
          {
            toDelete.Add(period);
          }
          else if (period.End > start && period.Start < start)
          {
            period.End = start;
          }
        }

        toDelete.ForEach(TP => TP.Task.TimePeriods.Remove(TP));
      }
    }

    Task mActiveTask;
    DateTime mTimerStart;
    DateTime mLastAutoSave;
    DateTime mLastTimeReminder;
    DateTime mLastNoTaskReminder;
    DateTime mLastKeyPress;
    DateTime mActivityStart;
    bool mUserIsActive = true;
    Dictionary<ITaskImporter, DateTime> lastImportTimes = new Dictionary<ITaskImporter,DateTime>();
    DateTime mLastUpdateCheck = DateTime.MinValue;
    DateTime mLastUpdateReminder = DateTime.MinValue;
    DateTime mLastNag;
    TaskList taskList;
  }
}
