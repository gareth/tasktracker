﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Ninject;
using Tasks3.Config;
using Tasks3.Persistence;
using Tasks3.UI;

namespace Tasks3
{
  public static class Program
  {
    static Mutex mutex = new Mutex(true, "{4009FFE8-B5D3-4789-8800-A45610FFA74A}");
    static TaskList tasks;

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      if (mutex.WaitOne(TimeSpan.Zero, true))
      {
        Application.EnableVisualStyles();
        Application.SetCompatibleTextRenderingDefault(false);

        var config = new Config.Config();
        

        initInjection();

        // Copy old tasks file from previous version
        if (!File.Exists(config.FileLocation) &&
            File.Exists(config.FileLocation.Replace(".tl3", ".tsk")))
        {
          File.Copy(config.FileLocation.Replace(".tl3", ".tsk"), config.FileLocation);
        }

        tasks = TaskList.LoadFromFile(config, new XmlBackingStore());

        var configStore = new Config.ConfigStore();
        var errorLogger = new ErrorLogger();

        var importers = new ITaskImporter[] {
          new BugzillaImporter(),
          new JiraImporter(configStore.get<string>(Config.eConfig.jiraUser), configStore.get<SecureString>(Config.eConfig.jiraPassword), errorLogger)
        };
        var timer = new Timer(DateTime.Now, tasks, config, importers);

        var tsaImporter = get<ITaskProvider>();
        var topForm = new TopForm(config, configStore, timer, importers, tasks, tsaImporter);
        errorLogger.ErrorNotified += (o, e) => topForm.NotifyUserError(e.Message);

        tsaImporter.Init(tasks, errorLogger);

        var screenManager = new ScreenManager(topForm);
        var fullScreen = new FullScreen(topForm, screenManager,configStore.get<bool>(eConfig.showRecentTasks));
        var weekView = new WeekView(topForm);
        
        screenManager.RegisterScreen(fullScreen);
        screenManager.RegisterScreen(weekView);
        screenManager.RegisterPreference(fullScreen, weekView);

        topForm.ScreenManager = screenManager;

        Application.Run(topForm);
        mutex.ReleaseMutex();
      }
    }

    //TODO: ninject adding no value here
    private static void initInjection()
    {
      Kernel = new StandardKernel();
      Kernel.Bind<ITaskProvider>().To<TimeSheetAppConnector>().InSingletonScope();
      Kernel.Bind<Tasks3.Config.Config>().To<Config.Config>().InSingletonScope();
    }

    public static T get<T>()
    {
      return Kernel.Get<T>();
    }

    public static IKernel Kernel { get; private set; }

    public static void Quit()
    {
      tasks.Save();
      Application.Exit();
    }
  }
}
