﻿using System;

namespace Tasks3
{
  public interface IErrorLogger
  {
    void NotifyUserError(string message, Exception e);
    event EventHandler<ErrorEventArgs> ErrorNotified;
  }

  public class ErrorEventArgs : EventArgs
  {
    public string Message { get; private set;}
    public ErrorEventArgs(string message)
    {
      Message = message;
    }
  }

  public class ErrorLogger : IErrorLogger
  {
    #region IErrorLogger Members

    public void NotifyUserError(string message, Exception e)
    {
      if (ErrorNotified != null)
      {
        ErrorNotified(this, new ErrorEventArgs(message + "\r\n" + e.ToString()));
      }
    }

    public event EventHandler<ErrorEventArgs> ErrorNotified;

    #endregion
  }

}
