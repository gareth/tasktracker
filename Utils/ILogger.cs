﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3
{
  public interface ILogger
  {
    void LogEvent(string detail);
  }
}
