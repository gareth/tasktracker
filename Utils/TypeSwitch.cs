﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3
{
  // via. http://stackoverflow.com/questions/298976/c-sharp-is-there-a-better-alternative-than-this-to-switch-on-type
  public static class TypeSwitch
  {
    public class CaseInfo
    {
      public bool IsDefault { get; set; }
      public Type Target { get; set; }
      public object Value { get; set; }
    }

    public static T Do<T>(object source, params CaseInfo[] cases)
    {
      var type = source.GetType();
      foreach (var entry in cases)
      {
        if (entry.IsDefault || entry.Target.IsAssignableFrom(type))
        {
          return (T)entry.Value;
        }
      }

      throw new Exception("no matching type");
    }

    public static CaseInfo Case<T>(object value)
    {
      return new CaseInfo()
               {
                 Value = value,
                 Target = typeof(T)
               };
    }
  }
}
