﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3
{
  public class AutoComplete
  {
    private TaskList list;
    private ITaskProvider tsaConnector;

    public AutoComplete(TaskList list, ITaskProvider tsaConnector)
    {
      this.list = list;
      this.tsaConnector = tsaConnector;      
    }

    public void Clear()
    {
      MatchedTask = null;
    }

    public string Match(string substring)
    {
      Clear();

      foreach (Task lTask in list.AllChildTasks.OrderByDescending(t => t.TimePeriods.Any() ? t.TimePeriods.Max(p => p.End) : DateTime.MinValue))
      {
        if (!lTask.Deleted && 
          (lTask.Description.ToLower().StartsWith(substring.ToLower()) || (lTask.TimesheetCode ?? "").ToLower().StartsWith(substring.ToLower())))
        {
          MatchedTask = lTask;
          return (lTask.TimesheetCode == null || substring.Length > lTask.TimesheetCodePrefix.Length) ? lTask.Description : lTask.TimesheetCodePrefix;
        }
      }

      return null;
    }

    public Task MatchedTask { get; private set; }
  }
}
