﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Tasks3.Config;

namespace Tasks3
{
  public class AutoCompleteTextBox : TextBox
  {
    public TopForm TopForm { get; set; }

    public Tasks3.Config.Config Config
    {
      get { return TopForm.Config; }
    }

    public TaskList TaskList
    {
      get { return TopForm.TaskList; }
    }

    //private Task mAutoCompleteTask;
    //private string mCode;
    private bool mInHandler;
    private string mPrevious;

    public AutoCompleteTextBox()
    {
      KeyDown += this_KeyDown;
      TextChanged += this_TextChanged;
    }

    private void this_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        if (this.Text.Trim() != "")
        {

          if (bestMatch != null && ! e.Control)
          {
            TopForm.SetActiveTask(bestMatch);
          }
          else
          {

            Task lNewTask = new Task();
            lNewTask.Description = this.Text.Trim();
            TaskList.AddQuickTask(lNewTask);
            TopForm.SetActiveTask(lNewTask);
          }
            
            if (ActionCompleted != null)
            {
              ActionCompleted(true);
            }
            /*
          if (mAutoCompleteTask != null &&
            (this.Text.Trim().ToLower() == mAutoCompleteTask.Description.ToLower()))
          {
            TopForm.SetActiveTask(mAutoCompleteTask);
            if (ActionCompleted != null)
            {
              ActionCompleted(true);
            }
          }
          else
          {

            Task lNewTask = new Task();
            lNewTask.Description = mCode ?? this.Text.Trim();
            lNewTask.TimesheetCode = mCode;
            TaskList.AddQuickTask(lNewTask);
            TopForm.SetActiveTask(lNewTask);

            if (ActionCompleted != null)
            {
              ActionCompleted(true);
            }
          }*/
        }

        if (ActionCompleted != null)
        {
          ActionCompleted(false);
        }

      }
      else if (e.KeyCode == Keys.Escape)
      {
        if (ActionCompleted != null)
        {
          ActionCompleted(false);
        }

      }
    }

    private void this_TextChanged(object sender, EventArgs e)
    {

      if (!mInHandler && this.Text != "" && this.TopForm != null)
      {
        mInHandler = true;
        string lOld = this.Text;
        //mCode = null;

        try
        {
            // Rules:
            //   - live tasks (order by last activity desc) > TSA tasks (order by project activity desc?) > dead tasks (activity desc)
            //   - spaces separate search terms, instr match
            //   - min x chars to autocomplete? or initial substring if < x
            //   - enter uses best suggestion, ctrl+enter creates new
            //   - if the start of your task name matches exactly a whole TSA task, then assign that as the code
            //   - down arrow moves to list
            //   - exact match beats whitespace-seperated components match

            // qq maintain search index? 
            // ordered 



          if ((mPrevious == null || mPrevious.Length < this.Text.Length))
          {
            var match = TaskList.Match(this.Text);

              if (match != bestMatch)
              {
                  bestMatch = match;

                  if (BestMatchChanged != null)
                  {
                      BestMatchChanged(match == null ? null : match.QualifiedName);
                  }
              }


            /*  if (Program.get<ITaskProvider>().GetBestMatch(this.Text) != null)
              {
                  this.Text += Program.get<ITaskProvider>().GetBestMatch(this.Text).Substring(this.Text.Length);
                  this.SelectionStart = lOld.Length;
                  this.SelectionLength = this.Text.Length - lOld.Length;

                  mCode = this.Text;

                  return;
              }

            foreach (Task lTask in TaskList.AllChildTasks)
            {
              if (!lTask.Deleted && lTask.Description.ToLower().StartsWith(this.Text.ToLower()))
              {
                this.Text += lTask.Description.Substring(this.Text.Length);
                this.SelectionStart = lOld.Length;
                this.SelectionLength = this.Text.Length - lOld.Length;

                mAutoCompleteTask = lTask;
                return;
              }
            }*/


          }
        }
        finally
        {
          mPrevious = lOld;
          mInHandler = false;
        }
      }
    }


    private Task bestMatch;

    public event Action<bool> ActionCompleted;
    public event Action<string> BestMatchChanged;
  }
}
