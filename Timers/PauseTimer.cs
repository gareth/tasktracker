﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Timers
{
  public class PauseTimer
  {
    private TimeSpan timeout;
    private DateTime lastActivity;

    public PauseTimer(DateTime now, TimeSpan timeout)
    {
      this.lastActivity = now;
      this.timeout = timeout;
    }

    //todo return suggestion for next tick
    public void tick(DateTime now)
    {
      if (now > lastActivity + timeout)
      {
        RaisePauseDue();
      }
    }

    private void RaisePauseDue()
    {
      if (PauseDue != null)
      {
        PauseDue(this, EventArgs.Empty);
      }
    }

    public event EventHandler PauseDue;
  }
}
