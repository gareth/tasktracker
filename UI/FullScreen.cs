﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tasks3.Config;
using Tasks3.Data;
using Tasks3.UI;

namespace Tasks3
{
  public partial class FullScreen : TasksForm, IFullScreenUI
  {
    private Task lastMatch;
    private IDictionary<Screen,BlankScreen> secondary = new Dictionary<Screen,BlankScreen>();
    private ScreenManager screenManager;
    private readonly ConfigItem<bool> showRecent;

    public FullScreen(TopForm topForm, ScreenManager screenManager, ConfigItem<bool> showRecent) : base(topForm)
    {
      InitializeComponent();
      textBox1.Focus();

      this.Visible = false;
      this.screenManager = screenManager;
      this.showRecent = showRecent;
    }

 
    protected override void OnActivated(EventArgs e)
    {
      base.OnActivated(e);
      textBox1.Focus();
    }

    protected override void OnVisibleChanged(EventArgs e)
    {
      if (this.Visible)
      {
        populateRecent();
        populateTimeline();
      }


      base.OnVisibleChanged(e);
      TopForm.Visible = !this.Visible;

      if (this.Visible)
      {
        this.Activate();
      }
      else
      {
        RecentTasksGroupBox.Controls.Clear();
      }

      textBox1.Clear();
    }

    private void textBox1_TextChanged(object sender, EventArgs e)
    {
      Task match = TopForm.TaskList.Match(TaskNameFilter);
      lastMatch = null;
      if (string.IsNullOrWhiteSpace(TaskNameFilter))
      {
        matchingTaskButton.Visible = false;
      }
      else if (match != null)
      {
        matchingTaskButton.Text = match.QualifiedName;
        matchingTaskButton.BackColor = match.Colour;
        matchingTaskButton.Visible = true;
        lastMatch = match;
      }
      else 
      {
        matchingTaskButton.Text = TaskNameFilter;
        matchingTaskButton.BackColor = SystemColors.Control;
        matchingTaskButton.Visible = true;
      }
        

      matchingTaskButton.ForeColor = matchingTaskButton.BackColor.ComplementaryTextColor();

      updateMidPanel();
    }

    private string TaskNameFilter
    {
      get { return textBox1.Text; }
    }

    private string lastFilter;
    private bool canFilterInPlace;

    private void populateMidButtons()
    {
      midPanel.SuspendLayout();
      try
      {
        midPanel.Controls.Clear();

        Button prev = null;
        var font = new Font("Segoe UI", 9F, FontStyle.Regular);

        while (true)
        {
            var button = new Button();
            button.Click += new EventHandler(button_Click);
            button.ContextMenuStrip = contextMenu;

            if (prev != null)
            {
              button.Top = prev.Bottom + 8;
              button.Left = prev.Left;
            }
            else
            {
              button.Top = 32;
              button.Left = 10;
            }

            prev = button;

            button.Width = 232;
            button.Height = 24;
            button.Font = font;
            button.FlatStyle = FlatStyle.Flat;            


            if (button.Bottom > midPanel.Height)
            {
              button.Left = prev.Right + 16;
              button.Top = 32;

              if (button.Right > midPanel.Width)
              {
                return;
              }
            }

            midPanel.Controls.Add(button);
        } 
      } 
      finally
      {
        midPanel.ResumeLayout();
      }
    }

    private void updateMidPanel()
    {
      if (midPanel.Controls.Count == 0)
      {
        populateMidButtons();
      }

      try
      {
        midPanel.SuspendLayout();

        var filter = TaskNameFilter;
        if (lastFilter != null && filter.StartsWith(lastFilter) && lastFilter.Length > 2 && canFilterInPlace)
        {
          foreach (Button b in midPanel.Controls)
          {
            var t = b.Tag as Task;
            b.Visible = t != null && t.Matches(filter);
          }

          return;
        }
        else
        {
          var tasks = TaskList.AllChildTasks.Where(Task => MatchesFilter(Task, filter)).OrderBy(Task => Task.QualifiedName).GetEnumerator();
          canFilterInPlace = false;

          foreach (Button button in midPanel.Controls)
          {
            if (tasks.MoveNext())
            {
              button.FormatFromTask(tasks.Current, toolTip);
              button.Show();
            }
            else
            {
              button.Hide();
              canFilterInPlace = true;
            }
          }
        }

        lastFilter = filter;

      }
      finally
      {
        midPanel.ResumeLayout();
      }
    }
    

    private bool MatchesFilter(Task xiTask, string filter)
    {
      return        
        
        xiTask.Matches(filter)
        // qq really we don't want to include archived unless the specificity is high in terms of result count
        && ((!xiTask.Archived && !xiTask.Complete && !xiTask.Deleted) || filter.Length > 2);
    }


    private void matchingTaskButton_Click(object sender, EventArgs e)
    {
      doSubmit(false);
    }

    private void setTask(Task task)
    {
      TopForm.SetActiveTask(task);
      Hide();
    }

    private void onTextboxKeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        doSubmit((int)(e.Modifiers & Keys.Control) != 0);
      }
      else if (e.KeyCode == Keys.Escape && Config.AllowNoTaskAssignment)
      {
        Hide();
      }
    }

    private void doSubmit(bool forceNew)
    {
      if (lastMatch != null && !forceNew)
      {
        setTask(lastMatch);
      }
      else if (TaskNameFilter.Trim() != "")
      {
        Task lNewTask = new Task();
        lNewTask.Description = TaskNameFilter.Trim();
        TaskList.AddQuickTask(lNewTask);
        setTask(lNewTask);
      }
    }

    public void populateRecent()
    {
      RecentTasksGroupBox.Visible = showRecent;

      if (!showRecent)
      {
        return;
      }

      RecentTasksGroupBox.Controls.Clear();
      Control prev = null;
      foreach (Task t in TaskList.RecentTasks)
      {
        var button = new Button();
        button.Font = new Font("Segoe UI", 9F, FontStyle.Regular);
        button.FormatFromTask(t, toolTip);
        button.Click += new EventHandler(button_Click);
        button.ContextMenuStrip = contextMenu;
        RecentTasksGroupBox.Controls.Add(button);

        if (prev != null)
        {
          button.Top = prev.Bottom + 8;
        }
        else
        {
          button.Top = 32;
        }
        prev = button;
        button.Left = 10;
        button.Width = 232;
        button.Height = 24;

        button.FlatStyle = FlatStyle.Flat;

        if (button.Bottom > RecentTasksGroupBox.Height)
        {
          button.Visible = false;
          return;
        }
      }
    }

    private void populateTimeline()
    {
      if (Screen.AllScreens.Count() == 1)
      {
        RightPanel.Visible = true;
        midPanel.Width = RightPanel.Left - midPanel.Left;

        TimeLine tl = new TimeLine(TaskList, DateTime.Now, Config.MinCountablePeriod);
        timeLinePanel.TimeLine = tl;
        PeriodData pd = new PeriodData(TaskList);
        topTasksPanel1.data = pd;
      }
      else
      {
        RightPanel.Visible = false;
        midPanel.Width = RightPanel.Right - midPanel.Left;
      }
    }

    void button_Click(object sender, EventArgs e)
    {
      setTask((Task)((Button)sender).Tag);
    }

    private void label1_Click(object sender, EventArgs e)
    {

    }

    private void label2_Click(object sender, EventArgs e)
    {

    }

    private void label3_Click(object sender, EventArgs e)
    {

    }

    private void label4_Click(object sender, EventArgs e)
    {

    }

    private void matchingTaskButton_VisibleChanged(object sender, EventArgs e)
    {
      BestMatchHint.Visible = BestMatchLabel.Visible = matchingTaskButton.Visible;
    }

    private void label2_Click_1(object sender, EventArgs e)
    {

    }

    private void moreTimelinesButton_Click(object sender, EventArgs e)
    {
      screenManager.Show<WeekView>();
    }

    private void contextMenu_Opening(object sender, CancelEventArgs e)
    {
      Task task = (Task)contextMenu.SourceControl.Tag;
      completeToolStripMenuItem.Checked = task.Complete;
      deletedToolStripMenuItem.Checked = task.Deleted;
    }

    private void detailsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var task = (Task)contextMenu.SourceControl.Tag;
      var dialog = new EditTask(task, TopForm);
      dialog.TopMost = true;
      dialog.ShowDialog();
    }

    private void completeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var task = (Task)contextMenu.SourceControl.Tag;
      task.Complete = !task.Complete;
      ((Button)contextMenu.SourceControl).FormatFromTask(task, toolTip);
    }

    private void deletedToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var task = (Task)contextMenu.SourceControl.Tag;
      task.Deleted = !task.Deleted;
      ((Button)contextMenu.SourceControl).FormatFromTask(task, toolTip);
    }
  }
}
