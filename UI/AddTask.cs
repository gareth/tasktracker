﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3
{
  public partial class AddTask : TasksForm
  {

    public AddTask(bool xiSetNewTaskActive, TopForm topForm) :base(topForm)
    {
      mSetNewTaskActive = xiSetNewTaskActive;

      InitializeComponent();
      this.Icon = global::Tasks3.Properties.Resources.SitesHigh;
      ParentCombo.KeyPress += new KeyPressEventHandler(Utils.OnAutoCompleteKeyPress);

      ParentCombo.Items.Add(TaskList);

      foreach (Task lTask in TaskList.Children.OrderBy(Task => Task.Description))
      {
        ParentCombo.Items.Add(lTask);
      }
    }

    private void OKButton_Click(object sender, EventArgs e)
    {
      if (!String.IsNullOrEmpty(DescriptionTextBox.Text))
      {

        Task lNewTask = new Task();
        lNewTask.Description = DescriptionTextBox.Text;

        if (ParentCombo.SelectedItem != null)
        {
          ((Task)ParentCombo.SelectedItem).AddChild(lNewTask);
        }
        else if (!String.IsNullOrEmpty(ParentCombo.Text))
        {
          if (TaskList.AllChildTasks.Where(T => string.Compare(T.Description, ParentCombo.Text) == 0).Count() == 1)
          {
            Task lParent = TaskList.AllChildTasks.Where(T => string.Compare(T.Description, ParentCombo.Text) == 0).First();
            lParent.AddChild(lNewTask);
          }
          else
          {
            Task lNewParent = new Task();
            lNewParent.Description = ParentCombo.Text;
            TaskList.AddChild(lNewParent);
            lNewParent.AddChild(lNewTask);
          }
        }
        else
        {
          TaskList.AddChild(lNewTask);
        }

        if (Timer.ActiveTask == null || mSetNewTaskActive)
        {
          TopForm.SetActiveTask(lNewTask);
        }
      }

      Close();
    }

    private void CancelButton_Click(object sender, EventArgs e)
    {
      Close();
    }

    private bool mSetNewTaskActive;

    private void button1_Click(object sender, EventArgs e)
    {
      mSetNewTaskActive = true;
      OKButton_Click(sender, e);
    }
  }
}
