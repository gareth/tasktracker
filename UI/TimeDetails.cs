﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

using AdvancedDataGridView;

namespace Tasks3
{
  public partial class TimeDetails : PositionPersistingForm
  {
    private int mTaskSplitterPosition;
    private int mTimeSplitterPosition;
    private bool mUpdating;

    public TimeDetails(TopForm topForm) : base(topForm)
    {
      mUpdating = true;

      InitializeComponent();

      mTaskSplitterPosition = TaskList.IntData.ContainsKey("TaskSplitterPosition") ? TaskList.IntData["TaskSplitterPosition"] : 768;
      mTimeSplitterPosition = TaskList.IntData.ContainsKey("TimeSplitterPosition") ? TaskList.IntData["TimeSplitterPosition"] : 468;
      ViewByTimeRadio.Checked = TaskList.IntData.ContainsKey("ViewByTime") && TaskList.IntData["ViewByTime"] != 0;

      FromDatePicker.Value = Utils.GetStartOfWeek();
      ToDatePicker.Value = Utils.GetStartOfWeek().AddDays(7);

      mUpdating = false;
    }

    private Task.ChangeDelegate changeDelegate;

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      changeDelegate = new Task.ChangeDelegate(TaskList_Change);
      TopForm.TaskList.Change += changeDelegate;
      RefreshData();
    }

    protected override void  OnClosing(CancelEventArgs e)
    {
      TopForm.TaskList.Change -= changeDelegate;
 	    base.OnClosing(e);
    }

    void TaskList_Change(Task sender, bool xiRedraw)
    {
      RefreshData();
    }

    private void PrevWeekButton_Click(object sender, EventArgs e)
    {
      FromDatePicker.Value = FromDatePicker.Value.AddDays(-7);
      ToDatePicker.Value = ToDatePicker.Value.AddDays(-7);
    }

    private void FromDatePicker_ValueChanged(object sender, EventArgs e)
    {
      RefreshData();
    }

    private void ToDatePicker_ValueChanged(object sender, EventArgs e)
    {
      RefreshData();
    }

    private void RefreshData()
    {
      if (mUpdating)
      {
        return;
      }


      mUpdating = true;

      int lSplitterDistance;

      TasksTreeView.Nodes.Clear();

      if (this.ViewByTaskRadio.Checked)
      {
        TasksTreeView.Columns[0].HeaderText = "Task";
        TimePeriodsView.Columns[0].HeaderText = "Date";

        RefreshDataRecurse(TasksTreeView.Nodes, TaskList);
        lSplitterDistance = mTaskSplitterPosition;
      }
      else
      {
        TasksTreeView.Columns[0].HeaderText = "Date";
        TimePeriodsView.Columns[0].HeaderText = "Task";

        for (DateTime lDate = FromDatePicker.Value.Date; lDate < ToDatePicker.Value.Date; lDate = lDate.AddDays(1))
        {
          TimeSpan lDayTime = TimeSpan.FromSeconds(
            TaskList.AllChildTasks.Sum(Task => Task.TimePeriods.Sum(TimePeriod => TimePeriod.GetDuration(lDate, lDate.AddDays(1)).TotalSeconds)));

          var lChildNode = TasksTreeView.Nodes.Add(lDate.ToString("dddd dd MMMM yy"), "", lDayTime.ToStringExcludeZero(false), "");
          lChildNode.Tag = lDate;

          if (lDate.Date == DateTime.Today)
          {
            TasksTreeView.ClearSelection();
            lChildNode.Cells[0].Selected = true;
          }
        }
        lSplitterDistance = mTimeSplitterPosition;
      }

      TaskList.IntData["ViewByTime"] = ViewByTimeRadio.Checked ? 1 : 0;

      DateTime lFrom = FromDatePicker.Value;
      DateTime lTo = ToDatePicker.Value;

      TimeSpan lTotalTime = TimeSpan.FromSeconds(
        TaskList.AllChildTasks.Sum(Task => Task.TimePeriods.Sum(TimePeriod => TimePeriod.GetDuration(lFrom, lTo).TotalSeconds)));

      TasksTreeView.Nodes.Add("Total", "-", (lTotalTime).ToStringExcludeZero(false));

      

      /*TasksTreeView.AutoResizeColumns();
      TimePeriodsView.AutoResizeColumns();*/

      RefreshTimePeriodData();
      Splitter.SplitterDistance = lSplitterDistance;

      // todo: boldify

      mUpdating = false;

    }

    private void RefreshDataRecurse(TreeGridNodeCollection xiNodeCollection, Task xiParentTask)
    {
      DateTime lFrom = FromDatePicker.Value;
      DateTime lTo = ToDatePicker.Value;

      foreach (Task lChildTask in xiParentTask.Children.OrderBy(Task => Task.Description))
      {
        TimeSpan lThisTaskTime = TimeSpan.FromSeconds(
          lChildTask.TimePeriods.Sum(TimePeriod => TimePeriod.GetDuration(lFrom, lTo).TotalSeconds));
        TimeSpan lChildTaskTime = TimeSpan.FromSeconds(
          lChildTask.AllChildTasks.Sum(Task => Task.TimePeriods.Sum(TimePeriod => TimePeriod.GetDuration(lFrom, lTo).TotalSeconds)));

        if (lThisTaskTime > TimeSpan.Zero || lChildTaskTime > TimeSpan.Zero)
        {
          var lChildNode = xiNodeCollection.Add(lChildTask.Description, lChildTask.TimesheetCode, lThisTaskTime.ToStringExcludeZero(false), (lThisTaskTime + lChildTaskTime).ToStringExcludeZero(false));
          lChildNode.Tag = lChildTask;

          RefreshDataRecurse(lChildNode.Nodes, lChildTask);
        }
      }
    }

    private void RefreshTimePeriodData()
    {
      DateTime lFrom = FromDatePicker.Value;
      DateTime lTo = ToDatePicker.Value;

      TimePeriodsView.Rows.Clear();

      if (TasksTreeView.CurrentRow != null && TasksTreeView.CurrentRow.Tag != null)
      {
        if (TasksTreeView.CurrentRow.Tag is Task)
        {
          foreach (TimePeriod lPeriod in ((Task)TasksTreeView.CurrentRow.Tag).TimePeriods)
          {
            if (lPeriod.GetDuration(lFrom, lTo) > TimeSpan.Zero)
            {
              TimePeriodsView.Rows.Add(lPeriod.Start.ToString("dddd dd MMMM yy"), lPeriod.Start.ToString("HH:mm"), lPeriod.End.ToString("HH:mm"), lPeriod.Duration.ToStringExcludeZero(false));
            }
          }
        }
        else
        {
          foreach (var lTaskAndTime in TaskList.AllChildTasks.SelectMany(Task => Task.TimePeriods, (Task, TimePeriod) => new { Task, TimePeriod }).Where(TaskAndTime => TaskAndTime.TimePeriod.Start.Date == (DateTime)TasksTreeView.CurrentRow.Tag).OrderBy(TaskAndTime => TaskAndTime.TimePeriod.Start))
          {
            TimePeriodsView.Rows.Add(lTaskAndTime.Task.Description, lTaskAndTime.TimePeriod.Start.ToString("HH:mm"), lTaskAndTime.TimePeriod.End.ToString("HH:mm"), lTaskAndTime.TimePeriod.Duration.ToStringExcludeZero(false));
          }


        }
      }

      //TimePeriodsView.AutoResizeColumns();
    }

    private void TasksTreeView_CurrentCellChanged(object sender, EventArgs e)
    {
      RefreshTimePeriodData();
    }

    private void NextWeekButton_Click(object sender, EventArgs e)
    {
      FromDatePicker.Value = FromDatePicker.Value.AddDays(7);
      ToDatePicker.Value = ToDatePicker.Value.AddDays(7);
    }

    private void ViewByTaskRadio_CheckedChanged(object sender, EventArgs e)
    {
      RefreshData();
    }

    private void CloseButton_Click(object sender, EventArgs e)
    {
      Close();      
    }

    private void Splitter_SplitterMoved(object sender, SplitterEventArgs e)
    {
      if (mUpdating)
      {
        return;
      }

      if (this.ViewByTaskRadio.Checked)
      {
        mTaskSplitterPosition = TaskList.IntData["TaskSplitterPosition"] = e.SplitX;
      }
      else
      {
        mTimeSplitterPosition = TaskList.IntData["TimeSplitterPosition"] = e.SplitX;
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (TasksTreeView.CurrentRow != null &&
        TasksTreeView.CurrentRow.Tag != null &&
        TasksTreeView.CurrentRow.Tag is DateTime)
      {
        new EditTimeline((DateTime)TasksTreeView.CurrentRow.Tag, Config, TaskList).Show();
      }
      else
      {
        new EditTimeline(FromDatePicker.Value, Config, TaskList).Show();
      }
    }

    private void TasksTreeView_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex == 1 && this.ViewByTaskRadio.Checked)
      {
        TasksTreeView.BeginEdit(true);
      }
    }

    private void TasksTreeView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
      

    }

    private void TasksTreeView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
    {
      ((TextBox)TasksTreeView.EditingControl).TextChanged += new EventHandler(TimeDetails_TextChanged);
    }

    private bool mInHandler;
    private string mPrevious;

    void TimeDetails_TextChanged(object sender, EventArgs e)
    {
      if (mInHandler)
        return;

      mInHandler = true;

      try
      {

        TextBox lBox = (TextBox)TasksTreeView.EditingControl;
        string lOld = lBox.Text ?? "";

        if ((mPrevious == null || mPrevious.Length < lOld.Length))
        {
          lBox.Text = Program.get<ITaskProvider>().GetBestMatch(lOld) ?? lOld;
          lBox.SelectionStart = lOld.Length;
          lBox.SelectionLength = lBox.Text.Length - lOld.Length;
        }

        mPrevious = lOld;
      }
      finally
      {
        mInHandler = false;
      }
    }

    private void TasksTreeView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
    {
      Task lTask = TasksTreeView.CurrentRow.Tag as Task;

      if (lTask != null)
      {
        lTask.TimesheetCode = (string)TasksTreeView.CurrentRow.Cells[1].Value;
      }
    }

    private void TasksTreeView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
    {
      ((TextBox)TasksTreeView.EditingControl).TextChanged -= new EventHandler(TimeDetails_TextChanged);
      ((TextBox)TasksTreeView.EditingControl).TextChanged += new EventHandler(TimeDetails_TextChanged);
    }

    private void ExportButton_Click(object sender, EventArgs e)
    {
      Program.get<ITaskProvider>().SubmitTaskData(FromDatePicker.Value.Date); 
    }

  }
}
