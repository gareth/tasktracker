using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using Tasks3.Config;
using System.Text;
using System.Text.RegularExpressions;

namespace Tasks3
{
	/// <summary>
	/// Summary description for VersionHistory.
	/// </summary>
	public class VersionHistory : PositionPersistingForm
	{
    private System.Windows.Forms.RichTextBox richTextBox1;
    private System.Windows.Forms.Button button1;
    private Label TimeStampLabel;
    private Panel panel1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
    

    private static DateTime GetTimeStamp()
    {
		  var location = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.Contains("Tasks3")).First().Location;
      return File.GetLastWriteTime(location);
    }

    private static string revisionLog = @"
4.11.2
[improvement] Fix TSA integration
4.10
c5bac8ade3760c69363423a103b28936892e9824 [improvement] Option to hide recent tasks list entirely for the seriously willpower-deficient
7adea7d3c3c5a8e7c26338f5e78bcc3900a6fdd2 [bug] Fix another issue where time was tracked while paused
4.9
66ea7c30716119644f009cafa505dc40ab680ab1 [bug] Correct tab order on details window
b7fe1f44203835bfd6ec14443b1592630c143cee [bug] Stop incorrectly recording time after auto-pause, even when apparently paused
ff3a79f3b469cab05f5ba40dc465bec65b91ca05 [bug] Mismatched task name/code could break TSA import. Update code to match name in future
4.8
71b6d4acc24a5df8b5704edca51733e1223ac864 [bug] Adding time periods in the past sometimes overwrote the most recent time period for the same task
3199512c1d0887399befe86da3db29551b4cec8c [bug] Always unpause when selecting a task (including the same task as before)
4.7
e66e541038a3934197c37c2ba5c58d81109bca22 [bugfix] Make edit task window appear on top of full screen window not behind
4.6
37770769a7f51705adc3c3a2c369f24e20f80e88 [bug] fix display of overlay on 3+ monitors
4.5
a15345e49531f03ac6218a22c98431e99c3174d3 [bug] Speculative fix for screen-arranging race condition
a17246ab97415ca5efce7b7d67c8dde50ebf69ca [improvement] Daily totals on week view
363f952e130c497b1eb5ddecb7ad75ee6e10a549 [bug] Ensure task box has focus in full screen view
2f37c7833c9ed7bf655f4c471756880540463c6b [bug] Clean up tasks marked as TSA but without codes
5d454a5023f3ca8e7d25093f01dc53d7661acdac [bug] don't delete parent TSA tasks when the project is closed and children have tracked time
98d2516b4b37079d490d1ab532138e77641dc966 [improvement] add context menu to task select
[improvement] render completed/deleted tasks distinctly
8c3dff50e2e197e089c98862cd1b10897c688bbb [bugfix] mark complete defunct TSA tasks with tracked time, rather than deleting
99b9aebe9815b6adefd5fe3c9bbbf3d06e5a3650 [improvement] remove tasks from now-closed TSA projects
2bd251a21c194c1ca19356370fafb0b65f187fca [improvement] retain stack of previously shown screens when navigating between screens in fullscreen mode
0ad57113a4a636af2c5c4924465304d0dd90a37a [improvement] show weekview automatically if using multiple monitors
d78cecaef0a9866b25d0a65328a6ab73bbc6e3a4 [feature] Whole week timeline view
a21577d50f916c49bd48acd777c04eef7ab1c38b [improvement] improve readability on top tasks
[improvement] ability to toggle tasks/projects in weekly summary
c3d6382ac3ebf2ecd87ddc72ef6546d4acdfc58a [improvement] Better error handling / notification for TSA import
aeccb43081848993b9b8415315cc9acd45a40585 [bugfix] Anchor options dialog properly to handle resizes
0c46b4fe24f373d5b0ef5fbfde55934f25ec3fec [improvement] Sync with TSA asynchronously at startup to improve speed
";
    private static string lastChangeset = "";

    private string formatRtf(string log)
    {
      var builder = new StringBuilder();
      var regex = new Regex(@"^([a-f0-9]+\s+)?(\S.+)", RegexOptions.Multiline);

      foreach (Match match in regex.Matches(revisionLog))
      {        
        if (match.Groups.Count > 1)
        {
          if (string.IsNullOrEmpty(lastChangeset))
          {
            lastChangeset = match.Groups[1].Value;
          }
          if (char.IsDigit(match.Groups[2].Value[0]))
          {
            builder.Append(

@"\pard\par
\par
\i " + match.Groups[2].Value + @"\par
\i0\par");
          }
          else
          {
            builder.Append(@"\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720" + match.Groups[2].Value + @"\par" + "\r\n");
          }
        }
        else
        {
          builder.Append(
@"\pard\par

\par");
        }
      }

      return builder.ToString();
    }

    public VersionHistory(TopForm topForm) : base(topForm)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

      TimeStampLabel.Text = GetTimeStamp().ToString("dd MMM yyyy HH:mm");

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

      richTextBox1.Rtf =

@"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fswiss\fcharset0 Arial;}{\f1\fnil\fcharset2 Symbol;}}
{\*\generator Msftedit 5.41.15.1507;}\viewkind4\uc1\pard\b\fs24 Version History\b0\fs20\par

"
+
formatRtf(revisionLog) +
@"

\par
\i 4.4 - 7th November 2012:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720[bugfix] Adding tasks on timeline edit should use correct date\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720[bugfix] summary should contain most used tasks not least used\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720[bugfix] autohide properly after showing reassign time buttons\par
\pard\par

\par
\i 4.3 - 6th November 2012:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 [feature] Add summary of time this week to fullscreen view\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 [bugfix] Don't treat working after midnight as the first task in timeline for the next day's initial ""none"" task\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 [bugfix] Handle setting config values to null\par
\pard\par

\par
\i 4.2 - 31st October 2012:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Make main window hide at same speed as reassign buttons after changing task \par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Treat work just after midnight as part of the previous day in timelines\par
\pard\par

\par
\i 4.1 - 30th October 2012:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Draw slight attention to updates\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Fix alignment issue on options screen\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Speculative fix for multi-monitors with non-aligned tops\par
\pard\par


\par
\i 4.0 - 29th October 2012:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Lots of stuff...\par
\pard\par

\par
\i 3.11 - 22nd February 2011:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Fix cookie issue which was breaking bugzilla integration\par
\pard\par


\par
\i 3.10 - 20th September 2010:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Prevent multiple instances running in parallel\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Accept customer friendly bugzilla statuses from support DB\par
\pard\par

\par
\i 3.9 - 18th August 2010:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Handle quick tasks and TSA codes better\par
\pard\par

\par
\i 3.8 - 3rd February 2010:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Reassign task controls appear when un-pausing\par
\pard\par


\par
\i 3.7 - 3rd February 2010:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Can assign multiple semicolon-separated codes to one task to divide time equally\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Can assign the special code DivideAmongChildren to reassign time in proportion among child tasks for the week\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Can assign the special code DivideAmongAllTasks to reassign time in proportion among all tasks for the week\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Changing the task assigned to a time period in timeline view now persists\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Changes in timeline view are immediately persisted in time details grid\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Timeline shows gaps as empty tasks to allow easier reassignment\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Start and end of day are tracked and shown on timeline\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 When changing task you are given an option to reassign recent time to correct mistakes where you forgot to switch\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Replaced keyboard hook with a 64-bit friendly one\par
\pard\par

\par
\i 3.6.4 - 8th February 2009:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Include bug details when exporting to TSA, as well as BUG:xxx code\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Automatically assign TSA codes to bugs/incidents/reviews\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Autocomplete in code box accepts delete and backspace\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Quick task box autocompletes with TSA codes\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Tasks with same code generate only one line in TSA export\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Edit task box now saves changes to codes\par
\pard\par


\par
\i 3.6.3 - 8th January 2009:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Integrate with timesheet app\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Fix bug where link button didn't work for https bugzilla URLs\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Add quick task entry button and win Q keyboard shortcut\par
\pard\par

\par
\i 3.6.2 - 15th October 2008:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Remove references to prince.zoo.lan\par
\pard\par

\par
\i 3.6.1 - 15th September 2008:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Fix bugzilla integration to work with new bugzilla\par
{\pntext\f1\'B7\tab}Prevent dodgy old task data from blocking bugzilla import\par
\pard\par


\par
\i 3.6 - 20th July 2008:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Add scrollbars to timeline edit\par
{\pntext\f1\'B7\tab}Recent tasks have correct colour icons when selected\par
{\pntext\f1\'B7\tab}Don't nag just after changing task, or while task select is open\par
{\pntext\f1\'B7\tab}Add auto-complete to parent combo on edit task\par
{\pntext\f1\'B7\tab}Stop loss of line breaks in notes\par
{\pntext\f1\'B7\tab}Prevent overlapping names in timeline\par
{\pntext\f1\'B7\tab}Make time details remember splitter position properly\par
{\pntext\f1\'B7\tab}Add days to time details\par
{\pntext\f1\'B7\tab}Make newly added tasks have top priority\par
{\pntext\f1\'B7\tab}Add ""send to top"", ""send to bottom"" in priority view\par
{\pntext\f1\'B7\tab}Make task hierarchy clear in names in priority view\par
{\pntext\f1\'B7\tab}Display notes in tooltip in treeview\par
{\pntext\f1\'B7\tab}Add context menu item to reassign task to different parent\par
\pard\par

\par
\i 3.5.1 - 27th March 2008:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Fix crashes in timeline edit caused by time periods spanning midnight\par
{\pntext\f1\'B7\tab}Fix incorrect colours for some child tasks\par
{\pntext\f1\'B7\tab}Fix resizing issues with timeline edit, and tidy up the UI slightly\par
\pard\par

\par
\i 3.5 - 25h May 2008:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Fix logic errors in auto-pause which could result in time being tracked even when paused\par
{\pntext\f1\'B7\tab}Make current task bold in treeview\par
{\pntext\f1\'B7\tab}Prevent infinite loop when dragging a task to become a child of itself\par
{\pntext\f1\'B7\tab}Pause rather than stopping tasks after inactivity\par
{\pntext\f1\'B7\tab}Add option to pause immediately when computer is locked (off by default)\par
{\pntext\f1\'B7\tab}Add timeline view to edit time-periods (accessible from context menu or timedetails)\par
{\pntext\f1\'B7\tab}Fix bug where bugzilla link button could disappear\par
{\pntext\f1\'B7\tab}Make recent tasks correctly coloured\par
{\pntext\f1\'B7\tab}Add aggresive nag feature for people who forget to switch tasks (disabled by default - see options)\par
\pard\par

\par
\i 3.4 - 21st April 2008:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Fix crash when selecting ""no task""\par
{\pntext\f1\'B7\tab}Fix crash opening time details window more than once\par
{\pntext\f1\'B7\tab}Reset pause button state when changing tasks\par
{\pntext\f1\'B7\tab}Wire up Completed and Deleted checkboxes on edit task properly\par
{\pntext\f1\'B7\tab}Change context menu to toggle complete/deleted rather than always setting \par

\pard\par
\par
\i 3.3 - 19th April 2008:\par
\i0\par
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-720\li720 Add version history(!)\par
{\pntext\f1\'B7\tab}Fix resizing of options window\par
{\pntext\f1\'B7\tab}Task button on toolbar closes task list if open\par
{\pntext\f1\'B7\tab}Correctly update after colour changes\par
{\pntext\f1\'B7\tab}Distinguish complete/deleted tasks in list \par
{\pntext\f1\'B7\tab}Improve drag/drop speed in treeview \par
{\pntext\f1\'B7\tab}Make time details remember splitter position and view option \par
{\pntext\f1\'B7\tab}Add an ""Add and set active"" button to new task window \par
{\pntext\f1\'B7\tab}Select today by default in time details \par
{\pntext\f1\'B7\tab}Add pause button \par
{\pntext\f1\'B7\tab}Only allow one instance of time details, task details windows \par
{\pntext\f1\'B7\tab}Order tasks alphabetically in time details \par
{\pntext\f1\'B7\tab}Add recent tasks list \par
{\pntext\f1\'B7\tab}Add priority ordering mode \par
}
"
;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VersionHistory));
      this.TimeStampLabel = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.richTextBox1 = new System.Windows.Forms.RichTextBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // TimeStampLabel
      // 
      this.TimeStampLabel.AutoSize = true;
      this.TimeStampLabel.Location = new System.Drawing.Point(8, 398);
      this.TimeStampLabel.Name = "TimeStampLabel";
      this.TimeStampLabel.Size = new System.Drawing.Size(0, 13);
      this.TimeStampLabel.TabIndex = 2;
      // 
      // button1
      // 
      this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.button1.Location = new System.Drawing.Point(340, 398);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(88, 24);
      this.button1.TabIndex = 1;
      this.button1.Text = "OK";
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // richTextBox1
      // 
      this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.richTextBox1.Location = new System.Drawing.Point(10, 10);
      this.richTextBox1.Margin = new System.Windows.Forms.Padding(10);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.Size = new System.Drawing.Size(420, 372);
      this.richTextBox1.TabIndex = 0;
      this.richTextBox1.Text = "";
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.Color.White;
      this.panel1.Controls.Add(this.richTextBox1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Padding = new System.Windows.Forms.Padding(10);
      this.panel1.Size = new System.Drawing.Size(440, 392);
      this.panel1.TabIndex = 3;
      // 
      // VersionHistory
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(440, 430);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.TimeStampLabel);
      this.Controls.Add(this.button1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "VersionHistory";
      this.Text = "Version History";
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }
		#endregion

    private void button1_Click(object sender, System.EventArgs e)
    {
      Close();
    }

	  public static void CheckForNewChanges(IConfigStore configStore, TopForm topForm)
	  {
	    var current = GetTimeStamp();
	    var previous = configStore.get<DateTime>(eConfig.lastKnownDatestamp);

      if (current > previous.Value.AddMinutes(1))
      {
        topForm.NotifyUpdateAvailable();
        previous.Value = current;
      }
	  }
	}
}
