﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3.UI
{
  public class ScreenManager
  {
    TopForm topForm;
    IDictionary<Type,IFullScreenUI> potentialScreens = new Dictionary<Type, IFullScreenUI>();
    IDictionary<Type, Type> preferences = new Dictionary<Type, Type>();
    IDictionary<string, IFullScreenUI> currentScreens = new Dictionary<string, IFullScreenUI>();
    Stack<IFullScreenUI> previousScreens = new Stack<IFullScreenUI>();

    private object lockable = new object();

    public ScreenManager(TopForm topForm)
    {
      this.topForm = topForm;
    }

    public void RegisterScreen(IFullScreenUI screen)
    {
      potentialScreens[screen.GetType()] = screen;
      screen.VisibleChanged += new EventHandler(VisibleChanged);

#if LEAVE_SECONDARY
      screen.TopMost = false;
#endif
    }

    void VisibleChanged(object sender, EventArgs e)
    {
      if (!((IFullScreenUI)sender).Visible)
      {
        if (previousScreens.Any())
        {
          Show(previousScreens.Pop().GetType());
        }
        else
        {
          foreach (IFullScreenUI active in currentScreens.Values)
          {
            active.Hide();
          }
          topForm.Show();
        }
           
      }

    }

    public void RegisterPreference(IFullScreenUI focus, IFullScreenUI secondary)
    {
      preferences[focus.GetType()] = secondary.GetType();
    }

    public void Show<T>() where T : IFullScreenUI
    {
      Show(typeof(T));
    }

    private void Show(Type type) 
    {
      lock (lockable)
      {
        IFullScreenUI old = null;
        currentScreens.TryGetValue(PrimaryScreen.DeviceName, out old);
        if (old == null || !old.Visible || old.GetType() == type)
        {
          old = null;
        }

        var primary = getInstance(type);
        IFullScreenUI secondary = null;
        if (preferences.ContainsKey(primary.GetType()))
        {
          secondary = getInstance(preferences[primary.GetType()]);
        }

        position(primary, secondary);

        if (old != null)
        {
          previousScreens.Push(old);
        }
        topForm.Hide();
      }
    }

    private Screen PrimaryScreen
    {
      get { return Screen.FromControl(topForm); }
    }

    private void position(IFullScreenUI primary, IFullScreenUI secondary)
    {
      lock (lockable)
      {
        var newScreens = new Dictionary<string, IFullScreenUI>();
        var toRemove = currentScreens.Values.ToList();


        newScreens[PrimaryScreen.DeviceName] = primary;
        toRemove.Remove(primary);

        foreach (Screen screen in Screen.AllScreens)
        {
          if (screen.DeviceName != PrimaryScreen.DeviceName)
          {
            if (secondary != null)
            {
              newScreens[screen.DeviceName] = secondary;
              toRemove.Remove(secondary);
              secondary = null;
            }
            else
            {
              IFullScreenUI blank;
              currentScreens.TryGetValue(screen.DeviceName, out blank);
              newScreens[screen.DeviceName] = blank is BlankScreen ? blank : new BlankScreen();
              // TODO - user can close screen with alt-f4 which causes issues here
              if (blank != null)
              {
                toRemove.Remove(blank);
              }
            }
          }
        }


        foreach (KeyValuePair<string, IFullScreenUI> live in newScreens)
        {
          try
          {
            live.Value.Bounds = Screen.AllScreens.Where(s => s.DeviceName == live.Key).First().Bounds;
            live.Value.Show();
          }
          catch (Exception e)
          {
            topForm.NotifyUserError(e.ToString());
          }
        }

        currentScreens = newScreens;

        foreach (IFullScreenUI dead in toRemove)
        {
          dead.Hide();
          if (dead is BlankScreen)
          {
            dead.Dispose();
          }
        }

        currentScreens = newScreens;

        primary.Focus();
      }
    }

    private IFullScreenUI getInstance(Type type)
    {
      return potentialScreens[type];
    }




  }
}
