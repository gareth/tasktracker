﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using Tasks3.Config;

namespace Tasks3
{
  public partial class Options : PositionPersistingForm
  {
    public Options(TopForm topForm, IConfigStore configStore)
      : base(topForm)
    {
      InitializeComponent();

      AddNumericValue(
        "Smallest time interval to track (secs)",
        "Sets the minimum period which will be tracked. If you spend less than this time on a task in a single session then the time won't count towards totals",
        (int) Config.MinCountablePeriod.TotalSeconds,
        new Action<object>(value => { Config.MinCountablePeriod = TimeSpan.FromSeconds(double.Parse((string) value)); }));

      AddNumericValue(
        "Archive interval (days)",
        "Tasks with no tracked activity for this period will be hidden by default",
        (int) Config.ArchiveInterval.TotalDays,
        new Action<object>(value => { Config.ArchiveInterval = TimeSpan.FromDays(double.Parse((string) value)); }));

      AddNumericValue(
        "Interval for \"no task\" reminders (secs)",
        "If no task is selected, a reminder is shown after this period",
        (int) Config.NoTaskReminderInterval.TotalSeconds,
        new Action<object>(
          value => { Config.NoTaskReminderInterval = TimeSpan.FromSeconds(double.Parse((string) value)); }));

      AddNumericValue(
        "Auto-pause interval (mins)",
        "If no keypresses are registered for this period, the tracker is stopped. The time tracked is measured only up until the last keypress.",
        (int) Config.AutoPauseInterval.TotalMinutes,
        new Action<object>(value => { Config.AutoPauseInterval = TimeSpan.FromMinutes(double.Parse((string) value)); }));

      AddNumericValue(
        "Reminder interval (mins)",
        "After this period, a reminder will be briefly shown of the current task and duration.",
        (int) Config.ReminderInterval.TotalMinutes,
        new Action<object>(value => { Config.ReminderInterval = TimeSpan.FromMinutes(double.Parse((string) value)); }));

      AddBooleanValue(
        "Enable Bugzilla integration",
        "If enabled, tasks will automatically be imported from bugzilla. Requires other bugzilla configuration to be correctly configured",
        Config.EnableBugzillaIntegration,
        new Action<object>(value => { Config.EnableBugzillaIntegration = (bool) value; }));

      AddStringValue(
        "Bugzilla username",
        "Username for bugzilla login",
        Config.BugzillaUser,
        new Action<object>(value => { Config.BugzillaUser = (string) value; }));

      AddStringValue(
        "Bugzilla password",
        "Username for bugzilla password",
        Config.BugzillaPassword,
        new Action<object>(value => { Config.BugzillaPassword = (string) value; }));

      AddNumericValue(
        "Bugzilla update interval (secs)",
        "Frequency with which bugzilla tasks are updated.",
        (int) Config.BugzillaImportFrequency.TotalSeconds,
        new Action<object>(
          value => { Config.BugzillaImportFrequency = TimeSpan.FromSeconds(double.Parse((string) value)); }));

      AddNumericValue(
        "Jira update interval (secs)",
        "Frequency with which Jira tasks are updated.",
        (int) Config.JiraImportFrequency.TotalSeconds,
        new Action<object>(value => { Config.JiraImportFrequency = TimeSpan.FromSeconds(double.Parse((string) value)); }));

      AddStringValue(
        "Save file location",
        "Tasks will be saved to disk in this location",
        Config.FileLocation,
        new Action<object>(value => { Config.FileLocation = (string) value; }));

      AddStringValue(
        "Update check file location",
        "The file at this location will be checked periodically for updates",
        Config.VersionFileLocation,
        new Action<object>(value => { Config.VersionFileLocation = (string) value; }));

      AddBooleanValue(
        "Pause when computer is locked",
        "If enabled, locking the computer will immediately pause the current task. Unlocking will not resume automatically.",
        Config.PauseWhenStationLocked,
        new Action<object>(value => { Config.PauseWhenStationLocked = (bool) value; }));

      AddNumericValue(
        "Aggressive nagging interval (mins)",
        "If non-zero, the time tracker will nag you regularly to confirm what you're currently working on. Intended for people who forget to switch tasks...",
        (int) Config.NagInterval.TotalMinutes,
        new Action<object>(value => { Config.NagInterval = TimeSpan.FromMinutes(double.Parse((string) value)); }));

      AddBooleanValue(
        "Perma-nag when no task selected",
        "If enabled, the nag window will be permanently displayed when you have no active unpaused task.",
        !Config.AllowNoTaskAssignment,
        new Action<object>(value => { Config.AllowNoTaskAssignment = !(bool) value; }));


      AddBooleanValue(
        "Focus task in recent list",
        "If enabled, the focused task will be in the recent list not the main list.",
        Config.FocusRecentList,
        new Action<object>(value => { Config.FocusRecentList = (bool) value; }));

      AddStringValue(
        "Timesheet app URL",
        "The location of the timesheet app for importing tasks and exporting timesheet data",
        Config.TimesheetAppUrl,
        new Action<object>(value => { Config.TimesheetAppUrl = (string) value; }));

      AddBooleanValue(
        "Prompt for reassigning time",
        "If enabled, will show a prompt to allow you to reassign recent time when switching tasks",
        Config.EnableReassignPrompt,
        new Action<object>(value => { Config.EnableReassignPrompt = (bool) value; }));

      AddBooleanValue(
        "Show in task bar",
        "If enabled, tasktracker will be permanently visible in the taskbar (good on win7)",
        Config.ShowInTaskbar,
        new Action<object>(value => { Config.ShowInTaskbar = (bool) value; }));

      AddBooleanValue(
        "Auto-hide",
        "If enabled, tasktracker will pop up to the top of the screen and reappear on hover",
        Config.AutoHide,
        new Action<object>(value => { Config.AutoHide = (bool) value; }));

      //qq DRY
      AddBooleanValue(
        "Enable Jira integration",
        "If enabled, tasks will automatically be imported from Jira. Requires other Jira configuration to be correctly configured",
        Config.EnableJiraIntegration,
        new Action<object>(value => { Config.EnableJiraIntegration = (bool) value; }));

      foreach (eConfig config in Enum.GetValues(typeof (eConfig)))
      {
        if (string.IsNullOrEmpty(config.GetLabel()))
        {
          continue;
        }

        if (config.GetConfigType() == typeof (string))
        {
          var cfg = configStore.get<string>(config);
          AddStringValue(
            config.GetLabel(),
            config.GetConfigDescription(),
            cfg.Value,
            (x => cfg.Value = x));

        }
        else if (config.GetConfigType() == typeof (SecureString))
        {
          var cfg = configStore.get<SecureString>(config);
          var old = string.IsNullOrEmpty(cfg.Value) ? "not set" : "(hidden)";
          AddStringValue(
            config.GetLabel(),
            config.GetConfigDescription(),
            old,
            (x =>
              {
                if (x != old)
                {
                  cfg.Value = x;
                }
              }));
        }
        else if (config.GetConfigType() == typeof (bool))
        {
          var cfg = configStore.get<bool>(config);
          AddBooleanValue(
            config.GetLabel(),
            config.GetConfigDescription(),
            cfg.Value, (x => cfg.Value = (bool) x));
        }
      }
    }

    private void AddNumericValue(string xiName, string xiTooltip, int xiValue, Action<object> xiUpdateDelegate)
    {
      DataGridViewRow lRow = new DataGridViewRow();

      DataGridViewTextBoxCell lLabelCell = new DataGridViewTextBoxCell();
      lLabelCell.Value = xiName;
      lLabelCell.ToolTipText = xiTooltip;

      DataGridViewTextBoxCell lValueCell = new DataGridViewTextBoxCell();
      lValueCell.Value = xiValue.ToString();
      lValueCell.Tag = xiUpdateDelegate;

      lRow.Cells.AddRange(lLabelCell, lValueCell);

      OptionsDataGrid.Rows.Add(lRow);
    }

    private void AddListValue(string xiName, string xiTooltip, Dictionary<string, int> xiChoices, int xiValue, Action<object> xiUpdateDelegate)
    {
      DataGridViewRow lRow = new DataGridViewRow();

      DataGridViewTextBoxCell lLabelCell = new DataGridViewTextBoxCell();
      lLabelCell.Value = xiName;
      lLabelCell.ToolTipText = xiTooltip;

      DataGridViewComboBoxCell lValueCell = new DataGridViewComboBoxCell();
      lValueCell.DataSource = new List<KeyValuePair<string, int>>(xiChoices);
      lValueCell.DisplayMember = "Key";
      lValueCell.ValueMember = "Value";
      lValueCell.Value = xiValue;
      lValueCell.Tag = xiUpdateDelegate;

      lRow.Cells.AddRange(lLabelCell, lValueCell);

      OptionsDataGrid.Rows.Add(lRow);
    }

    private void AddBooleanValue(string xiName, string xiTooltip, bool xiValue, Action<object> xiUpdateDelegate)
    {
      DataGridViewRow lRow = new DataGridViewRow();
      lRow.Tag = "Numeric";

      DataGridViewTextBoxCell lLabelCell = new DataGridViewTextBoxCell();
      lLabelCell.Value = xiName;
      lLabelCell.ToolTipText = xiTooltip;

      DataGridViewCheckBoxCell lValueCell = new DataGridViewCheckBoxCell();
      lValueCell.Value = xiValue;
      lValueCell.Tag = xiUpdateDelegate;

      lRow.Cells.AddRange(lLabelCell, lValueCell);

      OptionsDataGrid.Rows.Add(lRow);
    }

    private void AddStringValue(string xiName, string xiTooltip, string xiValue, Action<string> xiUpdateDelegate)
    {
      DataGridViewRow lRow = new DataGridViewRow();

      DataGridViewTextBoxCell lLabelCell = new DataGridViewTextBoxCell();
      lLabelCell.Value = xiName;
      lLabelCell.ToolTipText = xiTooltip;

      DataGridViewTextBoxCell lValueCell = new DataGridViewTextBoxCell();
      lValueCell.Value = xiValue ?? "";
      lValueCell.Tag = new Action<object>(x => xiUpdateDelegate((string)x));

      lRow.Cells.AddRange(lLabelCell, lValueCell);

      OptionsDataGrid.Rows.Add(lRow);
    }

    private void SaveChanges()
    {
      foreach (DataGridViewRow lRow in OptionsDataGrid.Rows)
      {
        var lCell = lRow.Cells[1];
        ((Action<object>)lCell.Tag)(lCell.Value);
      }
    }

    private void OptionsDataGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
    {
      if (e.Control is TextBox)
      {
        ((TextBox)e.Control).KeyPress -= new KeyPressEventHandler(Options_KeyPress);
        ((TextBox)e.Control).KeyPress += new KeyPressEventHandler(Options_KeyPress);
      }
    }

    void Options_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (OptionsDataGrid.CurrentRow.Tag as string == "Numeric" && !char.IsNumber(e.KeyChar))
      {
        e.Handled = true;
      }
    }

    private void CancelBtn_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void OKBtn_Click(object sender, EventArgs e)
    {
      SaveChanges();
      Close();
    }
  }
}
