﻿namespace Tasks3
{
  partial class FullScreen
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FullScreen));
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.matchingTaskButton = new System.Windows.Forms.Button();
      this.RecentTasksGroupBox = new System.Windows.Forms.GroupBox();
      this.label1 = new System.Windows.Forms.Label();
      this.BestMatchLabel = new System.Windows.Forms.Label();
      this.BestMatchHint = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.timeLinePanel = new Tasks3.UI.TimeLinePanel();
      this.moreTimelinesButton = new System.Windows.Forms.Button();
      this.midPanel = new System.Windows.Forms.Panel();
      this.toolTip = new System.Windows.Forms.ToolTip(this.components);
      this.topTasksPanel1 = new Tasks3.UI.TopTasksPanel();
      this.label2 = new System.Windows.Forms.Label();
      this.RightPanel = new System.Windows.Forms.Panel();
      this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.detailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.completeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deletedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.RightPanel.SuspendLayout();
      this.contextMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // textBox1
      // 
      this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox1.Location = new System.Drawing.Point(504, 39);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(248, 29);
      this.textBox1.TabIndex = 0;
      this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
      this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onTextboxKeyDown);
      // 
      // matchingTaskButton
      // 
      this.matchingTaskButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.matchingTaskButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.matchingTaskButton.Location = new System.Drawing.Point(504, 82);
      this.matchingTaskButton.Name = "matchingTaskButton";
      this.matchingTaskButton.Size = new System.Drawing.Size(248, 23);
      this.matchingTaskButton.TabIndex = 1;
      this.matchingTaskButton.UseVisualStyleBackColor = true;
      this.matchingTaskButton.Visible = false;
      this.matchingTaskButton.VisibleChanged += new System.EventHandler(this.matchingTaskButton_VisibleChanged);
      this.matchingTaskButton.Click += new System.EventHandler(this.matchingTaskButton_Click);
      // 
      // RecentTasksGroupBox
      // 
      this.RecentTasksGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.RecentTasksGroupBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.RecentTasksGroupBox.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.RecentTasksGroupBox.Location = new System.Drawing.Point(12, 127);
      this.RecentTasksGroupBox.Name = "RecentTasksGroupBox";
      this.RecentTasksGroupBox.Size = new System.Drawing.Size(254, 761);
      this.RecentTasksGroupBox.TabIndex = 2;
      this.RecentTasksGroupBox.TabStop = false;
      this.RecentTasksGroupBox.Text = "Recent";
      // 
      // label1
      // 
      this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label1.Location = new System.Drawing.Point(250, 38);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(248, 25);
      this.label1.TabIndex = 3;
      this.label1.Text = "What are you working on?";
      this.label1.Click += new System.EventHandler(this.label1_Click);
      // 
      // BestMatchLabel
      // 
      this.BestMatchLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.BestMatchLabel.AutoSize = true;
      this.BestMatchLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.BestMatchLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.BestMatchLabel.Location = new System.Drawing.Point(413, 85);
      this.BestMatchLabel.Name = "BestMatchLabel";
      this.BestMatchLabel.Size = new System.Drawing.Size(85, 20);
      this.BestMatchLabel.TabIndex = 4;
      this.BestMatchLabel.Text = "Best match:";
      this.BestMatchLabel.Visible = false;
      this.BestMatchLabel.Click += new System.EventHandler(this.label2_Click);
      // 
      // BestMatchHint
      // 
      this.BestMatchHint.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.BestMatchHint.AutoSize = true;
      this.BestMatchHint.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.BestMatchHint.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.BestMatchHint.Location = new System.Drawing.Point(758, 82);
      this.BestMatchHint.Name = "BestMatchHint";
      this.BestMatchHint.Size = new System.Drawing.Size(119, 20);
      this.BestMatchHint.TabIndex = 5;
      this.BestMatchHint.Text = "(Enter to accept)";
      this.BestMatchHint.Visible = false;
      this.BestMatchHint.Click += new System.EventHandler(this.label3_Click);
      // 
      // label4
      // 
      this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label4.Location = new System.Drawing.Point(758, 43);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(206, 20);
      this.label4.TabIndex = 6;
      this.label4.Text = "(Ctrl-Enter to create new task)";
      this.label4.Click += new System.EventHandler(this.label4_Click);
      // 
      // timeLinePanel
      // 
      this.timeLinePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.timeLinePanel.BackColor = System.Drawing.Color.Transparent;
      this.timeLinePanel.ForeColor = System.Drawing.Color.White;
      this.timeLinePanel.Location = new System.Drawing.Point(0, 0);
      this.timeLinePanel.Max = null;
      this.timeLinePanel.Min = null;
      this.timeLinePanel.Name = "timeLinePanel";
      this.timeLinePanel.Selected = null;
      this.timeLinePanel.Size = new System.Drawing.Size(239, 541);
      this.timeLinePanel.TabIndex = 7;
      this.timeLinePanel.TimeLine = null;
      // 
      // moreTimelinesButton
      // 
      this.moreTimelinesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.moreTimelinesButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this.moreTimelinesButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
      this.moreTimelinesButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDarkDark;
      this.moreTimelinesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.moreTimelinesButton.ForeColor = System.Drawing.SystemColors.ControlLight;
      this.moreTimelinesButton.Location = new System.Drawing.Point(184, 547);
      this.moreTimelinesButton.Name = "moreTimelinesButton";
      this.moreTimelinesButton.Size = new System.Drawing.Size(52, 25);
      this.moreTimelinesButton.TabIndex = 1;
      this.moreTimelinesButton.Text = "More...";
      this.moreTimelinesButton.UseVisualStyleBackColor = true;
      this.moreTimelinesButton.Click += new System.EventHandler(this.moreTimelinesButton_Click);
      // 
      // midPanel
      // 
      this.midPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.midPanel.Location = new System.Drawing.Point(272, 137);
      this.midPanel.Name = "midPanel";
      this.midPanel.Size = new System.Drawing.Size(755, 751);
      this.midPanel.TabIndex = 8;
      // 
      // topTasksPanel1
      // 
      this.topTasksPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.topTasksPanel1.BackColor = System.Drawing.Color.Transparent;
      this.topTasksPanel1.data = null;
      this.topTasksPanel1.Location = new System.Drawing.Point(0, 593);
      this.topTasksPanel1.MinimumSize = new System.Drawing.Size(200, 100);
      this.topTasksPanel1.Name = "topTasksPanel1";
      this.topTasksPanel1.NumberToShow = 5;
      this.topTasksPanel1.Size = new System.Drawing.Size(239, 134);
      this.topTasksPanel1.TabIndex = 9;
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label2.Location = new System.Drawing.Point(3, 573);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(121, 17);
      this.label2.TabIndex = 10;
      this.label2.Text = "This week summary";
      this.label2.Click += new System.EventHandler(this.label2_Click_1);
      // 
      // RightPanel
      // 
      this.RightPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.RightPanel.Controls.Add(this.timeLinePanel);
      this.RightPanel.Controls.Add(this.moreTimelinesButton);
      this.RightPanel.Controls.Add(this.label2);
      this.RightPanel.Controls.Add(this.topTasksPanel1);
      this.RightPanel.Location = new System.Drawing.Point(1033, 137);
      this.RightPanel.Name = "RightPanel";
      this.RightPanel.Size = new System.Drawing.Size(239, 730);
      this.RightPanel.TabIndex = 11;
      // 
      // contextMenu
      // 
      this.contextMenu.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detailsToolStripMenuItem,
            this.completeToolStripMenuItem,
            this.deletedToolStripMenuItem});
      this.contextMenu.Name = "contextMenu";
      this.contextMenu.Size = new System.Drawing.Size(127, 70);
      this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
      // 
      // detailsToolStripMenuItem
      // 
      this.detailsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.detailsToolStripMenuItem.Name = "detailsToolStripMenuItem";
      this.detailsToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
      this.detailsToolStripMenuItem.Text = "Details..";
      this.detailsToolStripMenuItem.Click += new System.EventHandler(this.detailsToolStripMenuItem_Click);
      // 
      // completeToolStripMenuItem
      // 
      this.completeToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.completeToolStripMenuItem.Name = "completeToolStripMenuItem";
      this.completeToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
      this.completeToolStripMenuItem.Text = "Complete";
      this.completeToolStripMenuItem.Click += new System.EventHandler(this.completeToolStripMenuItem_Click);
      // 
      // deletedToolStripMenuItem
      // 
      this.deletedToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.deletedToolStripMenuItem.Name = "deletedToolStripMenuItem";
      this.deletedToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
      this.deletedToolStripMenuItem.Text = "Deleted";
      this.deletedToolStripMenuItem.Click += new System.EventHandler(this.deletedToolStripMenuItem_Click);
      // 
      // FullScreen
      // 
      this.AcceptButton = this.matchingTaskButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Black;
      this.ClientSize = new System.Drawing.Size(1284, 900);
      this.ControlBox = false;
      this.Controls.Add(this.RightPanel);
      this.Controls.Add(this.midPanel);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.BestMatchHint);
      this.Controls.Add(this.BestMatchLabel);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.RecentTasksGroupBox);
      this.Controls.Add(this.matchingTaskButton);
      this.Controls.Add(this.textBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "FullScreen";
      this.Opacity = 0.9D;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "FullScreen";
      this.TopMost = true;
      this.RightPanel.ResumeLayout(false);
      this.RightPanel.PerformLayout();
      this.contextMenu.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Button matchingTaskButton;
    private System.Windows.Forms.GroupBox RecentTasksGroupBox;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label BestMatchLabel;
    private System.Windows.Forms.Label BestMatchHint;
    private System.Windows.Forms.Label label4;
    private UI.TimeLinePanel timeLinePanel;
    private System.Windows.Forms.Panel midPanel;
    private System.Windows.Forms.ToolTip toolTip;
    private UI.TopTasksPanel topTasksPanel1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button moreTimelinesButton;
    private System.Windows.Forms.Panel RightPanel;
    private System.Windows.Forms.ContextMenuStrip contextMenu;
    private System.Windows.Forms.ToolStripMenuItem detailsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem completeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem deletedToolStripMenuItem;
  }
}