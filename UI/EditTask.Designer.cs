﻿namespace Tasks3
{
  partial class EditTask
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTask));
      this.DescriptionLabel = new System.Windows.Forms.Label();
      this.DescriptionTextBox = new System.Windows.Forms.TextBox();
      this.ParentLabel = new System.Windows.Forms.Label();
      this.ParentComboBox = new System.Windows.Forms.ComboBox();
      this.NotesGroupBox = new System.Windows.Forms.GroupBox();
      this.NotesTextBox = new System.Windows.Forms.TextBox();
      this.OKButton = new System.Windows.Forms.Button();
      this.ColorDialog = new System.Windows.Forms.ColorDialog();
      this.ColorLabel = new System.Windows.Forms.Label();
      this.ColorSample = new System.Windows.Forms.Label();
      this.ColorButton = new System.Windows.Forms.Button();
      this.CancelBtn = new System.Windows.Forms.Button();
      this.CompleteCheckBox = new System.Windows.Forms.CheckBox();
      this.DeletedCheckBox = new System.Windows.Forms.CheckBox();
      this.CodeCombo = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.NotesGroupBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // DescriptionLabel
      // 
      this.DescriptionLabel.AutoSize = true;
      this.DescriptionLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.DescriptionLabel.Location = new System.Drawing.Point(12, 9);
      this.DescriptionLabel.Name = "DescriptionLabel";
      this.DescriptionLabel.Size = new System.Drawing.Size(67, 15);
      this.DescriptionLabel.TabIndex = 0;
      this.DescriptionLabel.Text = "Description";
      // 
      // DescriptionTextBox
      // 
      this.DescriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.DescriptionTextBox.Location = new System.Drawing.Point(85, 7);
      this.DescriptionTextBox.Name = "DescriptionTextBox";
      this.DescriptionTextBox.Size = new System.Drawing.Size(574, 20);
      this.DescriptionTextBox.TabIndex = 1;
      // 
      // ParentLabel
      // 
      this.ParentLabel.AutoSize = true;
      this.ParentLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.ParentLabel.Location = new System.Drawing.Point(12, 35);
      this.ParentLabel.Name = "ParentLabel";
      this.ParentLabel.Size = new System.Drawing.Size(41, 15);
      this.ParentLabel.TabIndex = 2;
      this.ParentLabel.Text = "Parent";
      // 
      // ParentComboBox
      // 
      this.ParentComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.ParentComboBox.FormattingEnabled = true;
      this.ParentComboBox.Location = new System.Drawing.Point(85, 33);
      this.ParentComboBox.Name = "ParentComboBox";
      this.ParentComboBox.Size = new System.Drawing.Size(574, 21);
      this.ParentComboBox.TabIndex = 3;
      // 
      // NotesGroupBox
      // 
      this.NotesGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.NotesGroupBox.Controls.Add(this.NotesTextBox);
      this.NotesGroupBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.NotesGroupBox.Location = new System.Drawing.Point(12, 80);
      this.NotesGroupBox.Name = "NotesGroupBox";
      this.NotesGroupBox.Size = new System.Drawing.Size(647, 322);
      this.NotesGroupBox.TabIndex = 6;
      this.NotesGroupBox.TabStop = false;
      this.NotesGroupBox.Text = "Notes";
      // 
      // NotesTextBox
      // 
      this.NotesTextBox.AcceptsReturn = true;
      this.NotesTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.NotesTextBox.Location = new System.Drawing.Point(6, 19);
      this.NotesTextBox.Multiline = true;
      this.NotesTextBox.Name = "NotesTextBox";
      this.NotesTextBox.Size = new System.Drawing.Size(635, 297);
      this.NotesTextBox.TabIndex = 7;
      // 
      // OKButton
      // 
      this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.OKButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.OKButton.Location = new System.Drawing.Point(503, 412);
      this.OKButton.Name = "OKButton";
      this.OKButton.Size = new System.Drawing.Size(75, 23);
      this.OKButton.TabIndex = 13;
      this.OKButton.Text = "OK";
      this.OKButton.UseVisualStyleBackColor = true;
      this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
      // 
      // ColorLabel
      // 
      this.ColorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ColorLabel.AutoSize = true;
      this.ColorLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.ColorLabel.Location = new System.Drawing.Point(188, 412);
      this.ColorLabel.Name = "ColorLabel";
      this.ColorLabel.Size = new System.Drawing.Size(43, 15);
      this.ColorLabel.TabIndex = 10;
      this.ColorLabel.Text = "Colour";
      // 
      // ColorSample
      // 
      this.ColorSample.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ColorSample.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.ColorSample.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.ColorSample.Location = new System.Drawing.Point(237, 412);
      this.ColorSample.Name = "ColorSample";
      this.ColorSample.Size = new System.Drawing.Size(117, 17);
      this.ColorSample.TabIndex = 11;
      this.ColorSample.Text = " ";
      // 
      // ColorButton
      // 
      this.ColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ColorButton.Location = new System.Drawing.Point(360, 408);
      this.ColorButton.Name = "ColorButton";
      this.ColorButton.Size = new System.Drawing.Size(75, 23);
      this.ColorButton.TabIndex = 12;
      this.ColorButton.Text = "Edit...";
      this.ColorButton.UseVisualStyleBackColor = true;
      this.ColorButton.Click += new System.EventHandler(this.ColorButton_Click);
      // 
      // CancelBtn
      // 
      this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CancelBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.CancelBtn.Location = new System.Drawing.Point(584, 412);
      this.CancelBtn.Name = "CancelBtn";
      this.CancelBtn.Size = new System.Drawing.Size(75, 23);
      this.CancelBtn.TabIndex = 14;
      this.CancelBtn.Text = "Cancel";
      this.CancelBtn.UseVisualStyleBackColor = true;
      this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
      // 
      // CompleteCheckBox
      // 
      this.CompleteCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.CompleteCheckBox.AutoSize = true;
      this.CompleteCheckBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.CompleteCheckBox.Location = new System.Drawing.Point(12, 410);
      this.CompleteCheckBox.Name = "CompleteCheckBox";
      this.CompleteCheckBox.Size = new System.Drawing.Size(85, 19);
      this.CompleteCheckBox.TabIndex = 8;
      this.CompleteCheckBox.Text = "Completed";
      this.CompleteCheckBox.UseVisualStyleBackColor = true;
      // 
      // DeletedCheckBox
      // 
      this.DeletedCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.DeletedCheckBox.AutoSize = true;
      this.DeletedCheckBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.DeletedCheckBox.Location = new System.Drawing.Point(103, 410);
      this.DeletedCheckBox.Name = "DeletedCheckBox";
      this.DeletedCheckBox.Size = new System.Drawing.Size(66, 19);
      this.DeletedCheckBox.TabIndex = 9;
      this.DeletedCheckBox.Text = "Deleted";
      this.DeletedCheckBox.UseVisualStyleBackColor = true;
      // 
      // CodeCombo
      // 
      this.CodeCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.CodeCombo.FormattingEnabled = true;
      this.CodeCombo.Location = new System.Drawing.Point(85, 60);
      this.CodeCombo.Name = "CodeCombo";
      this.CodeCombo.Size = new System.Drawing.Size(574, 21);
      this.CodeCombo.TabIndex = 5;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(12, 62);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(35, 15);
      this.label1.TabIndex = 4;
      this.label1.Text = "Code";
      // 
      // EditTask
      // 
      this.AcceptButton = this.OKButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.CancelBtn;
      this.ClientSize = new System.Drawing.Size(671, 443);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.CodeCombo);
      this.Controls.Add(this.DeletedCheckBox);
      this.Controls.Add(this.CompleteCheckBox);
      this.Controls.Add(this.CancelBtn);
      this.Controls.Add(this.ColorButton);
      this.Controls.Add(this.ColorSample);
      this.Controls.Add(this.ColorLabel);
      this.Controls.Add(this.OKButton);
      this.Controls.Add(this.NotesGroupBox);
      this.Controls.Add(this.ParentComboBox);
      this.Controls.Add(this.ParentLabel);
      this.Controls.Add(this.DescriptionTextBox);
      this.Controls.Add(this.DescriptionLabel);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "EditTask";
      this.Text = "Edit Task";
      this.NotesGroupBox.ResumeLayout(false);
      this.NotesGroupBox.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label DescriptionLabel;
    private System.Windows.Forms.TextBox DescriptionTextBox;
    private System.Windows.Forms.Label ParentLabel;
    private System.Windows.Forms.ComboBox ParentComboBox;
    private System.Windows.Forms.GroupBox NotesGroupBox;
    private System.Windows.Forms.TextBox NotesTextBox;
    private System.Windows.Forms.Button OKButton;
    private System.Windows.Forms.ColorDialog ColorDialog;
    private System.Windows.Forms.Label ColorLabel;
    private System.Windows.Forms.Label ColorSample;
    private System.Windows.Forms.Button ColorButton;
    private System.Windows.Forms.Button CancelBtn;
    private System.Windows.Forms.CheckBox CompleteCheckBox;
    private System.Windows.Forms.CheckBox DeletedCheckBox;
    private System.Windows.Forms.ComboBox CodeCombo;
    private System.Windows.Forms.Label label1;
  }
}