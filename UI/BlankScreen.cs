﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tasks3.Data;
using Tasks3.UI;

namespace Tasks3
{
  public partial class BlankScreen : Form, IDisposable, IFullScreenUI
  {
    public BlankScreen()
    {
      InitializeComponent();
    }
  }
}
