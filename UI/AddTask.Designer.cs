﻿namespace Tasks3
{
  partial class AddTask
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.DescriptionLabel = new System.Windows.Forms.Label();
      this.ParentLabel = new System.Windows.Forms.Label();
      this.DescriptionTextBox = new System.Windows.Forms.TextBox();
      this.AddButton = new System.Windows.Forms.Button();
      this.CancelButton = new System.Windows.Forms.Button();
      this.ParentCombo = new System.Windows.Forms.ComboBox();
      this.button1 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // DescriptionLabel
      // 
      this.DescriptionLabel.AutoSize = true;
      this.DescriptionLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.DescriptionLabel.Location = new System.Drawing.Point(12, 9);
      this.DescriptionLabel.Name = "DescriptionLabel";
      this.DescriptionLabel.Size = new System.Drawing.Size(67, 15);
      this.DescriptionLabel.TabIndex = 0;
      this.DescriptionLabel.Text = "Description";
      // 
      // ParentLabel
      // 
      this.ParentLabel.AutoSize = true;
      this.ParentLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.ParentLabel.Location = new System.Drawing.Point(12, 40);
      this.ParentLabel.Name = "ParentLabel";
      this.ParentLabel.Size = new System.Drawing.Size(41, 15);
      this.ParentLabel.TabIndex = 1;
      this.ParentLabel.Text = "Parent";
      // 
      // DescriptionTextBox
      // 
      this.DescriptionTextBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.DescriptionTextBox.Location = new System.Drawing.Point(85, 7);
      this.DescriptionTextBox.Name = "DescriptionTextBox";
      this.DescriptionTextBox.Size = new System.Drawing.Size(402, 23);
      this.DescriptionTextBox.TabIndex = 2;
      // 
      // AddButton
      // 
      this.AddButton.Location = new System.Drawing.Point(131, 65);
      this.AddButton.Name = "AddButton";
      this.AddButton.Size = new System.Drawing.Size(75, 23);
      this.AddButton.TabIndex = 99;
      this.AddButton.Text = "Add";
      this.AddButton.UseVisualStyleBackColor = true;
      this.AddButton.Click += new System.EventHandler(this.OKButton_Click);
      // 
      // CancelButton
      // 
      this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CancelButton.Location = new System.Drawing.Point(293, 65);
      this.CancelButton.Name = "CancelButton";
      this.CancelButton.Size = new System.Drawing.Size(75, 23);
      this.CancelButton.TabIndex = 102;
      this.CancelButton.Text = "Cancel";
      this.CancelButton.UseVisualStyleBackColor = true;
      this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
      // 
      // ParentCombo
      // 
      this.ParentCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.ParentCombo.FormattingEnabled = true;
      this.ParentCombo.Location = new System.Drawing.Point(85, 36);
      this.ParentCombo.Name = "ParentCombo";
      this.ParentCombo.Size = new System.Drawing.Size(402, 23);
      this.ParentCombo.TabIndex = 7;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(212, 65);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 100;
      this.button1.Text = "Set Active";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // AddTask
      // 
      this.AcceptButton = this.AddButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(492, 94);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.ParentCombo);
      this.Controls.Add(this.CancelButton);
      this.Controls.Add(this.AddButton);
      this.Controls.Add(this.DescriptionTextBox);
      this.Controls.Add(this.ParentLabel);
      this.Controls.Add(this.DescriptionLabel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Name = "AddTask";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Add a new task";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label DescriptionLabel;
    private System.Windows.Forms.Label ParentLabel;
    private System.Windows.Forms.TextBox DescriptionTextBox;
    private System.Windows.Forms.Button AddButton;
    private System.Windows.Forms.Button CancelButton;
    private System.Windows.Forms.ComboBox ParentCombo;
    private System.Windows.Forms.Button button1;
  }
}