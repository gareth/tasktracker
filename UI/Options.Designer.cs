﻿namespace Tasks3
{
  partial class Options
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
      this.OptionsDataGrid = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.CancelBtn = new System.Windows.Forms.Button();
      this.OKBtn = new System.Windows.Forms.Button();
      this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.OptionsDataGrid)).BeginInit();
      this.SuspendLayout();
      // 
      // OptionsDataGrid
      // 
      this.OptionsDataGrid.AllowUserToAddRows = false;
      this.OptionsDataGrid.AllowUserToDeleteRows = false;
      this.OptionsDataGrid.AllowUserToResizeRows = false;
      this.OptionsDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.OptionsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.OptionsDataGrid.BackgroundColor = System.Drawing.SystemColors.Window;
      this.OptionsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.OptionsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameColumn,
            this.ValueColumn});
      this.OptionsDataGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
      this.OptionsDataGrid.Location = new System.Drawing.Point(12, 12);
      this.OptionsDataGrid.Name = "OptionsDataGrid";
      this.OptionsDataGrid.RowHeadersVisible = false;
      this.OptionsDataGrid.Size = new System.Drawing.Size(560, 317);
      this.OptionsDataGrid.TabIndex = 0;
      this.OptionsDataGrid.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.OptionsDataGrid_EditingControlShowing);
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.HeaderText = "Property Name";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 300;
      // 
      // dataGridViewTextBoxColumn2
      // 
      this.dataGridViewTextBoxColumn2.HeaderText = "Value";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.Width = 200;
      // 
      // CancelBtn
      // 
      this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CancelBtn.Location = new System.Drawing.Point(497, 335);
      this.CancelBtn.Name = "CancelBtn";
      this.CancelBtn.Size = new System.Drawing.Size(75, 23);
      this.CancelBtn.TabIndex = 1;
      this.CancelBtn.Text = "Cancel";
      this.CancelBtn.UseVisualStyleBackColor = true;
      this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
      // 
      // OKBtn
      // 
      this.OKBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.OKBtn.Location = new System.Drawing.Point(416, 335);
      this.OKBtn.Name = "OKBtn";
      this.OKBtn.Size = new System.Drawing.Size(75, 23);
      this.OKBtn.TabIndex = 2;
      this.OKBtn.Text = "OK";
      this.OKBtn.UseVisualStyleBackColor = true;
      this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
      // 
      // NameColumn
      // 
      this.NameColumn.FillWeight = 121.8274F;
      this.NameColumn.HeaderText = "Property Name";
      this.NameColumn.Name = "NameColumn";
      this.NameColumn.ReadOnly = true;
      // 
      // ValueColumn
      // 
      this.ValueColumn.FillWeight = 78.17259F;
      this.ValueColumn.HeaderText = "Value";
      this.ValueColumn.Name = "ValueColumn";
      // 
      // Options
      // 
      this.AcceptButton = this.OKBtn;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.CancelBtn;
      this.ClientSize = new System.Drawing.Size(584, 364);
      this.Controls.Add(this.OptionsDataGrid);
      this.Controls.Add(this.OKBtn);
      this.Controls.Add(this.CancelBtn);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MinimumSize = new System.Drawing.Size(600, 400);
      this.Name = "Options";
      this.Text = "Options";
      ((System.ComponentModel.ISupportInitialize)(this.OptionsDataGrid)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView OptionsDataGrid;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.Button CancelBtn;
    private System.Windows.Forms.Button OKBtn;
    private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ValueColumn;
  }
}