﻿namespace Tasks3
{
  partial class EditTimeline
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.CloseButton = new System.Windows.Forms.Button();
      this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
      this.button2 = new System.Windows.Forms.Button();
      this.button3 = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.button1 = new System.Windows.Forms.Button();
      this.UpdateButton = new System.Windows.Forms.Button();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.EditDuration = new System.Windows.Forms.TextBox();
      this.EditStartTime = new System.Windows.Forms.DateTimePicker();
      this.EditCombo = new System.Windows.Forms.ComboBox();
      this.label5 = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.label9 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.button4 = new System.Windows.Forms.Button();
      this.AddDuration = new System.Windows.Forms.TextBox();
      this.AddStartTime = new System.Windows.Forms.DateTimePicker();
      this.AddCombo = new System.Windows.Forms.ComboBox();
      this.timeLinePanel = new Tasks3.UI.TimeLinePanel();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // CloseButton
      // 
      this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CloseButton.Location = new System.Drawing.Point(688, 652);
      this.CloseButton.Name = "CloseButton";
      this.CloseButton.Size = new System.Drawing.Size(75, 29);
      this.CloseButton.TabIndex = 0;
      this.CloseButton.Text = "Close";
      this.CloseButton.UseVisualStyleBackColor = true;
      this.CloseButton.Click += new System.EventHandler(this.button1_Click);
      // 
      // dateTimePicker1
      // 
      this.dateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.dateTimePicker1.CustomFormat = "dddd dd MMMM yyyy";
      this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dateTimePicker1.Location = new System.Drawing.Point(281, 10);
      this.dateTimePicker1.Name = "dateTimePicker1";
      this.dateTimePicker1.Size = new System.Drawing.Size(239, 22);
      this.dateTimePicker1.TabIndex = 1;
      this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
      // 
      // button2
      // 
      this.button2.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.button2.Location = new System.Drawing.Point(200, 9);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(75, 27);
      this.button2.TabIndex = 2;
      this.button2.Text = "Prev";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // button3
      // 
      this.button3.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.button3.Location = new System.Drawing.Point(528, 9);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(75, 27);
      this.button3.TabIndex = 3;
      this.button3.Text = "Next";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.Controls.Add(this.timeLinePanel);
      this.groupBox1.Location = new System.Drawing.Point(12, 38);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(363, 643);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Timeline";
      // 
      // groupBox2
      // 
      this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox2.Controls.Add(this.button1);
      this.groupBox2.Controls.Add(this.UpdateButton);
      this.groupBox2.Controls.Add(this.label6);
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.label8);
      this.groupBox2.Controls.Add(this.EditDuration);
      this.groupBox2.Controls.Add(this.EditStartTime);
      this.groupBox2.Controls.Add(this.EditCombo);
      this.groupBox2.Controls.Add(this.label5);
      this.groupBox2.Location = new System.Drawing.Point(385, 38);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(377, 162);
      this.groupBox2.TabIndex = 5;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Edit Time period";
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(296, 122);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 29);
      this.button1.TabIndex = 14;
      this.button1.Text = "Delete";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click_1);
      // 
      // UpdateButton
      // 
      this.UpdateButton.Location = new System.Drawing.Point(213, 120);
      this.UpdateButton.Name = "UpdateButton";
      this.UpdateButton.Size = new System.Drawing.Size(75, 31);
      this.UpdateButton.TabIndex = 13;
      this.UpdateButton.Text = "Update";
      this.UpdateButton.UseVisualStyleBackColor = true;
      this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(152, 91);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(56, 13);
      this.label6.TabIndex = 12;
      this.label6.Text = "Duration:";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(6, 91);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(34, 13);
      this.label7.TabIndex = 11;
      this.label7.Text = "Start:";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(6, 58);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(32, 13);
      this.label8.TabIndex = 10;
      this.label8.Text = "Task:";
      // 
      // EditDuration
      // 
      this.EditDuration.Location = new System.Drawing.Point(224, 88);
      this.EditDuration.MaxLength = 4;
      this.EditDuration.Name = "EditDuration";
      this.EditDuration.Size = new System.Drawing.Size(55, 22);
      this.EditDuration.TabIndex = 9;
      this.EditDuration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EditDuration_KeyPress);
      // 
      // EditStartTime
      // 
      this.EditStartTime.CustomFormat = "HH:mm";
      this.EditStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.EditStartTime.Location = new System.Drawing.Point(53, 88);
      this.EditStartTime.Name = "EditStartTime";
      this.EditStartTime.ShowUpDown = true;
      this.EditStartTime.Size = new System.Drawing.Size(82, 22);
      this.EditStartTime.TabIndex = 8;
      // 
      // EditCombo
      // 
      this.EditCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
      this.EditCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
      this.EditCombo.FormattingEnabled = true;
      this.EditCombo.Location = new System.Drawing.Point(51, 55);
      this.EditCombo.Name = "EditCombo";
      this.EditCombo.Size = new System.Drawing.Size(320, 21);
      this.EditCombo.TabIndex = 7;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(6, 27);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(236, 13);
      this.label5.TabIndex = 1;
      this.label5.Text = "Select a time period to edit from the timeline";
      // 
      // groupBox3
      // 
      this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox3.Controls.Add(this.label9);
      this.groupBox3.Controls.Add(this.label3);
      this.groupBox3.Controls.Add(this.label2);
      this.groupBox3.Controls.Add(this.label1);
      this.groupBox3.Controls.Add(this.button4);
      this.groupBox3.Controls.Add(this.AddDuration);
      this.groupBox3.Controls.Add(this.AddStartTime);
      this.groupBox3.Controls.Add(this.AddCombo);
      this.groupBox3.Location = new System.Drawing.Point(385, 206);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(377, 126);
      this.groupBox3.TabIndex = 6;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Add Time period";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(106, 95);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(212, 13);
      this.label9.TabIndex = 8;
      this.label9.Text = "(Overlapping tasks will not be changed)";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(152, 63);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(56, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Duration:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 63);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(34, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "Start:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 26);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(32, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "Task:";
      // 
      // button4
      // 
      this.button4.Location = new System.Drawing.Point(296, 56);
      this.button4.Name = "button4";
      this.button4.Size = new System.Drawing.Size(75, 32);
      this.button4.TabIndex = 3;
      this.button4.Text = "Add";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new System.EventHandler(this.button4_Click);
      // 
      // AddDuration
      // 
      this.AddDuration.Location = new System.Drawing.Point(224, 62);
      this.AddDuration.MaxLength = 4;
      this.AddDuration.Name = "AddDuration";
      this.AddDuration.Size = new System.Drawing.Size(55, 22);
      this.AddDuration.TabIndex = 2;
      this.AddDuration.Text = "15";
      this.AddDuration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EditDuration_KeyPress);
      // 
      // AddStartTime
      // 
      this.AddStartTime.CustomFormat = "HH:mm";
      this.AddStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.AddStartTime.Location = new System.Drawing.Point(53, 59);
      this.AddStartTime.Name = "AddStartTime";
      this.AddStartTime.ShowUpDown = true;
      this.AddStartTime.Size = new System.Drawing.Size(82, 22);
      this.AddStartTime.TabIndex = 1;
      // 
      // AddCombo
      // 
      this.AddCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
      this.AddCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
      this.AddCombo.FormattingEnabled = true;
      this.AddCombo.Location = new System.Drawing.Point(51, 23);
      this.AddCombo.Name = "AddCombo";
      this.AddCombo.Size = new System.Drawing.Size(320, 21);
      this.AddCombo.TabIndex = 0;
      // 
      // timeLinePanel
      // 
      this.timeLinePanel.BackColor = System.Drawing.Color.Transparent;
      this.timeLinePanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.timeLinePanel.Location = new System.Drawing.Point(3, 18);
      this.timeLinePanel.Name = "timeLinePanel";
      this.timeLinePanel.Selected = null;
      this.timeLinePanel.Size = new System.Drawing.Size(357, 622);
      this.timeLinePanel.TabIndex = 0;
      this.timeLinePanel.TimeLine = null;
      this.timeLinePanel.SelectedItemChanged += new System.EventHandler(this.timeLinePanel1_SelectedItemChanged);
      // 
      // EditTimeline
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.CloseButton;
      this.ClientSize = new System.Drawing.Size(774, 688);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.button3);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.dateTimePicker1);
      this.Controls.Add(this.CloseButton);
      this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.MinimumSize = new System.Drawing.Size(500, 400);
      this.Name = "EditTimeline";
      this.Text = "Edit Timeline";
      this.groupBox1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button CloseButton;
    private System.Windows.Forms.DateTimePicker dateTimePicker1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.TextBox AddDuration;
    private System.Windows.Forms.DateTimePicker AddStartTime;
    private System.Windows.Forms.ComboBox AddCombo;
    private System.Windows.Forms.Button UpdateButton;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox EditDuration;
    private System.Windows.Forms.DateTimePicker EditStartTime;
    private System.Windows.Forms.ComboBox EditCombo;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Button button1;
    private UI.TimeLinePanel timeLinePanel;
  }
}