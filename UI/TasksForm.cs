﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3
{
  public class TasksForm : Form
  {
    protected TopForm TopForm { get; private set; }

#if DEBUG
    // For IDE support only
    public TasksForm()
    {
    }
#endif

    public TasksForm(TopForm topForm)
    {
      TopForm = topForm;
    }

    protected Tasks3.Config.Config Config
    {
      get { return TopForm.Config; }
    }

    protected TaskList TaskList
    {
      get { return TopForm.TaskList; }
    }

    protected ITimer Timer
    {
      get { return TopForm.Timer; }
    }
  }
}
