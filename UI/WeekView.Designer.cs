﻿namespace Tasks3
{
  partial class WeekView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeekView));
      this.toolTip = new System.Windows.Forms.ToolTip(this.components);
      this.matchingTaskButton = new System.Windows.Forms.Button();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.topTasksPanel1 = new Tasks3.UI.TopTasksPanel();
      this.DateLabel = new System.Windows.Forms.Label();
      this.BackButton = new System.Windows.Forms.Button();
      this.NextButton = new System.Windows.Forms.Button();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // matchingTaskButton
      // 
      this.matchingTaskButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.matchingTaskButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.matchingTaskButton.Location = new System.Drawing.Point(504, 82);
      this.matchingTaskButton.Name = "matchingTaskButton";
      this.matchingTaskButton.Size = new System.Drawing.Size(248, 23);
      this.matchingTaskButton.TabIndex = 1;
      this.matchingTaskButton.UseVisualStyleBackColor = true;
      this.matchingTaskButton.Visible = false;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tableLayoutPanel1.ColumnCount = 5;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.Controls.Add(this.label6, 4, 0);
      this.tableLayoutPanel1.Controls.Add(this.label5, 3, 0);
      this.tableLayoutPanel1.Controls.Add(this.label4, 2, 0);
      this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 66);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(1260, 631);
      this.tableLayoutPanel1.TabIndex = 11;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label6.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.Color.White;
      this.label6.Location = new System.Drawing.Point(1011, 0);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(246, 40);
      this.label6.TabIndex = 17;
      this.label6.Text = "Friday";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label5.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.ForeColor = System.Drawing.Color.White;
      this.label5.Location = new System.Drawing.Point(759, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(246, 40);
      this.label5.TabIndex = 16;
      this.label5.Text = "Thursday";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.ForeColor = System.Drawing.Color.White;
      this.label4.Location = new System.Drawing.Point(507, 0);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(246, 40);
      this.label4.TabIndex = 15;
      this.label4.Text = "Wednesday";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.White;
      this.label3.Location = new System.Drawing.Point(255, 0);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(246, 40);
      this.label3.TabIndex = 14;
      this.label3.Text = "Tuesday";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.Color.White;
      this.label1.Location = new System.Drawing.Point(3, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(246, 40);
      this.label1.TabIndex = 13;
      this.label1.Text = "Monday";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label2.Location = new System.Drawing.Point(1099, 724);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(121, 17);
      this.label2.TabIndex = 10;
      this.label2.Text = "This week summary";
      // 
      // topTasksPanel1
      // 
      this.topTasksPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.topTasksPanel1.BackColor = System.Drawing.Color.Transparent;
      this.topTasksPanel1.data = null;
      this.topTasksPanel1.Location = new System.Drawing.Point(1033, 754);
      this.topTasksPanel1.MinimumSize = new System.Drawing.Size(200, 100);
      this.topTasksPanel1.Name = "topTasksPanel1";
      this.topTasksPanel1.NumberToShow = 5;
      this.topTasksPanel1.Size = new System.Drawing.Size(239, 134);
      this.topTasksPanel1.TabIndex = 9;
      // 
      // DateLabel
      // 
      this.DateLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.DateLabel.ForeColor = System.Drawing.Color.White;
      this.DateLabel.Location = new System.Drawing.Point(62, 21);
      this.DateLabel.Name = "DateLabel";
      this.DateLabel.Size = new System.Drawing.Size(246, 25);
      this.DateLabel.TabIndex = 14;
      this.DateLabel.Text = "11th November 2012";
      this.DateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // BackButton
      // 
      this.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.BackButton.ForeColor = System.Drawing.SystemColors.ControlLight;
      this.BackButton.Location = new System.Drawing.Point(20, 23);
      this.BackButton.Name = "BackButton";
      this.BackButton.Size = new System.Drawing.Size(36, 23);
      this.BackButton.TabIndex = 15;
      this.BackButton.Text = "<<";
      this.BackButton.UseVisualStyleBackColor = true;
      this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
      // 
      // NextButton
      // 
      this.NextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.NextButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.NextButton.Location = new System.Drawing.Point(314, 23);
      this.NextButton.Name = "NextButton";
      this.NextButton.Size = new System.Drawing.Size(38, 23);
      this.NextButton.TabIndex = 16;
      this.NextButton.Text = ">>";
      this.NextButton.UseVisualStyleBackColor = true;
      this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
      // 
      // WeekView
      // 
      this.AcceptButton = this.matchingTaskButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Black;
      this.ClientSize = new System.Drawing.Size(1284, 900);
      this.ControlBox = false;
      this.Controls.Add(this.NextButton);
      this.Controls.Add(this.BackButton);
      this.Controls.Add(this.DateLabel);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.topTasksPanel1);
      this.Controls.Add(this.matchingTaskButton);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.KeyPreview = true;
      this.Name = "WeekView";
      this.Opacity = 0.9D;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "FullScreen";
      this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.WeekView_KeyUp);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button matchingTaskButton;
    private System.Windows.Forms.ToolTip toolTip;
    private UI.TopTasksPanel topTasksPanel1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label DateLabel;
    private System.Windows.Forms.Button BackButton;
    private System.Windows.Forms.Button NextButton;
  }
}