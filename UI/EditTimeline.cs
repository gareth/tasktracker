﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tasks3.Data;
using Tasks3.Config;

namespace Tasks3
{
  public partial class EditTimeline : Form //qq position
  {
    private Tasks3.Config.Config config;
    private TaskList taskList;

    public EditTimeline(Tasks3.Config.Config config, TaskList taskList)
      : this(DateTime.Now.EffectiveDate(), config, taskList)
    {
    }

    public EditTimeline(DateTime xiDate, Tasks3.Config.Config config, TaskList taskList)
    {
      InitializeComponent();
      this.config = config;
      this.taskList = taskList;

      var tasks = taskList.AllChildTasks.Where(T => !T.Complete && !T.Deleted && !T.Archived);

      EditCombo.Items.AddRange(tasks.OrderBy(T => T.QualifiedName).ToArray());
      EditCombo.DisplayMember = "QualifiedName";

      AddCombo.Items.AddRange(tasks.OrderBy(T => T.QualifiedName).ToArray());
      AddCombo.DisplayMember = "QualifiedName";

      dateTimePicker1.Value = xiDate;
      UpdateTimeline();
    }

    private void UpdateTimeline()
    {
      TimeLine tl = new TimeLine(taskList, dateTimePicker1.Value, config.MinCountablePeriod);
      timeLinePanel.TimeLine = tl; 
    }

    private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
    {
      UpdateTimeline();
    }

    Task mLastAdded;

    private void button2_Click(object sender, EventArgs e)
    {
      dateTimePicker1.Value = dateTimePicker1.Value.AddDays(-1);
    }

    private void button3_Click(object sender, EventArgs e)
    {
      dateTimePicker1.Value = dateTimePicker1.Value.AddDays(1);
    }

    private void EditDuration_KeyPress(object sender, KeyPressEventArgs e)
    {
      if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != (char)Keys.Back)
      {
        e.Handled = true;
      }
    }

    private void UpdateButton_Click(object sender, EventArgs e)
    {
      if (SelectedPeriod != null
        && EditCombo.SelectedItem != null
        && EditDuration.Text != "")
      {
        SelectedPeriod.Start = SelectedPeriod.Start.Date.EffectiveDate().Add(EditStartTime.Value.TimeOfDay);
        SelectedPeriod.End = SelectedPeriod.Start.AddMinutes(int.Parse(EditDuration.Text));
        SelectedPeriod.Task = (Task)EditCombo.SelectedItem;
        timeLinePanel.Refresh();
      }
      else
      {
        MessageBox.Show("Invalid data entered");
      }
    }

    private void button4_Click(object sender, EventArgs e)
    {
      Task lTask = (Task)AddCombo.SelectedItem;
      if (lTask == null && mLastAdded != null && AddCombo.Text == mLastAdded.Description)
      {
        lTask = mLastAdded;
      }

      if (lTask != null &&
        AddDuration.Text != "")
      {
        // BUG: effective date isn't right here if you try and create a task at e.g. 1am
        var start = dateTimePicker1.Value.Date.EffectiveDate().Add(AddStartTime.Value.TimeOfDay);
        lTask.AddTimePeriod(start, start.AddMinutes(int.Parse(AddDuration.Text)));
        timeLinePanel.Refresh();
        mLastAdded = lTask;
      }
      else
      {
        MessageBox.Show("Invalid data entered");
      }        
    }

    private void button1_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void button1_Click_1(object sender, EventArgs e)
    {
      if (SelectedPeriod != null)
      {
        SelectedPeriod.Task.DeleteTimePeriod(SelectedPeriod);
        timeLinePanel.Refresh();
      }
      else
      {
        MessageBox.Show("Invalid data entered");
      }

    }

    private TimePeriod SelectedPeriod { get { return timeLinePanel.Selected; } }

    private void timeLinePanel1_SelectedItemChanged(object sender, EventArgs e)
    {
      EditCombo.Enabled =
      EditStartTime.Enabled =
      EditDuration.Enabled =
      UpdateButton.Enabled = (SelectedPeriod != null);


      if (SelectedPeriod != null)
      {
        EditCombo.SelectedItem = SelectedPeriod.Task;
        EditStartTime.Value = SelectedPeriod.Start;
        EditDuration.Text = SelectedPeriod.Duration.TotalMinutes.ToString("0");
      }
    }
  }
}
