﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Tasks3.UI
{
  public interface IFullScreenUI : IDisposable
  {
    Rectangle Bounds { get; set; }
    bool TopMost { get; set; }
    bool Visible { get; set; }
    void Show();
    void Hide();
    bool Focus();
    event EventHandler VisibleChanged;
  }
}
