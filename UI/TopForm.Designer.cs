﻿namespace Tasks3
{
    partial class TopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
/*          if (mKeyboardHook != null)
          {
            mKeyboardHook.Dispose();
          }
 */ 

          if (registered)
          {
            WTSUnRegisterSessionNotification(Handle);
            registered = false;
          }        

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TopForm));
          this.TaskButton = new System.Windows.Forms.Button();
          this.ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
          this.ChangeParentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
          this.OptionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.BugzillaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.ReassignTimeMenu = new System.Windows.Forms.ToolStripMenuItem();
          this.ImportTSAMenu = new System.Windows.Forms.ToolStripMenuItem();
          this.errorDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
          this.AboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
          this.QuitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.TimeButton = new System.Windows.Forms.Button();
          this.DragHandle = new System.Windows.Forms.Label();
          this.PropertiesButton = new System.Windows.Forms.Button();
          this.AddButton = new System.Windows.Forms.Button();
          this.ClockTimer = new System.Windows.Forms.Timer(this.components);
          this.PointlessFocusButton = new System.Windows.Forms.Button();
          this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
          this.LinkButton = new System.Windows.Forms.Button();
          this.PauseButton = new System.Windows.Forms.Button();
          this.ReassignButton = new System.Windows.Forms.Button();
          this.Time15Mins = new System.Windows.Forms.Button();
          this.Time30Mins = new System.Windows.Forms.Button();
          this.Time45Mins = new System.Windows.Forms.Button();
          this.Time60Mins = new System.Windows.Forms.Button();
          this.Time90Mins = new System.Windows.Forms.Button();
          this.Time120Mins = new System.Windows.Forms.Button();
          this.TimeCustom = new System.Windows.Forms.Button();
          this.CancelReassignButton = new System.Windows.Forms.Button();
          this.MessageButton = new System.Windows.Forms.Button();
          this.ContextMenu.SuspendLayout();
          this.SuspendLayout();
          // 
          // TaskButton
          // 
          this.TaskButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
          this.TaskButton.ContextMenuStrip = this.ContextMenu;
          this.TaskButton.FlatAppearance.BorderSize = 0;
          this.TaskButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.TaskButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.TaskButton.ForeColor = System.Drawing.SystemColors.ControlText;
          this.TaskButton.Location = new System.Drawing.Point(16, 0);
          this.TaskButton.Name = "TaskButton";
          this.TaskButton.Size = new System.Drawing.Size(231, 22);
          this.TaskButton.TabIndex = 6;
          this.TaskButton.Text = "No task selected";
          this.ToolTip.SetToolTip(this.TaskButton, "The currently selected task. Click to change");
          this.TaskButton.UseVisualStyleBackColor = false;
          this.TaskButton.Click += new System.EventHandler(this.TaskButton_Click);
          this.TaskButton.Enter += new System.EventHandler(this.TaskButton_Enter);
          this.TaskButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.TaskButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // ContextMenu
          // 
          this.ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ChangeParentMenuItem,
            this.toolStripSeparator3,
            this.OptionsMenuItem,
            this.BugzillaMenuItem,
            this.ReassignTimeMenu,
            this.ImportTSAMenu,
            this.errorDetailsToolStripMenuItem,
            this.toolStripSeparator1,
            this.AboutMenuItem,
            this.toolStripSeparator2,
            this.QuitMenuItem});
          this.ContextMenu.Name = "ContextMenu";
          this.ContextMenu.Size = new System.Drawing.Size(223, 220);
          // 
          // ChangeParentMenuItem
          // 
          this.ChangeParentMenuItem.Enabled = false;
          this.ChangeParentMenuItem.Name = "ChangeParentMenuItem";
          this.ChangeParentMenuItem.Size = new System.Drawing.Size(222, 22);
          this.ChangeParentMenuItem.Text = "Change Current Task Parent";
          // 
          // toolStripSeparator3
          // 
          this.toolStripSeparator3.Name = "toolStripSeparator3";
          this.toolStripSeparator3.Size = new System.Drawing.Size(219, 6);
          // 
          // OptionsMenuItem
          // 
          this.OptionsMenuItem.Name = "OptionsMenuItem";
          this.OptionsMenuItem.Size = new System.Drawing.Size(222, 22);
          this.OptionsMenuItem.Text = "Options...";
          this.OptionsMenuItem.Click += new System.EventHandler(this.OptionsMenuItem_Click);
          // 
          // BugzillaMenuItem
          // 
          this.BugzillaMenuItem.Name = "BugzillaMenuItem";
          this.BugzillaMenuItem.Size = new System.Drawing.Size(222, 22);
          this.BugzillaMenuItem.Text = "Check Bugzilla / JIRA now";
          this.BugzillaMenuItem.Click += new System.EventHandler(this.BugzillaMenuItem_Click);
          // 
          // ReassignTimeMenu
          // 
          this.ReassignTimeMenu.Name = "ReassignTimeMenu";
          this.ReassignTimeMenu.Size = new System.Drawing.Size(222, 22);
          this.ReassignTimeMenu.Text = "Reassign time periods...";
          this.ReassignTimeMenu.Click += new System.EventHandler(this.ReassignTimeMenu_Click);
          // 
          // ImportTSAMenu
          // 
          this.ImportTSAMenu.Name = "ImportTSAMenu";
          this.ImportTSAMenu.Size = new System.Drawing.Size(222, 22);
          this.ImportTSAMenu.Text = "Import tasks from TSA";
          this.ImportTSAMenu.Click += new System.EventHandler(this.ImportTSAMenu_Click);
          // 
          // errorDetailsToolStripMenuItem
          // 
          this.errorDetailsToolStripMenuItem.Name = "errorDetailsToolStripMenuItem";
          this.errorDetailsToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
          this.errorDetailsToolStripMenuItem.Text = "Error Details...";
          this.errorDetailsToolStripMenuItem.Visible = false;
          this.errorDetailsToolStripMenuItem.Click += new System.EventHandler(this.ShowErrorDetails);
          // 
          // toolStripSeparator1
          // 
          this.toolStripSeparator1.Name = "toolStripSeparator1";
          this.toolStripSeparator1.Size = new System.Drawing.Size(219, 6);
          // 
          // AboutMenuItem
          // 
          this.AboutMenuItem.Name = "AboutMenuItem";
          this.AboutMenuItem.Size = new System.Drawing.Size(222, 22);
          this.AboutMenuItem.Text = "About...";
          this.AboutMenuItem.Click += new System.EventHandler(this.ShowAbout);
          // 
          // toolStripSeparator2
          // 
          this.toolStripSeparator2.Name = "toolStripSeparator2";
          this.toolStripSeparator2.Size = new System.Drawing.Size(219, 6);
          // 
          // QuitMenuItem
          // 
          this.QuitMenuItem.Name = "QuitMenuItem";
          this.QuitMenuItem.Size = new System.Drawing.Size(222, 22);
          this.QuitMenuItem.Text = "Quit";
          this.QuitMenuItem.Click += new System.EventHandler(this.QuitMenuItem_Click);
          // 
          // TimeButton
          // 
          this.TimeButton.BackColor = System.Drawing.SystemColors.Control;
          this.TimeButton.ContextMenuStrip = this.ContextMenu;
          this.TimeButton.FlatAppearance.BorderSize = 0;
          this.TimeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.TimeButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.TimeButton.ForeColor = System.Drawing.SystemColors.ControlText;
          this.TimeButton.Location = new System.Drawing.Point(300, 0);
          this.TimeButton.Name = "TimeButton";
          this.TimeButton.Size = new System.Drawing.Size(58, 22);
          this.TimeButton.TabIndex = 7;
          this.ToolTip.SetToolTip(this.TimeButton, "The time elapsed since the current task was started");
          this.TimeButton.UseVisualStyleBackColor = false;
          this.TimeButton.Click += new System.EventHandler(this.TimeButton_Click);
          this.TimeButton.Enter += new System.EventHandler(this.TaskButton_Enter);
          this.TimeButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.TimeButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // DragHandle
          // 
          this.DragHandle.BackColor = System.Drawing.SystemColors.Control;
          this.DragHandle.ContextMenuStrip = this.ContextMenu;
          this.DragHandle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.DragHandle.ForeColor = System.Drawing.SystemColors.ControlText;
          this.DragHandle.Image = ((System.Drawing.Image)(resources.GetObject("DragHandle.Image")));
          this.DragHandle.Location = new System.Drawing.Point(0, 0);
          this.DragHandle.Name = "DragHandle";
          this.DragHandle.Size = new System.Drawing.Size(14, 22);
          this.DragHandle.TabIndex = 8;
          this.DragHandle.Click += new System.EventHandler(this.DragHandle_Click);
          this.DragHandle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
          this.DragHandle.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.DragHandle.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          this.DragHandle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.OnMouseMove);
          this.DragHandle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnMouseUp);
          // 
          // PropertiesButton
          // 
          this.PropertiesButton.BackColor = System.Drawing.SystemColors.Control;
          this.PropertiesButton.ContextMenuStrip = this.ContextMenu;
          this.PropertiesButton.FlatAppearance.BorderSize = 0;
          this.PropertiesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.PropertiesButton.ForeColor = System.Drawing.SystemColors.Control;
          this.PropertiesButton.Image = global::Tasks3.Properties.Resources.Sites;
          this.PropertiesButton.Location = new System.Drawing.Point(250, 0);
          this.PropertiesButton.Name = "PropertiesButton";
          this.PropertiesButton.Size = new System.Drawing.Size(22, 22);
          this.PropertiesButton.TabIndex = 9;
          this.ToolTip.SetToolTip(this.PropertiesButton, "View / Edit properties of the current task");
          this.PropertiesButton.UseVisualStyleBackColor = false;
          this.PropertiesButton.Click += new System.EventHandler(this.PropertiesButton_Click);
          this.PropertiesButton.Enter += new System.EventHandler(this.TaskButton_Enter);
          this.PropertiesButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.PropertiesButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // AddButton
          // 
          this.AddButton.BackColor = System.Drawing.SystemColors.Control;
          this.AddButton.ContextMenuStrip = this.ContextMenu;
          this.AddButton.FlatAppearance.BorderSize = 0;
          this.AddButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.AddButton.ForeColor = System.Drawing.Color.Transparent;
          this.AddButton.Image = global::Tasks3.Properties.Resources.AddFolder;
          this.AddButton.Location = new System.Drawing.Point(361, 0);
          this.AddButton.Name = "AddButton";
          this.AddButton.Size = new System.Drawing.Size(22, 22);
          this.AddButton.TabIndex = 3;
          this.ToolTip.SetToolTip(this.AddButton, "Add a new task");
          this.AddButton.UseVisualStyleBackColor = false;
          this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
          this.AddButton.Enter += new System.EventHandler(this.TaskButton_Enter);
          this.AddButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.AddButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // ClockTimer
          // 
          this.ClockTimer.Enabled = true;
          this.ClockTimer.Interval = 50;
          this.ClockTimer.Tick += new System.EventHandler(this.ClockTimer_Tick);
          // 
          // PointlessFocusButton
          // 
          this.PointlessFocusButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.PointlessFocusButton.ForeColor = System.Drawing.Color.DimGray;
          this.PointlessFocusButton.Location = new System.Drawing.Point(1, 17);
          this.PointlessFocusButton.Name = "PointlessFocusButton";
          this.PointlessFocusButton.Size = new System.Drawing.Size(1, 1);
          this.PointlessFocusButton.TabIndex = 10;
          this.PointlessFocusButton.UseVisualStyleBackColor = true;
          this.PointlessFocusButton.Visible = false;
          this.PointlessFocusButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.PointlessFocusButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // LinkButton
          // 
          this.LinkButton.BackColor = System.Drawing.SystemColors.Control;
          this.LinkButton.ContextMenuStrip = this.ContextMenu;
          this.LinkButton.FlatAppearance.BorderSize = 0;
          this.LinkButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.LinkButton.ForeColor = System.Drawing.SystemColors.Control;
          this.LinkButton.Image = global::Tasks3.Properties.Resources.Downloads;
          this.LinkButton.Location = new System.Drawing.Point(386, 0);
          this.LinkButton.Name = "LinkButton";
          this.LinkButton.Size = new System.Drawing.Size(22, 22);
          this.LinkButton.TabIndex = 12;
          this.ToolTip.SetToolTip(this.LinkButton, "Go to the resource associated with this task - e.g. bugzilla webpage");
          this.LinkButton.UseVisualStyleBackColor = false;
          this.LinkButton.Visible = false;
          this.LinkButton.Click += new System.EventHandler(this.LinkButton_Click);
          this.LinkButton.Enter += new System.EventHandler(this.TaskButton_Enter);
          this.LinkButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.LinkButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // PauseButton
          // 
          this.PauseButton.BackColor = System.Drawing.SystemColors.Control;
          this.PauseButton.ContextMenuStrip = this.ContextMenu;
          this.PauseButton.FlatAppearance.BorderSize = 0;
          this.PauseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.PauseButton.ForeColor = System.Drawing.SystemColors.Control;
          this.PauseButton.Image = ((System.Drawing.Image)(resources.GetObject("PauseButton.Image")));
          this.PauseButton.Location = new System.Drawing.Point(275, 0);
          this.PauseButton.Name = "PauseButton";
          this.PauseButton.Size = new System.Drawing.Size(22, 22);
          this.PauseButton.TabIndex = 13;
          this.ToolTip.SetToolTip(this.PauseButton, "Pause the current task");
          this.PauseButton.UseVisualStyleBackColor = false;
          this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
          this.PauseButton.Enter += new System.EventHandler(this.TaskButton_Enter);
          this.PauseButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.PauseButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // ReassignButton
          // 
          this.ReassignButton.BackColor = System.Drawing.SystemColors.Control;
          this.ReassignButton.ContextMenuStrip = this.ContextMenu;
          this.ReassignButton.Enabled = false;
          this.ReassignButton.FlatAppearance.BorderSize = 0;
          this.ReassignButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.ReassignButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.ReassignButton.Location = new System.Drawing.Point(16, 57);
          this.ReassignButton.Name = "ReassignButton";
          this.ReassignButton.Size = new System.Drawing.Size(392, 23);
          this.ReassignButton.TabIndex = 18;
          this.ReassignButton.Text = "Add recent time to the current task?";
          this.ToolTip.SetToolTip(this.ReassignButton, "The time elapsed since the current task was started");
          this.ReassignButton.UseVisualStyleBackColor = false;
          this.ReassignButton.Visible = false;
          this.ReassignButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.ReassignButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // Time15Mins
          // 
          this.Time15Mins.BackColor = System.Drawing.SystemColors.Control;
          this.Time15Mins.ContextMenuStrip = this.ContextMenu;
          this.Time15Mins.FlatAppearance.BorderSize = 0;
          this.Time15Mins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.Time15Mins.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.Time15Mins.Location = new System.Drawing.Point(16, 86);
          this.Time15Mins.Name = "Time15Mins";
          this.Time15Mins.Size = new System.Drawing.Size(60, 23);
          this.Time15Mins.TabIndex = 19;
          this.Time15Mins.Tag = "15";
          this.Time15Mins.Text = "15 mins";
          this.ToolTip.SetToolTip(this.Time15Mins, "The time elapsed since the current task was started");
          this.Time15Mins.UseVisualStyleBackColor = false;
          this.Time15Mins.Visible = false;
          this.Time15Mins.Click += new System.EventHandler(this.ReassignButton_Click);
          this.Time15Mins.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.Time15Mins.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // Time30Mins
          // 
          this.Time30Mins.BackColor = System.Drawing.SystemColors.Control;
          this.Time30Mins.ContextMenuStrip = this.ContextMenu;
          this.Time30Mins.FlatAppearance.BorderSize = 0;
          this.Time30Mins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.Time30Mins.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.Time30Mins.Location = new System.Drawing.Point(82, 86);
          this.Time30Mins.Name = "Time30Mins";
          this.Time30Mins.Size = new System.Drawing.Size(60, 23);
          this.Time30Mins.TabIndex = 20;
          this.Time30Mins.Tag = "30";
          this.Time30Mins.Text = "30 mins";
          this.ToolTip.SetToolTip(this.Time30Mins, "The time elapsed since the current task was started");
          this.Time30Mins.UseVisualStyleBackColor = false;
          this.Time30Mins.Visible = false;
          this.Time30Mins.Click += new System.EventHandler(this.ReassignButton_Click);
          this.Time30Mins.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.Time30Mins.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // Time45Mins
          // 
          this.Time45Mins.BackColor = System.Drawing.SystemColors.Control;
          this.Time45Mins.ContextMenuStrip = this.ContextMenu;
          this.Time45Mins.FlatAppearance.BorderSize = 0;
          this.Time45Mins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.Time45Mins.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.Time45Mins.Location = new System.Drawing.Point(148, 86);
          this.Time45Mins.Name = "Time45Mins";
          this.Time45Mins.Size = new System.Drawing.Size(60, 23);
          this.Time45Mins.TabIndex = 21;
          this.Time45Mins.Tag = "45";
          this.Time45Mins.Text = "45 mins";
          this.ToolTip.SetToolTip(this.Time45Mins, "The time elapsed since the current task was started");
          this.Time45Mins.UseVisualStyleBackColor = false;
          this.Time45Mins.Visible = false;
          this.Time45Mins.Click += new System.EventHandler(this.ReassignButton_Click);
          this.Time45Mins.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.Time45Mins.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // Time60Mins
          // 
          this.Time60Mins.BackColor = System.Drawing.SystemColors.Control;
          this.Time60Mins.ContextMenuStrip = this.ContextMenu;
          this.Time60Mins.FlatAppearance.BorderSize = 0;
          this.Time60Mins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.Time60Mins.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.Time60Mins.Location = new System.Drawing.Point(213, 86);
          this.Time60Mins.Name = "Time60Mins";
          this.Time60Mins.Size = new System.Drawing.Size(60, 23);
          this.Time60Mins.TabIndex = 22;
          this.Time60Mins.Tag = "60";
          this.Time60Mins.Text = "1 hr";
          this.ToolTip.SetToolTip(this.Time60Mins, "The time elapsed since the current task was started");
          this.Time60Mins.UseVisualStyleBackColor = false;
          this.Time60Mins.Visible = false;
          this.Time60Mins.Click += new System.EventHandler(this.ReassignButton_Click);
          this.Time60Mins.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.Time60Mins.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // Time90Mins
          // 
          this.Time90Mins.BackColor = System.Drawing.SystemColors.Control;
          this.Time90Mins.ContextMenuStrip = this.ContextMenu;
          this.Time90Mins.FlatAppearance.BorderSize = 0;
          this.Time90Mins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.Time90Mins.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.Time90Mins.Location = new System.Drawing.Point(279, 86);
          this.Time90Mins.Name = "Time90Mins";
          this.Time90Mins.Size = new System.Drawing.Size(60, 23);
          this.Time90Mins.TabIndex = 23;
          this.Time90Mins.Tag = "90";
          this.Time90Mins.Text = "90 mins";
          this.ToolTip.SetToolTip(this.Time90Mins, "The time elapsed since the current task was started");
          this.Time90Mins.UseVisualStyleBackColor = false;
          this.Time90Mins.Visible = false;
          this.Time90Mins.Click += new System.EventHandler(this.ReassignButton_Click);
          this.Time90Mins.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.Time90Mins.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // Time120Mins
          // 
          this.Time120Mins.BackColor = System.Drawing.SystemColors.Control;
          this.Time120Mins.ContextMenuStrip = this.ContextMenu;
          this.Time120Mins.FlatAppearance.BorderSize = 0;
          this.Time120Mins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.Time120Mins.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.Time120Mins.Location = new System.Drawing.Point(348, 86);
          this.Time120Mins.Name = "Time120Mins";
          this.Time120Mins.Size = new System.Drawing.Size(60, 23);
          this.Time120Mins.TabIndex = 24;
          this.Time120Mins.Tag = "120";
          this.Time120Mins.Text = "2 hrs";
          this.ToolTip.SetToolTip(this.Time120Mins, "The time elapsed since the current task was started");
          this.Time120Mins.UseVisualStyleBackColor = false;
          this.Time120Mins.Visible = false;
          this.Time120Mins.Click += new System.EventHandler(this.ReassignButton_Click);
          this.Time120Mins.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.Time120Mins.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // TimeCustom
          // 
          this.TimeCustom.BackColor = System.Drawing.SystemColors.Control;
          this.TimeCustom.ContextMenuStrip = this.ContextMenu;
          this.TimeCustom.FlatAppearance.BorderSize = 0;
          this.TimeCustom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.TimeCustom.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.TimeCustom.Location = new System.Drawing.Point(16, 115);
          this.TimeCustom.Name = "TimeCustom";
          this.TimeCustom.Size = new System.Drawing.Size(306, 23);
          this.TimeCustom.TabIndex = 25;
          this.TimeCustom.Tag = "Custom";
          this.TimeCustom.Text = "Time since end of last activity";
          this.ToolTip.SetToolTip(this.TimeCustom, "The time elapsed since the current task was started");
          this.TimeCustom.UseVisualStyleBackColor = false;
          this.TimeCustom.Visible = false;
          this.TimeCustom.Click += new System.EventHandler(this.ReassignButton_Click);
          this.TimeCustom.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.TimeCustom.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // CancelReassignButton
          // 
          this.CancelReassignButton.BackColor = System.Drawing.SystemColors.Control;
          this.CancelReassignButton.ContextMenuStrip = this.ContextMenu;
          this.CancelReassignButton.FlatAppearance.BorderSize = 0;
          this.CancelReassignButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.CancelReassignButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.CancelReassignButton.Location = new System.Drawing.Point(328, 115);
          this.CancelReassignButton.Name = "CancelReassignButton";
          this.CancelReassignButton.Size = new System.Drawing.Size(80, 23);
          this.CancelReassignButton.TabIndex = 26;
          this.CancelReassignButton.Tag = "120";
          this.CancelReassignButton.Text = "Cancel";
          this.ToolTip.SetToolTip(this.CancelReassignButton, "The time elapsed since the current task was started");
          this.CancelReassignButton.UseVisualStyleBackColor = false;
          this.CancelReassignButton.Visible = false;
          this.CancelReassignButton.Click += new System.EventHandler(this.CancelReassignButton_Click);
          this.CancelReassignButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.CancelReassignButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // MessageButton
          // 
          this.MessageButton.BackColor = System.Drawing.SystemColors.Highlight;
          this.MessageButton.ContextMenuStrip = this.ContextMenu;
          this.MessageButton.FlatAppearance.BorderSize = 0;
          this.MessageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.MessageButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.MessageButton.ForeColor = System.Drawing.SystemColors.HighlightText;
          this.MessageButton.Location = new System.Drawing.Point(16, 28);
          this.MessageButton.Name = "MessageButton";
          this.MessageButton.Size = new System.Drawing.Size(392, 23);
          this.MessageButton.TabIndex = 11;
          this.MessageButton.UseVisualStyleBackColor = false;
          this.MessageButton.Visible = false;
          this.MessageButton.Click += new System.EventHandler(this.MessageButton_Click);
          this.MessageButton.MouseEnter += new System.EventHandler(this.OnMouseOver);
          this.MessageButton.MouseLeave += new System.EventHandler(this.OnMouseLeave);
          // 
          // TopForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.BackColor = System.Drawing.Color.DarkMagenta;
          this.ClientSize = new System.Drawing.Size(456, 214);
          this.ControlBox = false;
          this.Controls.Add(this.PointlessFocusButton);
          this.Controls.Add(this.CancelReassignButton);
          this.Controls.Add(this.Time120Mins);
          this.Controls.Add(this.TimeCustom);
          this.Controls.Add(this.Time90Mins);
          this.Controls.Add(this.Time45Mins);
          this.Controls.Add(this.Time60Mins);
          this.Controls.Add(this.Time30Mins);
          this.Controls.Add(this.ReassignButton);
          this.Controls.Add(this.Time15Mins);
          this.Controls.Add(this.PauseButton);
          this.Controls.Add(this.MessageButton);
          this.Controls.Add(this.PropertiesButton);
          this.Controls.Add(this.DragHandle);
          this.Controls.Add(this.TaskButton);
          this.Controls.Add(this.LinkButton);
          this.Controls.Add(this.AddButton);
          this.Controls.Add(this.TimeButton);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MinimumSize = new System.Drawing.Size(439, 214);
          this.Name = "TopForm";
          this.ShowInTaskbar = false;
          this.Text = "TaskTracker";
          this.TopMost = true;
          this.TransparencyKey = System.Drawing.Color.DarkMagenta;
          this.ContextMenu.ResumeLayout(false);
          this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button TaskButton;
        private System.Windows.Forms.Button TimeButton;
        private System.Windows.Forms.Label DragHandle;
        private System.Windows.Forms.Button PropertiesButton;
        private System.Windows.Forms.ContextMenuStrip ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem OptionsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem QuitMenuItem;
        private System.Windows.Forms.Timer ClockTimer;
        private System.Windows.Forms.Button PointlessFocusButton;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.Button MessageButton;
        private System.Windows.Forms.ToolStripMenuItem BugzillaMenuItem;
        private System.Windows.Forms.Button LinkButton;
        private System.Windows.Forms.ToolStripMenuItem AboutMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Button PauseButton;
        private System.Windows.Forms.ToolStripMenuItem ReassignTimeMenu;
        private System.Windows.Forms.ToolStripMenuItem ChangeParentMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Button ReassignButton;
        private System.Windows.Forms.Button Time15Mins;
        private System.Windows.Forms.Button Time30Mins;
        private System.Windows.Forms.Button Time45Mins;
        private System.Windows.Forms.Button Time60Mins;
        private System.Windows.Forms.Button Time90Mins;
        private System.Windows.Forms.Button Time120Mins;
        private System.Windows.Forms.Button TimeCustom;
        private System.Windows.Forms.Button CancelReassignButton;
        private System.Windows.Forms.ToolStripMenuItem ImportTSAMenu;
        private System.Windows.Forms.ToolStripMenuItem errorDetailsToolStripMenuItem;

    }
}

