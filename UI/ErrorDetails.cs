﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3.UI
{
  public partial class ErrorDetails : Form
  {
    public ErrorDetails(IEnumerable<String> messages)
    {
      InitializeComponent();
      this.ErrorText.Lines = messages.ToArray();
    }

    private void closeButton_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
