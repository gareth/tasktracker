﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tasks3.Data;
using Tasks3.UI;

namespace Tasks3
{
  public partial class WeekView : TasksForm, IFullScreenUI
  {
    private IList<TimeLinePanel> timeLinePanels = new List<TimeLinePanel>();
    private DateTime startOfWeek;

    public WeekView(TopForm topForm) : base(topForm)
    {
      InitializeComponent();
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);

      this.startOfWeek = Utils.GetStartOfWeek();
      populateTimeline();
      base.OnShown(e);
    }

    private void removeOld()
    {
      foreach (Control c in timeLinePanels)
      {
        c.Parent.Controls.Remove(c);
        c.Dispose();
      }

      timeLinePanels.Clear();
    }

    private void populateTimeline()
    {
      SuspendLayout();

      removeOld();
      
      var timelines = new List<TimeLine>();
      
      for (int i = 0; i < 5; ++i)
      {
        timelines.Add(new TimeLine(TaskList, startOfWeek.AddDays(i), Config.MinCountablePeriod));
      }

      //qq effective min hour, accounting for spanning hours
      var min = timelines.Where(tl => tl.TimePeriodsIncludingGaps.Any()).Min(tl => tl.EarliestTrackedTime);
      var max = timelines.Where(tl => tl.TimePeriodsIncludingGaps.Any()).Max(tl => tl.LatestTrackedTime);      

      for (int i = 0; i < 5; ++i)
      {
        var tl = timelines[i];
        var tlp = new TimeLinePanel();
        tlp.TimeLine = tl;
        tlp.Min = min == null ? TimeSpan.FromHours(9) : min.Value;
        tlp.Max = max == null ? TimeSpan.FromHours(18) : max.Value;
        tlp.ForeColor = System.Drawing.Color.White;
        tableLayoutPanel1.Controls.Add(tlp, i, 1);
        tlp.Dock = DockStyle.Fill;
        tlp.Margin = new Padding() { Left = 50 };
        timeLinePanels.Add(tlp);
      }

      PeriodData pd = new PeriodData(TaskList);
      topTasksPanel1.data = pd;

      DateLabel.Text = startOfWeek.ToNiceDateString();

      ResumeLayout(false);
    }

    private void WeekView_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
      {
        Hide();
      }
    }

    private void BackButton_Click(object sender, EventArgs e)
    {
      startOfWeek = startOfWeek.AddDays(-7);
      populateTimeline();
    }

    private void NextButton_Click(object sender, EventArgs e)
    {
      startOfWeek = startOfWeek.AddDays(7);
      populateTimeline();
    }

  }
}
