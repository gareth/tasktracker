﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using MouseKeyboardActivityMonitor;

using Tasks3.Config;
using Tasks3.UI;

namespace Tasks3
{
  using System.Diagnostics;

  public partial class TopForm : Form
  {
    // from wtsapi32.h
    private const int NotifyForThisSession = 0;

    // from winuser.h
    private const int SessionChangeMessage = 0x02B1;
    private const int SessionLockParam = 0x7;
    private const int SessionUnlockParam = 0x8;

    [DllImport("wtsapi32.dll")]
    private static extern bool WTSRegisterSessionNotification(IntPtr hWnd, int dwFlags);

    [DllImport("wtsapi32.dll")]
    private static extern bool WTSUnRegisterSessionNotification(IntPtr hWnd);

    // flag to indicate if we've registered for notifications or not
    private bool registered = false;

    public TaskList TaskList { get; private set; }
    public ITimer Timer { get; private set; }
    public Tasks3.Config.Config Config { get; private set; }
    public IEnumerable<ITaskImporter> importers { get; private set; }
    public ITaskProvider TSAConnector { get; private set; }

    private KeyboardHookListener keyListener;
    private bool constructed;
    private IConfigStore configStore;

    public TopForm(Tasks3.Config.Config Config, IConfigStore configStore, ITimer Timer, IEnumerable<ITaskImporter> importers, TaskList taskList, ITaskProvider provider)
    {
      InitializeComponent();
      this.Config = Config;
      this.configStore = configStore;
      this.Timer = Timer;
      Config.TopForm = this;
      this.importers = importers;
      foreach (ITaskImporter ti in importers)
      {
        ti.TopForm = this;
      }
      this.TaskList = taskList;
      this.TSAConnector = provider;

      new PositionPersister(taskList).Persist(this, false);

      this.keyListener = new KeyboardHookListener(new MouseKeyboardActivityMonitor.WinApi.GlobalHooker());
      keyListener.KeyDown += new KeyEventHandler(HookManager_KeyDown);
      keyListener.Enabled = true;

      this.ShowInTaskbar = Config.ShowInTaskbar;

      RefreshTask();

      this.Timer.NagDue += new EventHandler(Timer_NagDue);
      this.Timer.ShowMessage += new Tasks3.Timer.ShowMessageHandler(Timer_ShowMessage);
      this.Timer.ActiveTaskOrStatusChange += (s, e) => { RefreshTask(); };

      constructed = true;

      var runningTask = AppDomain.CurrentDomain.GetData("AUTOUPDATE_PERSISTENCE_DATA") as string;
      if (!string.IsNullOrEmpty(runningTask))
      {
        var task = taskList.Find(new Guid(runningTask));
        if (task != null)
        {
          SetActiveTask(task, true);
        }
      }
    }

    public ScreenManager ScreenManager { get; set; }

    protected override void OnFormClosing(FormClosingEventArgs e)
    {
      if (Timer.ActiveTask != null && !Timer.Paused)
      {
        AppDomain.CurrentDomain.SetData("AUTOUPDATE_PERSISTENCE_DATA", Timer.ActiveTask.Guid.ToString());
      }

      keyListener.Dispose();
      base.OnFormClosing(e);
    }

    void Timer_ShowMessage(bool persist, string xiMessage)
    {
      ShowMessage(persist, xiMessage);
    }

    void Timer_NagDue(object sender, EventArgs e)
    {
      if (!System.Diagnostics.Debugger.IsAttached)
      {
        ShowTaskSelect();
      }
    }

    void HookManager_KeyDown(object sender, KeyEventArgs e)
    {
      System.Diagnostics.Debug.Write(e.KeyCode);
      Timer.OnKeyPress();
    }

    public void SetActiveTask(Task task)
    {
      SetActiveTask(task, false);
    }
    
    private void SetActiveTask(Task task, bool suppressRecentTimePopup)
    {
      var unchanged = task == Timer.ActiveTask;

      Timer.SetActiveTask(task);

      if (!unchanged)
      {
        RefreshTask();

        LinkButton.Visible = (task != null && task.LinkId != null);

        if (task != null)
        {
          TaskList.AddRecent(task);

          ChangeParentMenuItem.Owner.SuspendLayout();
          ChangeParentMenuItem.DropDownItems.Clear();

          foreach (Task lTask in TaskList.Children.Where(t => !t.Archived && !t.IsTSA).OrderBy(T => T.Description))
          {
            if (!lTask.Complete && !lTask.Deleted)
            {
              ToolStripMenuItem lChild = new ToolStripMenuItem();
              lChild.Text = lTask.Description;
              lChild.Tag = lTask;
              lChild.Click += new EventHandler(SetParent);

              ChangeParentMenuItem.DropDownItems.Add(lChild);
            }
          }

          ChangeParentMenuItem.Owner.ResumeLayout();

          ChangeParentMenuItem.Enabled = true;
        }
        else
        {
          ChangeParentMenuItem.Enabled = false;
        }

        if (task != null && Config.EnableReassignPrompt && !suppressRecentTimePopup)
        {
          ShowReassign();
        }
      }
    }

    private void HideReassign()
    {
      foreach (Button lBut in new[] { Time30Mins, Time15Mins, Time45Mins, Time60Mins, Time90Mins, Time120Mins, TimeCustom, ReassignButton, CancelReassignButton })
      {
        lBut.Visible = false;
      }

      reassignVisible = false;
    }

    private DateTime reassignWindowHideTime = DateTime.MaxValue;
    private bool reassignVisible = false;

    private void ShowReassign()
    {
      reassignVisible = true;

      foreach (Button lBut in new[] { Time30Mins, Time15Mins, Time45Mins, Time60Mins, Time90Mins, Time120Mins, TimeCustom, ReassignButton, CancelReassignButton })
      {
        lBut.Visible = true;
      }

      if (Timer.GetPreviousPeriod() == null)
      {
        if (Timer.StartOfDay != null && Timer.StartOfDay.Value.AddMinutes(5) < DateTime.Now)
        {
          var lDuration = ((TimeSpan)(DateTime.Now - Timer.StartOfDay)).TotalMinutes;
          TimeCustom.Text = string.Format("Time since start of day ({0:0} mins) ",
            lDuration);

          TimeCustom.Tag = ((int)lDuration).ToString();
        }
        else
        {
          TimeCustom.Visible = false;
        }
      }
      else if (Timer.GetPreviousPeriod().End.AddMinutes(5) > DateTime.Now)
      {
        var lDuration = ((TimeSpan)(DateTime.Now - Timer.GetPreviousPeriod().Start)).TotalMinutes;
        TimeCustom.Text = string.Format("Replace previous task:({1:0} mins - {0}) ",
          Timer.GetPreviousPeriod().Task.QualifiedName,
          lDuration);
        TimeCustom.Tag = ((int)lDuration).ToString();
      }
      else
      {
        var lDuration = ((TimeSpan)(DateTime.Now - Timer.GetPreviousPeriod().End)).TotalMinutes;
        TimeCustom.Text = string.Format("Time since end of last activity ({0:0} mins)",
          lDuration);

        mTimeCustomStart = Timer.GetPreviousPeriod().End;
        TimeCustom.Tag = ((int)lDuration).ToString();
      }

      reassignWindowHideTime = DateTime.Now.AddSeconds(5);
    }

    private DateTime? mTimeCustomStart;

    private void SetParent(object xiSender, EventArgs e)
    {
      Timer.ActiveTask.Parent = (Task)((ToolStripMenuItem)xiSender).Tag;
      RefreshTask();
    }

    

    public void RefreshTask()
    {
      if (Timer.ActiveTask != null)
      {
        TaskButton.Text = Timer.ActiveTask.Description;
        TaskButton.BackColor = Timer.ActiveTask.TopLevelTask.Colour;
        TaskButton.ForeColor = TaskButton.BackColor.ComplementaryTextColor();
      }
      else
      {
        TaskButton.Text = "No task selected";
        TaskButton.BackColor = SystemColors.Control;
        TaskButton.ForeColor = SystemColors.ControlText;

        if (!Config.AllowNoTaskAssignment && constructed)
        {
          ShowTaskSelect();
          //nagger.ShowDialog();
        }
      }

      if (Timer.Paused)
      {
        PauseButton.Image = global::Tasks3.Properties.Resources.Play;
        ToolTip.SetToolTip(PauseButton, "Resume the currently paused task");

        if (!Config.AllowNoTaskAssignment)
        {
          ShowTaskSelect();
          //nagger.ShowDialog();
        }
      }
      else
      {
        PauseButton.Image = global::Tasks3.Properties.Resources.Pause;
        ToolTip.SetToolTip(PauseButton, "Pause the current task");
      }
    }

    public void ShowMessage(bool xiPersist, string xiMessage)
    {
      lock (mMessages)
      {
        mMessages.Add(new Message { Text = xiMessage, Persist = xiPersist });
      }
    }

    private List<Message> mMessages = new List<Message>();

    protected override void OnActivated(EventArgs e)
    {
      base.OnActivated(e);

      if (!string.IsNullOrEmpty(Config.TimesheetAppUrl))
      {
        ImportTSAMenu.Visible = true;
      }
      else
      {
      }

      if (this.Visible && Timer.ActiveTask == null && !Config.AllowNoTaskAssignment && constructed)
      {
        ShowTaskSelect();
      }
    }

    /// <summary>
    /// Register for event notifications
    /// </summary>
    protected override void OnHandleCreated(EventArgs e)
    {
      base.OnHandleCreated(e);

      registered = WTSRegisterSessionNotification(Handle, NotifyForThisSession);

      return;
    }

    /// <summary>
    /// Process windows messages
    /// </summary>
    protected override void WndProc(ref System.Windows.Forms.Message m)
    {
      // check for session change notifications
      if (m.Msg == SessionChangeMessage)
      {
        if (m.WParam.ToInt32() == SessionLockParam)
          OnSessionLock();
        else if (m.WParam.ToInt32() == SessionUnlockParam)
          OnSessionUnlock();
      }

      base.WndProc(ref m);
      return;
    }

    /// <summary>
    /// The windows session has been locked
    /// </summary>
    protected void OnSessionLock()
    {
      if (Config.PauseWhenStationLocked)
      {
        Timer.Pause();
      }
    }

    /// <summary>
    /// The windows session has been unlocked
    /// </summary>
    protected void OnSessionUnlock()
    {
      return;
    }

    // Jump to the screen top
    protected override void OnLoad(EventArgs e)
    {
      this.Top = 0;
      this.Left = (Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);

      foreach (Control lControl in new[] { ReassignButton, Time120Mins, Time15Mins, Time30Mins, Time45Mins, Time60Mins, Time90Mins, TimeCustom, CancelReassignButton })
      {
        lControl.Top -= 30;
      }

      base.OnLoad(e);

      VersionHistory.CheckForNewChanges(configStore, this);
    }

    private Point mMouseOffset;
    private Point mFormOffset;
    private Size mShuffleOffset;
    private bool mIsMouseDown;

    private void OnMouseDown(object sender,
        System.Windows.Forms.MouseEventArgs e)
    {
      Timer.OnKeyPress();

      if (e.Button == MouseButtons.Left)
      {
        mMouseOffset = MousePosition;
        mFormOffset = Location;
        mShuffleOffset = new Size(0, 0);

        mIsMouseDown = true;
      }
    }



    private void OnMouseMove(object sender,
      System.Windows.Forms.MouseEventArgs e)
    {
      if (mIsMouseDown)
      {
        Point lMousePos = Control.MousePosition;

        int lShuffleX = lMousePos.X - mMouseOffset.X;
        if (Math.Abs(lShuffleX) < 32 && mShuffleOffset.Width == 0)
        {
          lShuffleX = 0;
        }
        else
        {
          lShuffleX -= mShuffleOffset.Width;
        }

        Location += new Size(lShuffleX, Screen.GetWorkingArea(this).Top);

        mShuffleOffset.Width += lShuffleX;
        mShuffleOffset.Height = lMousePos.Y - mMouseOffset.Y;
      }
    }

    private void OnMouseUp(object sender,
      System.Windows.Forms.MouseEventArgs e)
    {
      // Changes the isMouseDown field so that the form does
      // not move unless the user is pressing the left mouse button.
      if (mIsMouseDown)
      {
        mIsMouseDown = false;

        if (Bounds.Top - Screen.GetWorkingArea(this).Top < 16)
        {
          Location += new Size(0, Screen.GetWorkingArea(this).Top - Bounds.Top);
        }
        if (Screen.GetWorkingArea(this).Right - Bounds.Right < 16)
        {
          Location += new Size(Screen.GetWorkingArea(this).Right - Bounds.Right, 0);
        }
      }
    }

    private void TaskButton_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      ShowTaskSelect();
    }

    private void AddButton_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      new AddTask(false, this).ShowDialog();
    }

    private void QuitMenuItem_Click(object sender, EventArgs e)
    {
      Program.Quit();
    }

    private void AutoHide()
    {
      if (Config.AutoHide && (DateTime.Now - mouseLeft).TotalSeconds > MAX_MOUSE_OUT_SECS && !reassignVisible)
      {
        AutoHide(false);
      }
    }

    private void AutoHide(bool visible)
    {
      this.Top = visible ? 0 : -20;
      if (visible)
      {
        mouseLeft = DateTime.Now;
      }
    }

    private const int MAX_MOUSE_OUT_SECS = 5;
    private DateTime mouseLeft = DateTime.Now;

    private void ClockTimer_Tick(object sender, EventArgs e)
    {
      if (!this.TopMost)
      {
        this.TopMost = true;
      }

      Timer.Tick();

      AutoHide();

      if (Timer.ActiveTask != null)
      {
        if (Timer.Paused)
        {
          TimeButton.Text = "Paused";
        }
        else
        {
          string lText = Timer.GetElapsedTime().ToString(false);
          TimeButton.Text = lText;
        }
      }
      else
      {
        TimeButton.Text = "";
      }

      ProcessMessages();

      if (DateTime.Now > reassignWindowHideTime)
      {
        reassignWindowHideTime = DateTime.MaxValue;
        HideReassign();
      }
      else
      {
        var lBounds = Rectangle.FromLTRB(ReassignButton.Left, ReassignButton.Top, CancelReassignButton.Right, CancelReassignButton.Bottom);
        
        if (lBounds.Contains(this.PointToClient(System.Windows.Forms.Control.MousePosition)))
        {
          reassignWindowHideTime = DateTime.Now.AddSeconds(5);
        }
      }
    }

    private void TaskButton_Enter(object sender, EventArgs e)
    {
      
      PointlessFocusButton.Focus();
    }

    private void PropertiesButton_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
     
      if (Timer.ActiveTask != null)
      {
        new EditTask(Timer.ActiveTask, this).ShowDialog();
      }
      else
      {
        ShowMessage(false, "You must select a task first to view properties");
      }
    }

    private void ProcessMessages()
    {
      if (mCurrent == null)
      {
        if (mMessages.Count > 0)
        {
          mCurrent = mMessages[0];
          mTickCount = 0;
        }
      }
      else
      {
        if (mTickCount == 0)
        {
          AutoHide(true);

          MessageButton.Left = 10 + 366 / 2;
          MessageButton.Width = 5;
          MessageButton.Text = "";
          MessageButton.Visible = true;

          using (Graphics lGraphics = Graphics.FromHwnd(MessageButton.Handle))
          {
            mTextSize = (int)lGraphics.MeasureString(mCurrent.Text, MessageButton.Font).Width;
          }
        }
        else if ((!mCurrent.Persist && mTickCount > 120) || !MessageButton.Visible)
        {
          MessageButton.Visible = false;
          mCurrent = null;
          mTextSize = int.MaxValue;
          Invalidate(true);

          lock (mMessages)
          {
            mMessages.RemoveAt(0);
          }
        }
        else if (mTickCount < 20)
        {
          MessageButton.Width += 16;
          MessageButton.Left -= 8;
         
          if (MessageButton.Width > mTextSize + 30)
          {
            MessageButton.Text = mCurrent.Text;
          }
        }
        else if (mTickCount == 20)
        {
          MessageButton.Text = mCurrent.Text;
        }
        else if (mTickCount == 40)
        {
          MessageButton.Text = mCurrent.Text;
        }
        else if (mTickCount > 100 && !mCurrent.Persist)
        {
          MessageButton.Width -= 16;
          MessageButton.Left += 8;

          if (MessageButton.Width < mTextSize + 30)
          {
            MessageButton.Text = "";
          }

        }

        mTickCount++;
      }
    }

    private Message mCurrent;
    private int mTickCount;
    private int mTextSize = int.MaxValue;

    private class Message
    {
      public string Text { get; set; }
      public bool Persist { get; set; }
    }

    private void TimeButton_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      new TimeDetails(this).Show();      
    }

    private void MessageButton_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      MessageButton.Visible = false;
    }

    private void OptionsMenuItem_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      new Options(this, configStore).Show();
    }

    private void BugzillaMenuItem_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      foreach (ITaskImporter ti in importers)
      {
        ti.ImportTasks(TaskList);
      }
    }

    private void LinkButton_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      Utils.FollowLink(Timer.ActiveTask.LinkId);
    }

    private void ShowAbout(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      new VersionHistory(this).Show();
    }

    private void PauseButton_Click(object sender, EventArgs e)
    {
      Timer.OnKeyPress();
      Timer.PauseResume();
      RefreshTask();

      if (!Timer.Paused && Timer.ActiveTask != null && Config.EnableReassignPrompt)
      {
        ShowReassign();
      }
    }

    private void ReassignTimeMenu_Click(object sender, EventArgs e)
    {
      new EditTimeline(Config, TaskList).ShowDialog();
    }

    
    private void ReassignButton_Click(object sender, EventArgs e)
    {
      var lDuration = int.Parse((string)((Button)sender).Tag);

      Timer.ReassignRecent(TimeSpan.FromMinutes(lDuration));

      HideReassign();      
    }

    public void ShowTaskSelect()
    {
      ScreenManager.Show<FullScreen>();
    }

    private T GetForm<T>(Func<T> constructor) where T : Form 
    {
      
      if (!formInstances.ContainsKey(typeof(T)))
      {
        formInstances.Add(typeof(T), constructor());
      }

      return (T)formInstances[typeof(T)];
    }

    private Dictionary<Type, Form> formInstances = new Dictionary<Type, Form>();

    private void ImportTSAMenu_Click(object sender, EventArgs e)
    {
      TSAConnector.Refresh();
      if (TSAConnector.AllTasks.Count > 2)
      {
        ShowMessage(false, "Imported " + TSAConnector.AllTasks.Count + " TSA Tasks");
      }
      else
      {
        ShowMessage(false, "Unable to import TSA tasks");
      }
    }

    private void OnMouseOver(object sender, EventArgs e)
    {
      AutoHide(true);
      mouseLeft = DateTime.MaxValue;
    }

    private void OnMouseLeave(object sender, EventArgs e)
    {
      mouseLeft = DateTime.Now;
    }

    private List<string> userErrors = new List<string>();
    private eDragHandleStatus DragStatus;

    public void NotifyUserError(string message)
    {
      try
      {
        userErrors.Add(message);

        if (DragStatus == eDragHandleStatus.normal)
        {
          DragStatus = eDragHandleStatus.errors;
          this.Invoke(new Action(() =>
          {
            DragHandle.BackColor = Color.Tomato;
            errorDetailsToolStripMenuItem.Visible = true;
            ToolTip.SetToolTip(DragHandle, "Errors have occured - click for details.");
          }));
        }
      }
      catch (Exception e)
      {
        Trace.Write("unhandled user error: " + message + "\n" + e);
        throw;
      }
    }

    private void ShowErrorDetails(object sender, EventArgs e)
    {
      new ErrorDetails(userErrors).ShowDialog();
      userErrors.Clear();
      DragStatus = eDragHandleStatus.normal;
      this.Invoke(new Action(() =>
                               {
                                 
                                 errorDetailsToolStripMenuItem.Visible = false;
                                 ToolTip.SetToolTip(DragHandle, "");
                               }));
    }

    public void NotifyUpdateAvailable()
    {
      DragStatus = eDragHandleStatus.updates;
      this.Invoke(new Action(() =>
                               {
                                 DragHandle.BackColor = Color.GreenYellow;
                                 ToolTip.SetToolTip(DragHandle, "Updated - click for details.");
                               }));

    }

    private void DragHandle_Click(object sender, EventArgs e)
    {
      DragHandle.BackColor = SystemColors.Control;

      switch (DragStatus)
      {
        case eDragHandleStatus.errors:
          ShowErrorDetails(sender, e);
          return;
        case eDragHandleStatus.updates:
          DragStatus = eDragHandleStatus.normal;
          ShowAbout(sender, e);
          return;
      }
    }

    private enum eDragHandleStatus
    {
      normal,
      updates,
      errors      
    }

    private void CancelReassignButton_Click(object sender, EventArgs e)
    {
      HideReassign();
    }
  }
}
