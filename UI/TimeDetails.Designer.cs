﻿namespace Tasks3
{
  partial class TimeDetails
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ViewByTaskRadio = new System.Windows.Forms.RadioButton();
      this.ViewByTimeRadio = new System.Windows.Forms.RadioButton();
      this.FromDatePicker = new System.Windows.Forms.DateTimePicker();
      this.ToDatePicker = new System.Windows.Forms.DateTimePicker();
      this.PrevWeekButton = new System.Windows.Forms.Button();
      this.NextWeekButton = new System.Windows.Forms.Button();
      this.FromLabel = new System.Windows.Forms.Label();
      this.ToLabel = new System.Windows.Forms.Label();
      this.CloseButton = new System.Windows.Forms.Button();
      this.Splitter = new System.Windows.Forms.SplitContainer();
      this.TasksTreeView = new AdvancedDataGridView.TreeGridView();
      this.TaskColumn = new AdvancedDataGridView.TreeGridColumn();
      this.CodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TotalTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TimePeriodsView = new System.Windows.Forms.DataGridView();
      this.DateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FromColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ToColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.button1 = new System.Windows.Forms.Button();
      this.ExportButton = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.Splitter)).BeginInit();
      this.Splitter.Panel1.SuspendLayout();
      this.Splitter.Panel2.SuspendLayout();
      this.Splitter.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TasksTreeView)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TimePeriodsView)).BeginInit();
      this.SuspendLayout();
      // 
      // ViewByTaskRadio
      // 
      this.ViewByTaskRadio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ViewByTaskRadio.AutoSize = true;
      this.ViewByTaskRadio.Checked = true;
      this.ViewByTaskRadio.Location = new System.Drawing.Point(12, 607);
      this.ViewByTaskRadio.Name = "ViewByTaskRadio";
      this.ViewByTaskRadio.Size = new System.Drawing.Size(95, 17);
      this.ViewByTaskRadio.TabIndex = 1;
      this.ViewByTaskRadio.TabStop = true;
      this.ViewByTaskRadio.Text = "Group by Task";
      this.ViewByTaskRadio.UseVisualStyleBackColor = true;
      this.ViewByTaskRadio.CheckedChanged += new System.EventHandler(this.ViewByTaskRadio_CheckedChanged);
      // 
      // ViewByTimeRadio
      // 
      this.ViewByTimeRadio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ViewByTimeRadio.AutoSize = true;
      this.ViewByTimeRadio.Location = new System.Drawing.Point(113, 607);
      this.ViewByTimeRadio.Name = "ViewByTimeRadio";
      this.ViewByTimeRadio.Size = new System.Drawing.Size(94, 17);
      this.ViewByTimeRadio.TabIndex = 2;
      this.ViewByTimeRadio.Text = "Group by Time";
      this.ViewByTimeRadio.UseVisualStyleBackColor = true;
      // 
      // FromDatePicker
      // 
      this.FromDatePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.FromDatePicker.CustomFormat = "ddd dd MMM yy";
      this.FromDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.FromDatePicker.Location = new System.Drawing.Point(279, 604);
      this.FromDatePicker.Name = "FromDatePicker";
      this.FromDatePicker.Size = new System.Drawing.Size(133, 20);
      this.FromDatePicker.TabIndex = 3;
      this.FromDatePicker.ValueChanged += new System.EventHandler(this.FromDatePicker_ValueChanged);
      // 
      // ToDatePicker
      // 
      this.ToDatePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ToDatePicker.CustomFormat = "ddd dd MMM yy";
      this.ToDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.ToDatePicker.Location = new System.Drawing.Point(462, 604);
      this.ToDatePicker.Name = "ToDatePicker";
      this.ToDatePicker.Size = new System.Drawing.Size(133, 20);
      this.ToDatePicker.TabIndex = 4;
      this.ToDatePicker.ValueChanged += new System.EventHandler(this.ToDatePicker_ValueChanged);
      // 
      // PrevWeekButton
      // 
      this.PrevWeekButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.PrevWeekButton.Location = new System.Drawing.Point(601, 601);
      this.PrevWeekButton.Name = "PrevWeekButton";
      this.PrevWeekButton.Size = new System.Drawing.Size(75, 23);
      this.PrevWeekButton.TabIndex = 5;
      this.PrevWeekButton.Text = "Prev week";
      this.PrevWeekButton.UseVisualStyleBackColor = true;
      this.PrevWeekButton.Click += new System.EventHandler(this.PrevWeekButton_Click);
      // 
      // NextWeekButton
      // 
      this.NextWeekButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.NextWeekButton.Location = new System.Drawing.Point(682, 601);
      this.NextWeekButton.Name = "NextWeekButton";
      this.NextWeekButton.Size = new System.Drawing.Size(75, 23);
      this.NextWeekButton.TabIndex = 6;
      this.NextWeekButton.Text = "Next week";
      this.NextWeekButton.UseVisualStyleBackColor = true;
      this.NextWeekButton.Click += new System.EventHandler(this.NextWeekButton_Click);
      // 
      // FromLabel
      // 
      this.FromLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.FromLabel.AutoSize = true;
      this.FromLabel.Location = new System.Drawing.Point(243, 608);
      this.FromLabel.Name = "FromLabel";
      this.FromLabel.Size = new System.Drawing.Size(30, 13);
      this.FromLabel.TabIndex = 7;
      this.FromLabel.Text = "From";
      // 
      // ToLabel
      // 
      this.ToLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ToLabel.AutoSize = true;
      this.ToLabel.Location = new System.Drawing.Point(426, 607);
      this.ToLabel.Name = "ToLabel";
      this.ToLabel.Size = new System.Drawing.Size(20, 13);
      this.ToLabel.TabIndex = 8;
      this.ToLabel.Text = "To";
      // 
      // CloseButton
      // 
      this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.CloseButton.Location = new System.Drawing.Point(925, 602);
      this.CloseButton.Name = "CloseButton";
      this.CloseButton.Size = new System.Drawing.Size(75, 23);
      this.CloseButton.TabIndex = 9;
      this.CloseButton.Text = "Close";
      this.CloseButton.UseVisualStyleBackColor = true;
      this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
      // 
      // Splitter
      // 
      this.Splitter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.Splitter.Location = new System.Drawing.Point(0, 0);
      this.Splitter.Name = "Splitter";
      // 
      // Splitter.Panel1
      // 
      this.Splitter.Panel1.Controls.Add(this.TasksTreeView);
      // 
      // Splitter.Panel2
      // 
      this.Splitter.Panel2.Controls.Add(this.TimePeriodsView);
      this.Splitter.Size = new System.Drawing.Size(1010, 595);
      this.Splitter.SplitterDistance = 732;
      this.Splitter.TabIndex = 0;
      this.Splitter.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.Splitter_SplitterMoved);
      // 
      // TasksTreeView
      // 
      this.TasksTreeView.AllowUserToAddRows = false;
      this.TasksTreeView.AllowUserToDeleteRows = false;
      this.TasksTreeView.AllowUserToResizeRows = false;
      this.TasksTreeView.BackgroundColor = System.Drawing.SystemColors.Window;
      this.TasksTreeView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TaskColumn,
            this.CodeColumn,
            this.TimeColumn,
            this.TotalTimeColumn});
      this.TasksTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.TasksTreeView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
      this.TasksTreeView.ImageList = null;
      this.TasksTreeView.Location = new System.Drawing.Point(0, 0);
      this.TasksTreeView.MultiSelect = false;
      this.TasksTreeView.Name = "TasksTreeView";
      this.TasksTreeView.RowHeadersVisible = false;
      this.TasksTreeView.Size = new System.Drawing.Size(732, 595);
      this.TasksTreeView.TabIndex = 0;
      this.TasksTreeView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TasksTreeView_CellClick);
      this.TasksTreeView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.TasksTreeView_CellEndEdit);
      this.TasksTreeView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.TasksTreeView_CellValueChanged);
      this.TasksTreeView.CurrentCellChanged += new System.EventHandler(this.TasksTreeView_CurrentCellChanged);
      this.TasksTreeView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.TasksTreeView_EditingControlShowing);
      // 
      // TaskColumn
      // 
      this.TaskColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.TaskColumn.DefaultNodeImage = null;
      this.TaskColumn.HeaderText = "Task";
      this.TaskColumn.Name = "TaskColumn";
      this.TaskColumn.ReadOnly = true;
      this.TaskColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // CodeColumn
      // 
      this.CodeColumn.HeaderText = "Code";
      this.CodeColumn.Name = "CodeColumn";
      this.CodeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.CodeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // TimeColumn
      // 
      this.TimeColumn.HeaderText = "Time (this task)";
      this.TimeColumn.Name = "TimeColumn";
      this.TimeColumn.ReadOnly = true;
      this.TimeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      this.TimeColumn.Width = 150;
      // 
      // TotalTimeColumn
      // 
      this.TotalTimeColumn.HeaderText = "Time (with child tasks)";
      this.TotalTimeColumn.Name = "TotalTimeColumn";
      this.TotalTimeColumn.ReadOnly = true;
      this.TotalTimeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      this.TotalTimeColumn.Width = 150;
      // 
      // TimePeriodsView
      // 
      this.TimePeriodsView.AllowUserToAddRows = false;
      this.TimePeriodsView.AllowUserToDeleteRows = false;
      this.TimePeriodsView.AllowUserToResizeRows = false;
      this.TimePeriodsView.BackgroundColor = System.Drawing.SystemColors.Window;
      this.TimePeriodsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.TimePeriodsView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DateColumn,
            this.FromColumn,
            this.ToColumn,
            this.Duration});
      this.TimePeriodsView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.TimePeriodsView.Location = new System.Drawing.Point(0, 0);
      this.TimePeriodsView.Name = "TimePeriodsView";
      this.TimePeriodsView.ReadOnly = true;
      this.TimePeriodsView.RowHeadersVisible = false;
      this.TimePeriodsView.Size = new System.Drawing.Size(274, 595);
      this.TimePeriodsView.TabIndex = 0;
      // 
      // DateColumn
      // 
      this.DateColumn.HeaderText = "Date";
      this.DateColumn.Name = "DateColumn";
      this.DateColumn.ReadOnly = true;
      this.DateColumn.Width = 55;
      // 
      // FromColumn
      // 
      this.FromColumn.HeaderText = "From";
      this.FromColumn.Name = "FromColumn";
      this.FromColumn.ReadOnly = true;
      this.FromColumn.Width = 55;
      // 
      // ToColumn
      // 
      this.ToColumn.HeaderText = "To";
      this.ToColumn.Name = "ToColumn";
      this.ToColumn.ReadOnly = true;
      this.ToColumn.Width = 45;
      // 
      // Duration
      // 
      this.Duration.HeaderText = "Duration";
      this.Duration.Name = "Duration";
      this.Duration.ReadOnly = true;
      this.Duration.Width = 110;
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.HeaderText = "Date";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 5;
      // 
      // dataGridViewTextBoxColumn2
      // 
      this.dataGridViewTextBoxColumn2.HeaderText = "From";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.ReadOnly = true;
      this.dataGridViewTextBoxColumn2.Width = 5;
      // 
      // dataGridViewTextBoxColumn3
      // 
      this.dataGridViewTextBoxColumn3.HeaderText = "To";
      this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
      this.dataGridViewTextBoxColumn3.ReadOnly = true;
      this.dataGridViewTextBoxColumn3.Width = 5;
      // 
      // dataGridViewTextBoxColumn4
      // 
      this.dataGridViewTextBoxColumn4.HeaderText = "Duration";
      this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
      this.dataGridViewTextBoxColumn4.ReadOnly = true;
      this.dataGridViewTextBoxColumn4.Width = 5;
      // 
      // button1
      // 
      this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button1.Location = new System.Drawing.Point(763, 602);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(78, 23);
      this.button1.TabIndex = 10;
      this.button1.Text = "Edit Details...";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // ExportButton
      // 
      this.ExportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.ExportButton.Location = new System.Drawing.Point(844, 602);
      this.ExportButton.Name = "ExportButton";
      this.ExportButton.Size = new System.Drawing.Size(75, 23);
      this.ExportButton.TabIndex = 11;
      this.ExportButton.Text = "Export...";
      this.ExportButton.UseVisualStyleBackColor = true;
      this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
      // 
      // TimeDetails
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1010, 629);
      this.Controls.Add(this.ExportButton);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.Splitter);
      this.Controls.Add(this.CloseButton);
      this.Controls.Add(this.ToLabel);
      this.Controls.Add(this.FromLabel);
      this.Controls.Add(this.NextWeekButton);
      this.Controls.Add(this.PrevWeekButton);
      this.Controls.Add(this.ToDatePicker);
      this.Controls.Add(this.FromDatePicker);
      this.Controls.Add(this.ViewByTimeRadio);
      this.Controls.Add(this.ViewByTaskRadio);
      this.MinimumSize = new System.Drawing.Size(1024, 400);
      this.Name = "TimeDetails";
      this.Text = "Time Details";
      this.Splitter.Panel1.ResumeLayout(false);
      this.Splitter.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.Splitter)).EndInit();
      this.Splitter.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.TasksTreeView)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TimePeriodsView)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.RadioButton ViewByTaskRadio;
    private System.Windows.Forms.RadioButton ViewByTimeRadio;
    private System.Windows.Forms.DateTimePicker FromDatePicker;
    private System.Windows.Forms.DateTimePicker ToDatePicker;
    private System.Windows.Forms.Button PrevWeekButton;
    private System.Windows.Forms.Button NextWeekButton;
    private System.Windows.Forms.Label FromLabel;
    private System.Windows.Forms.Label ToLabel;
    private System.Windows.Forms.Button CloseButton;
    private AdvancedDataGridView.TreeGridView TasksTreeView;
    private System.Windows.Forms.SplitContainer Splitter;
    private System.Windows.Forms.DataGridView TimePeriodsView;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.DataGridViewTextBoxColumn DateColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn FromColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ToColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn Duration;
    private System.Windows.Forms.Button ExportButton;
    private AdvancedDataGridView.TreeGridColumn TaskColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn CodeColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn TimeColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn TotalTimeColumn;
  }
}