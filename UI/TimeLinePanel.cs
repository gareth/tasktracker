﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Tasks3.Data;

namespace Tasks3.UI
{
  class TimeLinePanel : Panel
  {
    private TimeLine timeline;
    private TimePeriod selected;
    private Label label;
    private Dictionary<Rectangle, TimePeriod> periodAreas = new Dictionary<Rectangle, TimePeriod>();

    public TimeSpan? Min { get; set; }
    public TimeSpan? Max { get; set; }

    public TimeLinePanel()
    {
      this.Controls.Add(label = new Label());
      this.BackColor = Color.Transparent;
      label.BackColor = Color.Transparent;
      label.Click += new EventHandler(label_Click);
      label.Paint += new PaintEventHandler(label_Paint);
      label.Width = this.Width;
      label.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
    }

    public void Refresh()
    {
      Selected = null;
      label.Invalidate();
    }
    
    public event EventHandler SelectedItemChanged;

    public TimePeriod Selected
    {
      get
      {
        return selected;
      }
      set
      {
        if (selected != value)
        {
          selected = value;
          if (SelectedItemChanged != null)
          {
            SelectedItemChanged(this, EventArgs.Empty);
          }
          label.Invalidate();
        }
      }
    }

    public TimeLine TimeLine
    {
      get { return timeline; }
      set { timeline = value; Refresh(); }
    }

    private void label_Click(object sender, EventArgs e)
    {
      Point lPos = label.PointToClient(MousePosition);
      foreach (var item in periodAreas)
      {
        if (item.Key.Contains(lPos))
        {
          Selected = item.Value;
          return;
        }
      }      
    }

    private int normalizeHour(int hour)
    {
      return hour < TimeLine.CUTOFF_HOUR ? hour + 24 : hour;
    }

    private void label_Paint(object sender, PaintEventArgs e)
    {

      if (TimeLine == null)
      {
        return;
      }

      IEnumerable<TimePeriod> periods = TimeLine.TimePeriodsIncludingGaps;


      List<Task> lTasks = new List<Task>();

      int yMax = this.Height;

      periodAreas.Clear();

      if (periods.Count() > 0)
      {
        DateTime lStartDate = TimePeriod.MaxDate(periods.Min(P => P.Start).AddHours(-1), TimeLine.Start);
        lStartDate = lStartDate.Date.AddHours(lStartDate.Hour);

        DateTime lEndDate = TimePeriod.MinDate(periods.Max(P => P.End), TimeLine.End);
        lEndDate = lEndDate.Date.AddHours(lEndDate.Hour + 1);
        
        var axisStart = normalizeHour(lStartDate.Hour);
        var axisEnd = (int)(lEndDate - lStartDate).TotalHours + axisStart;

        var rangeStart = Min == null ? axisStart : Min.Value.Hours;
        var rangeEnd = Max == null ? axisEnd : normalizeHour(Max.Value.Hours) + 1;

        if (rangeEnd == rangeStart)
        {
          rangeEnd = rangeStart + 24;
        }

        float MINUTESCALE = Math.Max(0.5f, (this.Height - 40) / (rangeEnd - rangeStart) / 60f);
        float HOURSCALE = MINUTESCALE * 60f;        

        label.Height = (int)((rangeEnd - rangeStart) * HOURSCALE + 40 /* 20 for total time, 20 for spacing*/);

        Font lFont = label.Font;
        Brush lBlackBrush = new SolidBrush(this.ForeColor);
        Pen lBlackPen = new Pen(this.ForeColor, 2);


        for (int lHour = axisStart; lHour <= axisEnd; ++lHour)
        {
          int y = (int)(HOURSCALE * (lHour - rangeStart));

          e.Graphics.DrawString((lHour % 24).ToString("00") + ":00", lFont, lBlackBrush, new Point(0, y));
          e.Graphics.DrawLine(lBlackPen, 40, y + 10, 80, y + 10);
        }

        var axisYmax = 10 + (axisEnd - rangeStart) * HOURSCALE;
        e.Graphics.DrawLine(lBlackPen, 45, (axisStart - rangeStart) * HOURSCALE, 45, axisYmax);
        e.Graphics.DrawString(TimeLine.TimePeriods.Sum(t => t.Duration).ToString("hh\\:mm") + " hours", lFont, lBlackBrush, new Point(30, (int)axisYmax + 10));

        int x = 60;
        int lLastTaskName = 0;

        DateTime lMaxTime = DateTime.MinValue;

        foreach (TimePeriod period in periods)
        {
          Pen lPen = new Pen(period.Task.Colour, 15);
          Brush lBrush = new SolidBrush(period.Task.Colour);

          DateTime periodstart = TimePeriod.MaxDate(period.Start, TimeLine.Start);
          DateTime periodEnd = TimePeriod.MinDate(period.End, TimeLine.End);

          // move numbers in range 0-4 to 24-28
          var normalizedHour = normalizeHour(periodstart.Hour);  //(periodstart.Hour + (24 - timeline.Start.Hour)) % 24 + 4;

          int y1 = (int)Math.Max(10f, normalizedHour * HOURSCALE + periodstart.Minute * MINUTESCALE + 10 - rangeStart * HOURSCALE);
          int y2 = (int)Math.Min(axisEnd * HOURSCALE + 10, ((int)(periodEnd - lStartDate).TotalHours + axisStart) * HOURSCALE + periodEnd.Minute * MINUTESCALE + 10 - rangeStart * HOURSCALE);

          yMax = Math.Max(yMax, y2);

          if (period == Selected)
          {
            lPen = new Pen(Color.White, 15);
          }

          e.Graphics.DrawLine(lPen, x, y1, x, y2);

          lPen = new Pen(period.Task.LightColor, 2);
          e.Graphics.DrawLine(lPen, x - 7, y1, x - 7, y2);
          e.Graphics.DrawLine(lPen, x - 7, y1, x + 7, y1);
          lPen = new Pen(period.Task.DarkColor, 2);
          e.Graphics.DrawLine(lPen, x + 7, y1, x + 7, y2);
          e.Graphics.DrawLine(lPen, x - 7, y2, x + 7, y2);

          x = 140 - x;

          int y = Math.Max(lLastTaskName + 15, (y1 + y2) / 2 - 10);
          lLastTaskName = y;

          lBrush = new SolidBrush(period.Task.DarkColor);
          e.Graphics.DrawString(period.Task.Description, lFont, lBrush, 91, y + 1);
          lBrush = new SolidBrush(period.Task.LightColor);
          e.Graphics.DrawString(period.Task.Description, lFont, lBrush, 90, y);

          try
          {
            periodAreas.Add(new Rectangle(0, y1, label.Width, y2 - y1), period);
          }
          catch
          {
            // if someone creates identical overlapping periods then we can't accept clicks from both
          }

          lMaxTime = TimePeriod.MaxDate(periodEnd, lMaxTime);

          if (!lTasks.Contains(period.Task))
          {
            lTasks.Add(period.Task);
          }
        }

        

      }

      if (this.AutoScroll != (yMax > this.Height))
      {
        this.AutoScroll = (yMax > this.Height);
      }
    }
  }
}
