﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3
{
  public class PositionPersistingForm : TasksForm
  {
    protected PositionPersistingForm() : base(null)
    {
    }
    
    protected PositionPersistingForm(TopForm topForm) : this(true, topForm)
    {      
    }

    protected PositionPersistingForm(bool xiPersistLocation, TopForm topForm)
      : base(topForm)
    {
      new PositionPersister(topForm.TaskList).Persist(this, !xiPersistLocation);
    }
  }
}
