﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3
{
  public partial class EditTask : PositionPersistingForm
  {
    public EditTask(Task xiTask, TopForm topForm) : base(topForm)
    {
      topForm.TaskList.AddRecent(xiTask);

      InitializeComponent();
      this.Icon = global::Tasks3.Properties.Resources.SitesHigh;
      mTask = xiTask;

      if (!mTask.IsTopLevel)
      {
        ColorLabel.Visible = false;
        ColorButton.Visible = false;
        ColorSample.Visible = false;
      }
      else
      {
        ColorSample.BackColor = mTask.Colour;
      }

      // "Top-level task"
      ParentComboBox.Items.Add(TaskList);
      ParentComboBox.KeyPress += new KeyPressEventHandler(Utils.OnAutoCompleteKeyPress);

      CodeCombo.Items.AddRange(Program.get<ITaskProvider>().AllTasks.ToArray());
      CodeCombo.KeyPress += new KeyPressEventHandler(Utils.OnAutoCompleteKeyPress);

      foreach (Task lTask in TaskList.AllChildTasks.Where(Task => Task.Children.Count > 0 || Task.Parent == TaskList).OrderBy(Task => Task.Description))
      {
        if (mTask != lTask)
        {
          ParentComboBox.Items.Add(lTask);
        }
      }

      this.DescriptionTextBox.Text = mTask.Description;
      this.ParentComboBox.SelectedItem = mTask.Parent;
      this.NotesTextBox.Text = mTask.Notes.Replace("\n", "\r\n").Replace("\n\n", "\n");
      this.CompleteCheckBox.Checked = mTask.Complete;
      this.DeletedCheckBox.Checked = mTask.Deleted;
      this.CodeCombo.Text = mTask.TimesheetCode;
    }

    void CodeCombo_KeyPress(object sender, KeyPressEventArgs e)
    {
      throw new NotImplementedException();
    }

    private void ColorButton_Click(object sender, EventArgs e)
    {
      ColorDialog.Color = ColorSample.BackColor;

      if (ColorDialog.ShowDialog() == DialogResult.OK)
      {
        ColorSample.BackColor = ColorDialog.Color;
      }
    }

    Task mTask;

    private void OKButton_Click(object sender, EventArgs e)
    {
      if (((!mTask.Complete && CompleteCheckBox.Checked) ||
        (!mTask.Deleted && DeletedCheckBox.Checked)) &&
        Timer.ActiveTask == mTask)
      {
        TopForm.SetActiveTask(null);
      }

      mTask.Notes = this.NotesTextBox.Text;
      mTask.Description = this.DescriptionTextBox.Text;
      mTask.Complete = CompleteCheckBox.Checked;
      mTask.Deleted = DeletedCheckBox.Checked;

      if (ParentComboBox.SelectedItem != null)
      {
        mTask.Parent = (Task)ParentComboBox.SelectedItem;
      }
      else if (!String.IsNullOrEmpty(ParentComboBox.Text))
      {
        Task lNewParent = new Task();
        lNewParent.Description = ParentComboBox.Text;
        lNewParent.Parent = TaskList;
        mTask.Parent = lNewParent;
      }
      else
      {
        mTask.Parent = TaskList;
      }

      if (mTask.IsTopLevel)
      {
        mTask.Colour = ColorSample.BackColor;
      }

      if (CodeCombo.SelectedItem != null)
      {
        mTask.TimesheetCode = CodeCombo.SelectedItem.ToString();
      }
      else
      {
        mTask.TimesheetCode = CodeCombo.Text;
      }


      TopForm.RefreshTask();
      this.Close();
    }

    private void CancelBtn_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
