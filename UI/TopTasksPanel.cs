﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tasks3.Data;
using System.Drawing;

namespace Tasks3.UI
{
  class TopTasksPanel : Panel
  {
    private Button toggleButton;
  
    public PeriodData data { get; set; }

    public int NumberToShow { get; set; }

    private bool topTasksOnly = true;

    public TopTasksPanel()
    {
      NumberToShow = 5;
      InitializeComponent();
      this.toggleButton.Click += new EventHandler(toggleButton_Click);
    }

    void toggleButton_Click(object sender, EventArgs e)
    {
      this.topTasksOnly = !topTasksOnly;
      this.toggleButton.Text = topTasksOnly ? "Projects" : "Tasks";
      this.Invalidate();      
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);

      try
      {
        if (data != null)
        {
          var top = data.topTasks(topTasksOnly).Take(NumberToShow);

          if (top.Any())
          {
            var max = top.Max(kv => kv.Key);
            var y = 0;

            foreach (KeyValuePair<TimeSpan, Task> datum in top)
            {
              var pen = new Pen(datum.Value.Colour, 15);
              var brush = new SolidBrush(datum.Value.Colour);
              var width = (int)(datum.Key.TotalSeconds / max.TotalSeconds * this.Width);
              var rect = new Rectangle(0, y, width, this.Height / (NumberToShow + 2));

              e.Graphics.FillRectangle(brush, rect);

              var font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
              var size = e.Graphics.MeasureString(datum.Value.Description, font);
              var textcolor = size.Width < width ? datum.Value.Colour.ComplementaryTextColor() : Color.White;

              Brush stringbrush = new SolidBrush(textcolor);              
              var string_y =  y + (int)((this.Height / (NumberToShow + 2) - size.Height) / 2);

              e.Graphics.DrawString(datum.Value.Description, font, stringbrush, new Point(5 ,string_y));

              var timeString = datum.Key.ToString(@"hh\:mm");
              var timesize = e.Graphics.MeasureString(timeString, font);

              Brush whitebrush = new SolidBrush(Color.White);
              e.Graphics.DrawString(timeString, font, whitebrush, Math.Min(this.Width - (int)timesize.Width - 5, Math.Max(width, (int)size.Width) + 5), string_y);


              y += this.Height / (NumberToShow + 1);
            }

            this.toggleButton.Height = this.Height / (NumberToShow + 1);
            this.toggleButton.Location = new Point(this.Width - this.toggleButton.Width, this.Height - this.toggleButton.Height);            
          }
        }
      }
      catch (Exception ex)
      {
        Utils.LogError(ex);
      }
    }

    private void InitializeComponent()
    {
      this.toggleButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // toggleButton
      // 
      this.toggleButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this.toggleButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
      this.toggleButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDarkDark;
      this.toggleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.toggleButton.ForeColor = System.Drawing.Color.White;
      this.toggleButton.Location = new System.Drawing.Point(0, 0);
      this.toggleButton.Name = "toggleButton";
      this.toggleButton.Size = new System.Drawing.Size(55, 23);
      this.toggleButton.TabIndex = 0;
      this.toggleButton.Text = "Projects";
      this.toggleButton.UseVisualStyleBackColor = true;
      // 
      // TopTasksPanel
      // 
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.toggleButton);
      this.ResumeLayout(false);

    }
  }
}
