﻿namespace Tasks3.UI
{
  partial class ErrorDetails
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorDetails));
      this.ErrorText = new System.Windows.Forms.TextBox();
      this.closeButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // ErrorText
      // 
      this.ErrorText.Dock = System.Windows.Forms.DockStyle.Top;
      this.ErrorText.Location = new System.Drawing.Point(0, 0);
      this.ErrorText.Multiline = true;
      this.ErrorText.Name = "ErrorText";
      this.ErrorText.Size = new System.Drawing.Size(284, 240);
      this.ErrorText.TabIndex = 0;
      // 
      // closeButton
      // 
      this.closeButton.Location = new System.Drawing.Point(197, 246);
      this.closeButton.Name = "closeButton";
      this.closeButton.Size = new System.Drawing.Size(75, 23);
      this.closeButton.TabIndex = 1;
      this.closeButton.Text = "OK";
      this.closeButton.UseVisualStyleBackColor = true;
      this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
      // 
      // ErrorDetails
      // 
      this.AcceptButton = this.closeButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.ClientSize = new System.Drawing.Size(284, 274);
      this.Controls.Add(this.closeButton);
      this.Controls.Add(this.ErrorText);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "ErrorDetails";
      this.Text = "Error Details";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox ErrorText;
    private System.Windows.Forms.Button closeButton;
  }
}