﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3
{
  public partial class Nag : TasksForm
  {

    public Nag(TopForm topForm) : base(topForm)
    {
      InitializeComponent();

      ContinueButton.Enabled = true;

      if (Timer.ActiveTask == null)
      {
          ContinueButton.Text = "Carry on with no task selected";
          ContinueButton.UseVisualStyleBackColor = true;
          //ContinueButton.BackColor = SystemColors.Control;
          ContinueButton.ForeColor = Color.Black;

        if (!Config.AllowNoTaskAssignment)
        {
          ContinueButton.Enabled = false;
        }
      }
      else
      {
        if (Timer.Paused)
        {
          ContinueButton.Text = "Leave task paused: " + Timer.ActiveTask.Description;
          if (!Config.AllowNoTaskAssignment)
          {
            ContinueButton.Enabled = false;
          }

        }
        else
        {
          ContinueButton.Text = "Carry on with active task: " + Timer.ActiveTask.Description;
        }

        ContinueButton.UseVisualStyleBackColor = false;
        ContinueButton.BackColor = Timer.ActiveTask.Colour;
        ContinueButton.ForeColor = ContinueButton.BackColor.GetBrightness() > 0.45 ? Color.Black : Color.White;          
      }

      if (Timer.ActiveTask != null && Timer.Paused)
      {
        UnpauseButton.Visible = true;
        //Height = 157;
      }
      else
      {
        UnpauseButton.Visible = false;
        //Height = 127;
      }
    }

    private void ContinueButton_Click(object sender, EventArgs e)
    {
      Timer.ResetLastNag();
      Close();
    }

    private void UnpauseButton_Click(object sender, EventArgs e)
    {
      Timer.ResetLastNag();
      Timer.PauseResume();
      Close();
    }

    private void ChangeButton_Click(object sender, EventArgs e)
    {
      Timer.ResetLastNag();
      TopForm.ShowTaskSelect();
      Close();
    }

    private void ContinueButton_KeyPress(object sender, KeyPressEventArgs e)
    {
    }

    private void ChangeButton_KeyDown(object sender, KeyEventArgs e)
    {
    }

    private void Nag_Shown(object sender, EventArgs e)
    {
    }
  }
}
