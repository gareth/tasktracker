﻿using System;
namespace Tasks3
{
  public interface ITaskImporter
  {
    bool Enabled { get; }
    void ImportTasks(TaskList taskList);
    TopForm TopForm { get; set; }
    TimeSpan ImportFrequency { get; }
  }
}
