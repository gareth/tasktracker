﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using StoryQ;

namespace Tasks3.tests
{
  [TestFixture]
  public class AutoCompleteTests : BDDTest
  {
    [Test]
    public void AutocompleteMatches()
    {
      BDDHelpers.WithStory("Autocomplete matches existing task by description")
        .Given(AnEmptyTaskList)
        .And(NoTsaTasks)
        .And(ATaskWithName_, "Parent1")
        .And(ATaskWithName_, "Parent2")
        .And(ATaskWithName_, "Parent1.ChildA")
        .And(ATaskWithName_, "Parent2.ChildB")
        .When()
        .Then(TheAutoCompleteSuggestionFor_ShouldBe_, "Par", "Parent1")
        .And(TheAutoCompleteSuggestionFor_ShouldBe_, "Ch", "ChildA")
        .ExecuteWithReport();
    }

    [Test]
    public void DeletedNotMatched()
    {
      BDDHelpers.WithStory("Autocomplete doesn't match deleted tasks")
        .Given(AnEmptyTaskList)
        .And(NoTsaTasks)
        .And(ADeletedTaskWithName_, "_Deleted")
        .When()
        .Then(TheAutoCompleteSuggestionFor_ShouldBe_, "_", (string)null)
        .ExecuteWithReport();      
    }

    protected void NoTsaTasks()
    {
      tsaTask = null;
    }

    private string tsaTask;

    protected void AMatchingTsaTaskWithCode_(string name)
    {
      tsaTask = name;
    }

    private void TheAutoCompleteSuggestionFor_ShouldBe_(string text, string suggests)
    {
      var tsa = new Mock<TimeSheetAppConnector>(new Config());
      tsa.Setup(t => t.GetBestMatch(It.IsAny<string>())).Returns(tsaTask);
      var ac = new AutoComplete(taskList, tsa.Object);

      Assert.AreEqual(suggests, ac.Match(text));      
    }

    /*private void TheAutoCompleteBoxSuggestionFor_ShouldBe_(string text, string suggests)
    {
      var tsa = new Mock<TimeSheetAppConnector>();
      Program.Kernel.Bind<ITaskProvider>().ToConstant(tsa.Object);
      var acb = new AutoCompleteTextBox();
      acb.Text = "text";
      acb.

      Assert.AreEqual(acb.Text, );
    }*/

    /*[Test]
    public void AutocompletePreservesCase()
    {
      BDDHelpers.WithStory("Autocomplete preserves case after ignoring match")
        .Given(AnEmptyTaskList)
        .And(NoTsaTasks)
        .And(ATaskWithName_, "ALLCAPS")
        .When()
        .Then(TheAutoCompleteBoxSuggestionFor_ShouldBe_, "all", "allCAPS")
        .ExecuteWithReport();
    }
    */

    

    // Test that autocomplete matches a task with the same description

    // Test that autocomplete matches a TSA task with the same code even if it doesn't exist in TT, but only if there isn't already a matching TT task
  }
}
