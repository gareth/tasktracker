﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using NUnit.Framework;


namespace Tasks3.tests
{
  [Binding]
  public class TimerSteps
  {
    private Timer timer;
    private DateTime current;
    private Config.Config config;
    private TaskList tasks;
    private Task task;

    private TimeSpan naginterval;

    [BeforeScenario]
    public void Before()
    {
      naginterval = new Config.Config().NagInterval;
    }

    [AfterScenario]
    public void After()
    {
      new Config.Config().NagInterval = naginterval;
    }

    [Given("a timer")]
    public void GivenATimer()
    {
      config = new Config.Config();
      tasks = new TaskList(config, null);
      task = tasks.AddChild("test");
      current = DateTime.Now;
      timer = new Timer(current, tasks, config, new ITaskImporter[] {});
    }

    [Given("the nag interval is (.*) seconds")]
    public void GivenNagInterval(int seconds)
    {
      config.NagInterval = TimeSpan.FromSeconds(seconds);
    }

    [When("I select a task")]
    [When("I select the same task")]
    public void ISelectATask()
    {
      timer.SetActiveTask(task, current);
    }

    [When("I wait (.*) seconds")]
    public void ISelectATask(int seconds)
    {
      current = current.AddSeconds(seconds);
      timer.Tick(current);
    }

    [When("I press a key")]
    public void IPressAKey()
    {
      timer.OnKeyPress(current);
    }

    [When("I wait until it automatically pauses")]
    public void IWaitForAutopause()
    {
      current = current.AddSeconds(1) + config.AutoPauseInterval;
      timer.Tick(current);
    }




    


    [Then("the (.*) event should fire after (.*) seconds")]
    public void EventShouldFire(string eventName, int seconds)  
    {
      

      var fired = false;

      switch (eventName.ToLower())
      {
        case "nag":
          timer.NagDue += (s, e) => fired = true;
          break;
        default:
          throw new NUnit.Framework.InconclusiveException("unrecognised event " + eventName);
      }

      timer.Tick(current.AddSeconds(seconds - 1));
      Assert.False(fired);

      current = current.AddSeconds(seconds + 1);

      timer.Tick(current);
      Assert.True(fired);      

    }

    [When(@"I pause the task")]
    public void WhenIPauseTheTask()
    {
      timer.Pause(current);
    }

    [When(@"I resume the task")]
    public void WhenIResumeTheTask()
    {
      timer.PauseResume(current);
    }


    [Then(@"the task should have have a total of (.*) seconds tracked")]
    public void ThenTheTaskShouldHaveHaveATotalOfSecondsTracked(int p0)
    {
      Assert.Less(Math.Abs(timer.ActiveTask.GetTotalTime().TotalSeconds - p0), 1);
    }

  }
}
