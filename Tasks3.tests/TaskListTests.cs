﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using StoryQ;

namespace Tasks3.tests
{
  [TestFixture]
  public class TaskListTests : BDDTest
  {

    [Test]
    public void TestMultipleTaskCodes()
    {
      
    }

    [Test]
    public void TestQuickTaskWithNewParent()
    {
      BDDHelpers.WithStory("Quick task creates task and parent")
        .Given(AnEmptyTaskList)
        .And(ATaskWithName_, "Random")
        .When(IAddAQuickTaskWithName_, "parent:child")
        .Then(ThereShouldBeATaskWithName_, "parent")
        .And(ThereShouldBeATaskWithName_, "parent:child")
        .And(TheParentOfTask_ShouldBe_, "parent:child", "parent")
        .ExecuteWithReport();      
    }

    [Test]
    public void TestQuickTaskWithExistingParent()
    {
      BDDHelpers.WithStory("Quick task creates task under existing parent")
        .Given(AnEmptyTaskList)
        .And(ATaskWithName_, "parent")
        .When(IAddAQuickTaskWithName_, "parent:child")
        .Then(ThereShouldBeATaskWithName_, "parent")
        .And(ThereShouldBeATaskWithName_, "parent:child")
        .And(TheParentOfTask_ShouldBe_, "parent:child", "parent")
        .ExecuteWithReport();
    }

    [Test]
    public void TestQuickTaskWithCommonParent()
    {
      BDDHelpers.WithStory("Quick task creates task under common parent for existing tasks with same prefix")
        .Given(AnEmptyTaskList)
        .And(ATaskWithName_, "p")
        .And(ATaskWithName_AndTimeSheetCode_, "p.c1", "parent:c1")
        .When(IAddAQuickTaskWithName_, "parent:c2")
        .Then(ThereShouldBeATaskWithName_, "parent:c2")
        .And(TheParentOfTask_ShouldBe_, "parent:c2", "p")
        .ExecuteWithReport();
    }

    public void IAddAQuickTaskWithName_(string name)
    {
      Task task = new Task();
      task.Description = name;
      task.TimesheetCode = name;
      taskList.AddQuickTask(task);
    }

  }
}
