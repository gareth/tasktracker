﻿Feature: Pause
	In order to not track time away from my desk
	As a user
	I want to be able to pause the current task

Scenario: Pause
  Given a timer
	When I select a task
	And I wait 1000 seconds
	And I pause the task
	Then the task should have have a total of 1000 seconds tracked

Scenario: Resume doesn't count intervening time
  Given a timer
	When I select a task
	And I wait 1000 seconds
	And I pause the task
	And I wait 1000 seconds
	And I resume the task
	And I wait 1000 seconds
	Then the task should have have a total of 2000 seconds tracked

Scenario: Re-selecting the same task when paused doesn't count intervening time
  Given a timer
	When I select a task
	And I wait 1000 seconds
	And I pause the task
	And I wait 1000 seconds
	And I select the same task
	And I wait 1000 seconds
	Then the task should have have a total of 2000 seconds tracked

Scenario: Time tracked is correct after autopause
  Given a timer
	When I select a task
	And I wait 1000 seconds
	And I press a key
	And I wait until it automatically pauses
	Then the task should have have a total of 1000 seconds tracked

