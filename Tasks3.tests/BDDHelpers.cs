﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using StoryQ;

namespace Tasks3.tests
{
  public abstract class BDDTest
  {
    protected TaskList taskList { get; private set; }

    protected BDDTest()
    {
      taskList = new TaskList(new Config.Config(), null);
    }

    protected void AnEmptyTaskList()
    {
      taskList = new TaskList(new Config.Config(), null);      
    }

    protected void ATaskWithName_(string name)
    {
      CreateTask(name);

    }

    protected void ATaskWithName_AndTimeSheetCode_(string name, string code)
    {
      CreateTask(name).TimesheetCode = code;
    }

    protected void ADeletedTaskWithName_(string name)
    {
      CreateTask(name).Deleted = true;      
    }

    private Task CreateTask(string name)
    {
      Task parent = taskList;

      if (name.Contains("."))
      {
        string parentName = name.Split('.', 2)[0];
        name = name.Split('.', 2)[1];
        parent = taskList.FindByDescription(parentName).FirstOrDefault();
      }

      var child = new Task();
      child.Description = name;
      parent.AddChild(child);
      return child;
    }

    protected void ThereShouldBeATaskWithName_(string name)
    {
      Assert.AreEqual(1, taskList.FindByDescription(name).Count());      
    }
    
    protected void TheParentOfTask_ShouldBe_(string child, string parent)
    {
      Assert.AreEqual(parent, taskList.FindByDescription(child).FirstOrDefault().Parent.Description);      
    }

  }

  public static class BDDHelpers
  {
    public static Scenario WithStory(string name)
    {
      return new Story(name)
        .InOrderTo("...")
        .AsA("...")
        .IWant("...")
        .WithScenario("...");
    }

    public static Operation When(this Condition condition)
    {
      return condition.When(IDoNothing);
    }

    public static void IDoNothing()
    {
    }
  }
}
