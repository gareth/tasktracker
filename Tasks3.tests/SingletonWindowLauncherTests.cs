﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Windows.Forms;
using Moq;
using System.Threading;

namespace Tasks3.tests
{
  [TestFixture]
  [Ignore]
  public class SingletonWindowLauncherTests
  {
    private int formCount = 0;

    private Form getForm()
    {
      formCount++;
      var mock = new Mock<Form>();
      mock.Setup(f => f.ShowDialog()).Callback(() => Thread.Sleep(100));
      return mock.Object;
    }

    [Test]
    public void testConcurrent()
    {
      SingletonWindowLauncher swl = new SingletonWindowLauncher(getForm);

      ThreadPool.QueueUserWorkItem(obj => swl.ShowDialog());
      ThreadPool.QueueUserWorkItem(obj => swl.ShowDialog());
      ThreadPool.QueueUserWorkItem(obj => swl.ShowDialog());

      Thread.Sleep(200);

      Assert.AreEqual(1, formCount);
    }

    

  }
}
