﻿Feature: Nag
	In order to avoid tracking the wrong task
	As a user
	I want to be reminded to check my task assignment periodically

Scenario: Nag periodically
  Given a timer
	And the nag interval is 60 seconds
	When I select a task
	And I wait 30 seconds
	And I press a key
	Then the nag event should fire after 30 seconds

Scenario: Don't nag while typing
	Given a timer
	And the nag interval is 60 seconds
	When I select a task
	And I wait 60 seconds
	And I press a key
	Then the nag event should fire after 10 seconds

Scenario: Don't nag just after starting
	Given a timer
	And the nag interval is 60 seconds
	When I select a task
	And I wait until it automatically pauses
	And I press a key
	Then the nag event should fire after 30 seconds  

@ignore
Scenario: Nag dialog should hide on auto-pause
	Given a timer
	And an active nag dialog
	And I wait until it automatically pauses
	Then the nag dialog should close itself