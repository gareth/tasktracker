﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.tests
{
  internal static class TestHelpers
  {
    public static Task AddChild(this Task xiTask, string xiNameAndCode)
    {
      var lTask = new Task() { Description = xiNameAndCode, TimesheetCode = xiNameAndCode };
      xiTask.AddChild(lTask);
      return lTask;
    }

    public static Task AddTime(this Task xiTask, DateTime xiFrom, int xiMinutes)
    {
      xiTask.AddTimePeriod(xiFrom, xiFrom.AddMinutes(xiMinutes));
      return xiTask;
    }

    public static Task AddTime(this Task xiTask, int xiHours, int xiMinutes, int xiDurationInMinutes)
    {
      var lStart = DateTime.Today.AddHours(xiHours).AddMinutes(xiMinutes);
      xiTask.AddTimePeriod(lStart, lStart.AddMinutes(xiDurationInMinutes));
      return xiTask;
    }
  }
}
