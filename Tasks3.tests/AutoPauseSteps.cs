﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;
using Tasks3.Timers;

namespace Tasks3.tests
{
  [Binding]
  public class AutoPauseSteps
  {
    private PauseTimer timer;
    private DateTime now;
    private bool hasFired;

    [Given(@"I have a pause timer with a (.*) min timeout")]
    public void GivenIHaveAPauseTimerWithAMinTimeout(int p0)
    {
      now = DateTime.Now; 
      timer = new PauseTimer(now, TimeSpan.FromMinutes(p0));
      timer.tick(now);
      hasFired = false;
      timer.PauseDue += new EventHandler((s, e) => hasFired = true);
    }

    [When(@"I do nothing")]
    public void WhenIDoNothing()
    {
    }

    [Then(@"the the autopause event should fire after (.*) minutes")]
    public void ThenTheTheAutopauseEventShouldFireAfterMinutes(int p0)
    {
      advanceTimer(p0 - 1);
      Assert.False(hasFired);
      advanceTimer(2);
      Assert.True(hasFired);
    }

    private void advanceTimer(int minutes)
    {
      now = now.AddMinutes(minutes);
      timer.tick(now);
    }
  }
}
