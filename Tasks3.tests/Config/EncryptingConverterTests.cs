﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Tasks3.Config;

namespace Tasks3.tests
{
  [TestFixture]
  public class EncryptingConverterTests
  {
    [Test]
    public void RoundTrip()
    {
      var ec = new EncryptingConverter();
      string input = "input";

      Assert.AreNotEqual(input, ec.convertLeft(input), "input");
      Assert.AreEqual(input, (string)ec.convertRight(ec.convertLeft("input")), "input");

    }
  }
}
