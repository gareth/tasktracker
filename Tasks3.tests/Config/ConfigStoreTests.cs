﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Tasks3.Config;

namespace Tasks3.tests
{
  [TestFixture]
  public class ConfigStoreTests
  {
    [Test]
    public void testString()
    {
      var store = new ConfigStore();
      var JiraUser = store.get<string>(eConfig.jiraUser);

      var old = JiraUser.Value;

      JiraUser.Value = "new";

      Assert.AreEqual("new", JiraUser.Value);
      Assert.AreEqual("new", new ConfigStore().get<string>(eConfig.jiraUser).Value);

      JiraUser.Value = old;
    }

    [Test]
    public void testEncryptedString()
    {
      var store = new ConfigStore();
      var value = store.get<SecureString>(eConfig.jiraPassword);

      var old = value.Value;

      value.Value = "new";

      Assert.AreEqual("new", (string)value.Value);
      Assert.AreEqual("new", (string)new ConfigStore().get<SecureString>(eConfig.jiraPassword).Value);

      value.Value = old;
    }
  }
}
