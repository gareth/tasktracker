﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Tasks3.Config;

namespace Tasks3.tests
{
  [TestFixture]
  public class RegistryTests
  {
    private void ReadWriteTest<T>(IConfigAccessor<T> toTest, T val1, T val2) 
    {
      toTest.persist("test", val1);
      T output;
      Assert.IsTrue(toTest.retrieve("test", out output));
      Assert.AreEqual(val1, output);

      toTest.persist("test", val2);
      Assert.IsTrue(toTest.retrieve("test", out output));
      Assert.AreEqual(val2, output);

      Assert.IsFalse(toTest.retrieve("spurious", out output));

    }

    [Test]
    public void ReadWriteInts()
    {
      ReadWriteTest(new RegistryIntAccessor(), 6, 7);
    }

    [Test]
    public void ReadWriteStrings()
    {
      ReadWriteTest(new RegistryStringAccessor(), "6", "7");
    }

  }
}
