﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Tasks3;

namespace Tasks3.tests
{
  [TestFixture]
  public class TaskTrackerExportTests
  {
    private TimeSheetAppConnector tsaConnector = new TimeSheetAppConnector(new Config.Config());

    [Test]
    public void TestMultipleTaskCodes()
    {
      Task lParent = new TaskList(new Config.Config(), null);
      lParent.AddChild("Code1;Code2").AddTime(0, 0, 20).AddTime(1, 0, 20);
      lParent.AddChild("Code2").AddTime(2, 0, 20);
      lParent.AddChild(";;").AddTime(0, 0, 30);

      var lData = tsaConnector.GetTimeData(lParent, DateTime.Today);
      Assert.AreEqual(20, lData["Code1"].TotalMinutes);
      Assert.AreEqual(40, lData["Code2"].TotalMinutes);
      Assert.AreEqual(3, lData.Count);
    }

    [Test]
    public void TestDivideAmongChildren()
    {

      Task lParent = new TaskList(new Config.Config(), null);
      var lChild = lParent.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN).AddTime(0, 0, 90);
      lChild.AddChild("Code1").AddTime(0, 0, 10);
      lChild.AddChild("Code2").AddTime(0, 0, 20);

      /*
       * P = 90
       *   C1 = 10 + (30)
       *   C2 = 20 + (60)
       */

      var lData = tsaConnector.GetTimeData(lParent, DateTime.Today);
      Assert.AreEqual(2, lData.Count);
      Assert.AreEqual(40, lData["Code1"].TotalMinutes);
      Assert.AreEqual(80, lData["Code2"].TotalMinutes);

      var lGrandChild = lChild.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN).AddTime(0, 0, 30);
      lGrandChild.AddChild("Code3").AddTime(0, 0, 10);
      lGrandChild.AddChild("Code4").AddTime(0, 0, 20);

      /*
       * P = 90
       *   C1 = 10 + (10)
       *   C2 = 20 + (20)
       *   C  = 30 + (60)
       *     C3 = 10 + (30)
       *     C4 = 20 + (60)
       */


      lData = tsaConnector.GetTimeData(lParent, DateTime.Today);
      Assert.AreEqual(4, lData.Count);
      Assert.AreEqual(20, lData["Code1"].TotalMinutes);
      Assert.AreEqual(40, lData["Code2"].TotalMinutes);
      Assert.AreEqual(40, lData["Code3"].TotalMinutes);
      Assert.AreEqual(80, lData["Code4"].TotalMinutes);      
      Assert.AreEqual(lData.Values.Sum().TotalMinutes, lParent.GetTimeWithChildren(DateTime.Today, DateTime.Today.AddDays(7)).TotalMinutes);

      lParent = new TaskList(new Config.Config(), null);
      var lChild1 = lParent.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN).AddTime(0, 0, 100);
      var lChild2 = lParent.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN).AddTime(0, 0, 200);
      lChild1.AddChild("Code1").AddTime(0, 0, 10);
      lChild2.AddChild("Code2").AddTime(0, 0, 20);

      /*
       * P = 100
       *   C1 = 10 + (100)
       * P = 200
       *   C2 = 20 + (200)
       */

      lData = tsaConnector.GetTimeData(lParent, DateTime.Today);
      Assert.AreEqual(2, lData.Count);
      Assert.AreEqual(110, lData["Code1"].TotalMinutes);
      Assert.AreEqual(220, lData["Code2"].TotalMinutes);

      lParent = new TaskList(new Config.Config(), null);
      lParent.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_ALL).AddTime(0, 0, 150);
      lChild1 = lParent.AddChild("Code1").AddTime(0, 0, 10);
      lChild2 = lParent.AddChild("Code2");
      var lChild3 = lChild2.AddChild("Code3").AddTime(0, 0, 10);
      var lChild4 = lParent.AddChild("Code4").AddTime(0, 0, 5);
      lChild4.AddChild("Code5").AddTime(0, 0, 5);

      /*
       * P = 150
       *   C1 = 10 + (50)
       *   C2 = 0 + (0)
       *     C3 = 10 + (50)
       *   C4 = 5 + (25)
       *     C5 = 5 + (25)
       */

      lData = tsaConnector.GetTimeData(lParent, DateTime.Today);
      Assert.AreEqual(4, lData.Count);
      Assert.AreEqual(60, lData["Code1"].TotalMinutes);
      Assert.AreEqual(60, lData["Code3"].TotalMinutes);
      Assert.AreEqual(30, lData["Code4"].TotalMinutes);
      Assert.AreEqual(30, lData["Code5"].TotalMinutes);



    }

    [Test]
    public void TestPathologicalDivideAmongChildren()
    {
      Task lTop = new TaskList(new Config.Config(), null);
      var lParent = lTop.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN);

      var lData = tsaConnector.GetTimeData(lTop, DateTime.Today);
      Assert.AreEqual(0, lData.Count);
        
      lParent.AddTime(0, 0, 90);
      lData = tsaConnector.GetTimeData(lTop, DateTime.Today);
      Assert.AreEqual(1, lData.Count);
      Assert.IsTrue(lData.First().Key.Contains(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN));

      lParent.AddChild("Code1");
      lParent.AddChild("Code2");

      lData = tsaConnector.GetTimeData(lTop, DateTime.Today);
      Assert.AreEqual(1, lData.Count);
      Assert.IsTrue(lData.First().Key.Contains(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN));
    }

    [Test]
    public void TestDivideAmongAll()
    {
      TaskList lParent = new TaskList(new Config.Config(), null);
      lParent.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_ALL).AddTime(0, 0, 30);
      lParent.AddChild("P1").AddTime(0, 0, 10);
      lParent.AddChild("P2").AddTime(0, 0, 20);
      var lChild = lParent.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_CHILDREN);

      lChild.AddChild("C1").AddTime(0, 0, 10);
      lChild.AddChild(TimeSheetAppConnector.DIVIDE_AMONG_ALL).AddTime(0, 0, 10);

      var lData = tsaConnector.GetTimeData(lParent, DateTime.Today);
      Console.Out.WriteLine(string.Join(" ", lData.Keys.ToArray()));
      Assert.AreEqual(3, lData.Count);
      Assert.AreEqual(20, lData["P1"].TotalMinutes);
      Assert.AreEqual(40, lData["P2"].TotalMinutes);
      Assert.AreEqual(20, lData["C1"].TotalMinutes);

    }
  }
}
