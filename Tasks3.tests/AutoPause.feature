﻿Feature: AutoPause
	In order to avoid tracking time when I'm not at my computer
	I want the timer to stop after a period of inactivity

@mytag
Scenario: AutoPause
	Given I have a pause timer with a 60 min timeout
	When I do nothing 
	Then the the autopause event should fire after 60 minutes
