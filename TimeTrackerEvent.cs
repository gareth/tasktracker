﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3
{
  public enum eEventType
  {
    StartOfDay,
    EndOfDay
  }

  public class TimeTrackerEvent
  {
    public eEventType Type;
    public DateTime Time;
  }
}
