﻿using System;
namespace Tasks3
{
  public interface ITimer
  {
    Task ActiveTask { get; }
    event EventHandler ActiveTaskOrStatusChange;
    TimeSpan GetElapsedTime(DateTime xiNow);
    TimePeriod GetPreviousPeriod(DateTime xiNow);
    event EventHandler NagDue;
    void OnKeyPress(DateTime xiNow);
    void Pause(DateTime xiNow);
    bool Paused { get; }
    void PauseResume(DateTime xiNow);
    void ReassignRecent(TimeSpan xiDuration, DateTime xiNow);
    void ResetLastNag(DateTime xiNow);
    void SetActiveTask(Task xiTask, DateTime xiNow);
    event Timer.ShowMessageHandler ShowMessage;
    DateTime? StartOfDay { get; }
    void Tick(DateTime xiNow);
  }

  public static class TimerHelper
  {
    public static void ResetLastNag(this ITimer timer) { timer.ResetLastNag(DateTime.Now); }
    public static void PauseResume(this ITimer timer) { timer.PauseResume(DateTime.Now); }
    public static void OnKeyPress(this ITimer timer) { timer.OnKeyPress(DateTime.Now); }
    public static void SetActiveTask(this ITimer timer, Task task) { timer.SetActiveTask(task, DateTime.Now); }
    public static TimePeriod GetPreviousPeriod(this ITimer timer) { return timer.GetPreviousPeriod(DateTime.Now); }
    public static void Tick(this ITimer timer) { timer.Tick(DateTime.Now); }
    public static TimeSpan GetElapsedTime(this ITimer timer) { return timer.GetElapsedTime(DateTime.Now); }
    public static void Pause(this ITimer timer) { timer.Pause(DateTime.Now); }
    public static void ReassignRecent(this ITimer timer, TimeSpan duration) { timer.ReassignRecent(duration, DateTime.Now); }
  }
}
