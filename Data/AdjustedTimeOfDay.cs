﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Data
{
  public class AdjustedTimeOfDay : IComparable<AdjustedTimeOfDay>
  {
    TimeSpan ts;

    public AdjustedTimeOfDay(TimeSpan ts)
    {
      this.ts = ts;
    }

    public TimeSpan Value { get { return ts; } }

    #region IComparable<AdjustedTimeOfDay> Members

    public int CompareTo(AdjustedTimeOfDay other)
    {
      if (this.ts.Hours < TimeLine.CUTOFF_HOUR && other.ts.Hours >= TimeLine.CUTOFF_HOUR)
      {
        return 1;
      }
      else if (this.ts.Hours >= TimeLine.CUTOFF_HOUR && other.ts.Hours < TimeLine.CUTOFF_HOUR)
      {
        return -1;
      }
      else
      {
        return this.ts.CompareTo(other.ts);
      }
    }

    #endregion
  }
}
