﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


namespace Tasks3.Data
{
  class TimeLine
  {
    public const int CUTOFF_HOUR = 4;
    private TaskList taskList;
    private DateTime date;
    private TimeSpan minCountablePeriod;



    public TimeLine(TaskList tasks, DateTime date, TimeSpan minCountablePeriod)
    {
      taskList = tasks;
      this.date = date.EffectiveDate();
      this.minCountablePeriod = minCountablePeriod;
    }

    public DateTime Start { get { return date.Date.AddHours(CUTOFF_HOUR); } }
    public DateTime End { get { return date.Date.AddDays(1).AddHours(CUTOFF_HOUR); } }

    public AdjustedTimeOfDay EarliestTrackedTime
    {
      get
      {
        if (TimePeriodsIncludingGaps.Any())
        {
          var first = TimePeriodsIncludingGaps.First();
          if (first.Start.EffectiveDate() == this.date)
          {
            return first.Start.AdjustedTimeOfDay();
          }
          else
          {
            return Start.AdjustedTimeOfDay();
          }
        }

        return null;

      }
    }

    public AdjustedTimeOfDay LatestTrackedTime
    {
      get
      {
        if (TimePeriodsIncludingGaps.Any())
        {
          var last = TimePeriodsIncludingGaps.Last();
          if (last.End.EffectiveDate() == this.date)
          {
            return last.End.AdjustedTimeOfDay();
          }
          else
          {
            return End.AdjustedTimeOfDay();
          }
        }

        return null;

      }
    }

    public IEnumerable<TimePeriod> TimePeriodsIncludingGaps
    {
      get
      {
        TimePeriod prev = null;

        foreach (TimePeriod period in taskList.GetTimePeriods(Start, End).OrderBy(T => T.Start))
        {
          if (prev == null)
          {
            TimeTrackerEvent start = taskList.GetOrCreateDailyData(period.Start.Date).GetEvent(eEventType.StartOfDay);

            if (start != null && start.Time < period.Start && start.Time > Start)
            {
              yield return new TimePeriod(EmptyTask, start.Time, period.Start);
            }
          }

          if (prev != null && prev.End + minCountablePeriod < period.Start)
          {
            yield return new TimePeriod(EmptyTask, prev.End, period.Start);
          }

          prev = period;
          yield return period;
        }

        if (prev != null)
        {
          TimeTrackerEvent end = taskList.GetOrCreateDailyData(prev.Start.Date).GetEvent(eEventType.EndOfDay);
          if (end != null && end.Time > prev.End)
          {
            yield return new TimePeriod(EmptyTask, prev.End, end.Time);
          }
        }
      }
    }

    public IEnumerable<TimePeriod> TimePeriods
    {
      get { return TimePeriodsIncludingGaps.Where(tp => tp.Task != EmptyTask); }
    }

    private static Task EmptyTask = new Task() { Colour = Color.White, Description = "None" };
  }
}

