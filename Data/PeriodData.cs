﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Data
{
  class PeriodData
  {
    Task rootTask;

    public PeriodData(Task rootTask)
    {
      this.rootTask = rootTask;
    }

    public IEnumerable<KeyValuePair<TimeSpan, Task>> topTasks(bool topLevelOnly)
    {
      var list = new SortedList<TimeSpan, Task>();
      var i = 0L;

      var tasks = topLevelOnly ? rootTask.Children : rootTask.AllChildTasks;

      foreach (Task task in tasks)
      {
        var time = topLevelOnly ? task.GetTimeThisWeekWithChildren() : task.GetTimeThisWeek();
        if (time.Ticks > 0)
        {
          // ensure unique keys
          list.Add(time.Add(TimeSpan.FromTicks(i++)), task);
        }
      }

      return list.Reverse();
    }
  }
}
