﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Tasks3
{
  public class PositionPersister
  {
    private Form form;
    private bool sizeOnly;
    private TaskList taskList;
    private bool loaded;

    public PositionPersister(TaskList tasklist)
    {
      this.taskList = tasklist;
    }

    public void Persist(Form form, bool sizeOnly)
    {
      this.form = form;
      this.sizeOnly = sizeOnly;

      form.Load += new EventHandler(form_Load);
      form.LocationChanged += new EventHandler(form_Save);
      form.Resize += new EventHandler(form_Save);
    }

    void form_Save(object sender, EventArgs e)
    {
      if (loaded)
      {
        PersistedBounds = form.Bounds;
      }
    }

    void form_Load(object sender, EventArgs e)
    {
      if (HasPersistedBounds)
      {
        if (sizeOnly || BoundsAreOffScreen)
        {
          form.Size = PersistedBounds.Size;
        }
        else
        {
          form.Bounds = PersistedBounds;
        }
      }
      loaded = true;
    }

    private bool HasPersistedBounds
    {
      get { return taskList.LocationData.ContainsKey(form.GetType().ToString()); }
    }

    private Rectangle PersistedBounds
    {
      get
      {
        return taskList.LocationData [form.GetType().ToString()];
      }
      set
      {
        taskList.LocationData[form.GetType().ToString()] = value;
      }
    }

    private bool BoundsAreOffScreen
    {
      get
      {
        return !Screen.AllScreens.Where(s => s.Bounds.Contains(PersistedBounds)).Any();
      }
    }
  }
}
