﻿namespace Tasks3
{
  partial class Nag
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nag));
      this.label1 = new System.Windows.Forms.Label();
      this.ContinueButton = new System.Windows.Forms.Button();
      this.ChangeButton = new System.Windows.Forms.Button();
      this.UnpauseButton = new System.Windows.Forms.Button();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(219, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(178, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Are you tracking your time correctly?";
      // 
      // ContinueButton
      // 
      this.ContinueButton.Location = new System.Drawing.Point(154, 35);
      this.ContinueButton.Name = "ContinueButton";
      this.ContinueButton.Size = new System.Drawing.Size(325, 23);
      this.ContinueButton.TabIndex = 1;
      this.ContinueButton.UseVisualStyleBackColor = true;
      this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
      this.ContinueButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ContinueButton_KeyPress);
      // 
      // ChangeButton
      // 
      this.ChangeButton.Location = new System.Drawing.Point(154, 64);
      this.ChangeButton.Name = "ChangeButton";
      this.ChangeButton.Size = new System.Drawing.Size(325, 25);
      this.ChangeButton.TabIndex = 2;
      this.ChangeButton.Text = "Select a new current task...";
      this.ChangeButton.UseVisualStyleBackColor = true;
      this.ChangeButton.Click += new System.EventHandler(this.ChangeButton_Click);
      this.ChangeButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangeButton_KeyDown);
      this.ChangeButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ContinueButton_KeyPress);
      // 
      // UnpauseButton
      // 
      this.UnpauseButton.Location = new System.Drawing.Point(154, 95);
      this.UnpauseButton.Name = "UnpauseButton";
      this.UnpauseButton.Size = new System.Drawing.Size(325, 25);
      this.UnpauseButton.TabIndex = 3;
      this.UnpauseButton.Text = "Un-pause my current task";
      this.UnpauseButton.UseVisualStyleBackColor = true;
      this.UnpauseButton.Click += new System.EventHandler(this.UnpauseButton_Click);
      this.UnpauseButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ContinueButton_KeyPress);
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(12, 12);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(132, 131);
      this.pictureBox1.TabIndex = 4;
      this.pictureBox1.TabStop = false;
      // 
      // Nag
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.ClientSize = new System.Drawing.Size(491, 151);
      this.ControlBox = false;
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.UnpauseButton);
      this.Controls.Add(this.ChangeButton);
      this.Controls.Add(this.ContinueButton);
      this.Controls.Add(this.label1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "Nag";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Just checking...";
      this.TopMost = true;
      this.Shown += new System.EventHandler(this.Nag_Shown);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ContinueButton_KeyPress);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button ContinueButton;
    private System.Windows.Forms.Button ChangeButton;
    private System.Windows.Forms.Button UnpauseButton;
    private System.Windows.Forms.PictureBox pictureBox1;
  }
}