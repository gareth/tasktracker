﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace Tasks3
{
  using System.Runtime.InteropServices;

  //ToDO: break out into interface

  public class TimeSheetAppConnector : ITaskProvider
  {
    public TimeSheetAppConnector(Tasks3.Config.Config config)
    {
      this.config = config;
    }

   
    public enum eIntegrationMode
    {
      None,
      ExportOnly,
      ImportTasks,
      AllowOnlyTSATasks
    }

    private List<string> mTaskCodes;

    public List<string> AllTasks
    {
      get
      {
          if (mTaskCodes == null || Expired)
          {
              Refresh();
          }

          return mTaskCodes ?? mBuiltInTaskCodes;
      }      
    }

    private DateTime lastUpdate = DateTime.MinValue;
    private object _lock = new object();

    private bool Expired
    {
      get 
      {
        lock (_lock)
        {
          return lastUpdate.AddDays(1) < DateTime.Now;
        }
      }
    }

/*    public IEnumerable<IGrouping<string,string>> TaskTree
    {
      get
      {
        return AllTasks.GroupBy(t => t.Substring(0, t.IndexOf(':')), t => t.Substring(t.IndexOf(':') + 1)); 
      }
    }
 */

    public void SubmitTaskData(DateTime xiStartDate)
    {
      // TODO = try to match unassigned tasks to newly-created TSA tasks

        try
        {

            StringBuilder lBuilder = new StringBuilder();
            var lTimes = GetTimeData(taskList, xiStartDate);
            foreach (var lPair in lTimes)
            {
              lBuilder.AppendFormat("{0},{1:0.00}\n", lPair.Key, lPair.Value.TotalHours);
            }                

            string lTimeData = lBuilder.ToString();

            lTimeData = System.Web.HttpUtility.UrlEncode(lTimeData);

            HttpWebRequest lRequest = (HttpWebRequest)WebRequest.Create(config.TimesheetAppUrl + "mvc/actions/timesheetimport/import");
            lRequest.Method = "POST";
            lRequest.ContentType = "text/xml";

            string lData = string.Format(@"
<TimesheetData xmlns=""http://schemas.datacontract.org/2004/07/Website"">
<Data>{1}${2}${0}</Data>
</TimesheetData>
",
    lTimeData,
    System.Environment.UserName,
    xiStartDate.ToString("yyyy-MM-dd"));
            byte[] lEncodedData = System.Text.Encoding.UTF8.GetBytes(lData);

            lRequest.ContentLength = lEncodedData.Length;

            using (Stream lStream = lRequest.GetRequestStream())
            {
                lStream.Write(lEncodedData, 0, lEncodedData.Length);
            }

            Process.Start(config.TimesheetAppUrl + "mvc/actions/timesheets/createfromimporteddata?date=" + xiStartDate.ToString("yyyy-MM-dd"));
        }
        catch (Exception e)
        {
            MessageBox.Show("An error occurred submitting data to the timesheet app\n" + e.ToString());
        }
    }

    public Dictionary<string, TimeSpan> GetTimeData(Task xiParentTask, DateTime xiFrom)
    {
      DateTime lFrom = xiFrom;
      DateTime lTo = xiFrom.AddDays(7);

      TimeSpan lTopLevelTime = 
        xiParentTask.AllChildTasks.
        Where(T => T.TimesheetCode == DIVIDE_AMONG_ALL).
        Sum(T => T.GetTime(lFrom, lTo));

      TimeSpan lRemainingTime =
        xiParentTask.AllChildTasks.
        Where(T => T.TimesheetCode != DIVIDE_AMONG_ALL).
        Sum(T => T.GetTime(lFrom, lTo));

      var globalMultiplier = 1.0d;

      if (lRemainingTime.TotalSeconds > 0)
      {
        globalMultiplier += lTopLevelTime.TotalSeconds / lRemainingTime.TotalSeconds;
      }


      Dictionary<string, TimeSpan> lTimes = new Dictionary<string,TimeSpan>();
      GetTimeDataRecurse(xiParentTask, xiFrom, lTimes, globalMultiplier);

      // This isn't valid since a) The times above don't exclude times under 1 min, and b) they don't account for the rare case of tasks with multiple codes
      //Debug.Assert(Math.Abs(lTimes.Sum(KV => KV.Value.TotalSeconds) - (lTopLevelTime + lRemainingTime).TotalSeconds) < 60);      

      return lTimes;
    }

    private void GetTimeDataRecurse(Task xiParentTask, DateTime xiFrom, Dictionary<string, TimeSpan> xbTimes, double multiplier)
    {
        DateTime lFrom = xiFrom;
        DateTime lTo = xiFrom.AddDays(7);
        
/*        if (xiInherited > TimeSpan.Zero && (xiParentTask.Children.Count == 0 || lChildrenTotal.TotalSeconds == 0))
        {
          xbTimes.Add(xiParentTask.Description + "(no children to reassign to)", xiInherited);
        }*/

        foreach (Task lTask in xiParentTask.Children.OrderBy(Task => Task.Description))        
        {
          var childMultiplier = multiplier;
            TimeSpan lThisTaskTime = TimeSpan.FromSeconds( lTask.GetTime(lFrom, lTo).TotalSeconds * multiplier);

            var lCodes = (lTask.TimesheetCode ?? "").Split(new[] { ';' }).Select(c => CleanCode(c, lTask));

            if (lCodes.Contains(DIVIDE_AMONG_ALL))
            {
              continue;
            }
            else if (lCodes.Contains(DIVIDE_AMONG_CHILDREN))
            {
              var childTime =
                lTask.AllChildTasks.
                Where(T => T.TimesheetCode != DIVIDE_AMONG_ALL).
                Sum(T => T.GetTime(lFrom, lTo));

              if (childTime > TimeSpan.Zero)
              {
                childMultiplier += lThisTaskTime.TotalSeconds / childTime.TotalSeconds;
              }
              else if (lThisTaskTime > TimeSpan.Zero)
              {
                xbTimes.Add(lTask.Description + " (no children to reassign to)", lThisTaskTime);
              }

              //lInherited = lThisTaskTime;// +TimeSpan.FromSeconds(lChildTime.TotalSeconds * (lMultiplier - 1));
            }
            else
            {
              foreach (string lCode in lCodes)
              {

                if (lThisTaskTime > TimeSpan.FromMinutes(1))
                {
                  if (xbTimes.ContainsKey(lCode))
                  {
                    xbTimes[lCode] += TimeSpan.FromSeconds(lThisTaskTime.TotalSeconds / lCodes.Count());
                  }
                  else
                  {
                    xbTimes[lCode] = TimeSpan.FromSeconds(lThisTaskTime.TotalSeconds / lCodes.Count());
                  }
                }
              }
            }

            GetTimeDataRecurse(lTask, xiFrom, xbTimes, childMultiplier);
        }
    }

    private string CleanCode(string xiCode, Task xiTask)
    {
      var lCleanedCode = xiCode;

      if (xiCode == null || xiCode == "")
      {
        lCleanedCode = "NONE (" + xiTask.QualifiedName + ")";
      }

      lCleanedCode = lCleanedCode.Replace("\r", "").Replace("\n", "");

      return lCleanedCode;
    }

    public volatile bool inProgress;

    public void Init(Task task, IErrorLogger errorLogger)
    {
      taskList = task;
      this.errorLogger = errorLogger;
      RefreshAsync();
    }

    private Task taskList;
    private IErrorLogger errorLogger;

    private void RefreshAsync()
    {

      if (inProgress)
      {
        return;
      }

      inProgress = true;

      ThreadPool.QueueUserWorkItem(o =>
      {
        Thread.Sleep(TimeSpan.FromSeconds(5));
        Refresh();
      });
    }

    public void Refresh()
    {
      try
      {
        if (inProgress)
        {
          return;
        }

        inProgress = true;

        WebRequest lRequest = WebRequest.Create(config.TimesheetAppUrl + "mvc/actions/tasks/export");
        lRequest.Credentials = config.TsaCredentials;

        using (WebResponse lResponse = lRequest.GetResponse())
        {
          if (lResponse.ContentLength == 0 || ((HttpWebResponse)lResponse).StatusCode != HttpStatusCode.OK)
          {
            return;
          }

          XmlTextReader lReader = new XmlTextReader(lResponse.GetResponseStream());
          XmlDocument lDom = new XmlDocument();
          lDom.Load(lReader);

          List<string> lRet = new List<string>();

          lRet.Add(DIVIDE_AMONG_CHILDREN);
          lRet.Add(DIVIDE_AMONG_ALL);

          foreach (XmlNode lNode in lDom.SelectNodes(@"//Code"))
          {
            if (!lNode.InnerText.StartsWith("BUG:")
              && !lNode.InnerText.StartsWith("INC:")
              && !lNode.InnerText.StartsWith("JIRA:"))
            {
              lRet.Add(lNode.InnerText);
            }
          }

          lRet.Sort();

          mTaskCodes = lRet;

          return;
        }
      }
      catch (Exception e)
      {
        errorLogger.NotifyUserError("TSA task import failed. Check the TSA settings on the options screen.\r\n", e);
        mTaskCodes = mBuiltInTaskCodes;

        var webexception = e as WebException;
        if (webexception != null && webexception.Response != null)
        {
          var stream = webexception.Response.GetResponseStream();
          if (stream != null)
          {
            Debug.Write(new StreamReader(stream).ReadToEnd());
          }
        }        
      }
      finally
      {
        lock (_lock)
        {
          inProgress = false;
          lastUpdate = DateTime.Now;
          MergeTaskList();
        }
      }
    }

    private void MergeTaskList()
    {
      foreach (var task in AllTasks.Where(t => t.Contains(":")).GroupBy(t => t.Substring(0, t.IndexOf(':')), t => t.Substring(t.IndexOf(':') + 1)))
      {
        var parent = CreateIfMissing(taskList, task.Key);

        foreach (var child in task)
        {
          CreateIfMissing(parent, child);
        }
      }

      var toRemove = new List<Task>();

      foreach (var task in taskList.AllChildTasks)
      {
        // clean up bad data
        if (task.IsTSA && string.IsNullOrEmpty(task.TimesheetCode))
        {
          task.IsTSA = false;
        }

        if (task.IsTSA && !AllTasks.Contains(task.TimesheetCode) && (task.TimesheetCode.Contains(":") || !AllTasks.Where(t => t.StartsWith(task.TimesheetCode + ":")).Any()))
        {
          System.Diagnostics.Debug.WriteLine(task.TimesheetCode);

          if (!task.TimePeriods.Any() && !task.AllChildTasks.Select(t => t.TimePeriods).Any())
          {
            toRemove.Add(task);
          }
          else
          {
            task.Complete = true;
          }
        }
      }

      foreach (var task in toRemove)
      {
        task.Parent.Children.Remove(task);
      }
    }

    private Task CreateIfMissing(Task parent, string newTaskName)
    {
      var newCode = (parent.TimesheetCode == null ? "" : parent.TimesheetCode + ":") + newTaskName;

      var candidates = parent.Children.Where(t => t.TimesheetCode == newCode || string.Compare(t.Description, newTaskName, true) == 0);

      if (candidates.Any())
      {
        var task = candidates.First();
        task.TimesheetCode = newCode;
        return task;
      }
      else
      {
        var newTask = new Task();
        newTask.Description = newTaskName;
        newTask.TimesheetCode = newCode;
        newTask.IsTSA = true;
        parent.AddChild(newTask);
        return newTask;
      }
    }

    public const string DIVIDE_AMONG_CHILDREN = "DivideAmongChildren";
    public const string DIVIDE_AMONG_ALL = "DivideAmongAllTasks";

    private List<String> mBuiltInTaskCodes = new List<string>() { DIVIDE_AMONG_ALL, DIVIDE_AMONG_CHILDREN };
    private Tasks3.Config.Config config;

    public virtual string GetBestMatch(string xiPrefix)
    {
      if ((xiPrefix ?? "") != "")
      {

        foreach (string lCode in AllTasks)
        {
          if (lCode.ToLower().StartsWith(xiPrefix.ToLower()))
          {
            if (xiPrefix.Contains(":"))
            {
              return (lCode);
            }
            else if (lCode.Contains(":"))
            {
              return lCode.Substring(0, lCode.IndexOf(":") + 1);
            }
            else
            {
              return lCode;
            }
          }
        }
      }

      return null;
      //return xiRequireMatch ? null : xiPrefix;
    }
  }
}
