﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3
{
  public class DailyData
  {
    public DateTime Date;

    public List<TimeTrackerEvent> Events
    {
      get { return mEvents; }
      set { mEvents = value; }
    }

    private List<TimeTrackerEvent> mEvents = new List<TimeTrackerEvent>();

    public TimeTrackerEvent GetEvent(eEventType xiType)
    {
      return Events.Where(e => e.Type == xiType).FirstOrDefault();
    }

    public TimeTrackerEvent GetOrCreateEvent(eEventType xiType)
    {
      var lRet = GetEvent(xiType);

      if (lRet == null)
      {
        lRet = new TimeTrackerEvent() { Type = xiType };
        Events.Add(lRet);
      }

      return lRet;
    }
  }
}
