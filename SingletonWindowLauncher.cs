﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tasks3
{
  public class SingletonWindowLauncher
  {
    private readonly Func<Form> constructor;
    private volatile bool active;
    private readonly object lockable = new object();

    public SingletonWindowLauncher(Func<Form> constructor)
    {
      this.constructor = constructor;
    }

    public void ShowDialog()
    {
      lock (lockable)
      {
        if (active)
        {
          return;
        }
        else
        {
          active = true;
        }
      }

      var form = constructor();
      form.FormClosed += new FormClosedEventHandler(form_FormClosed);
      form.ShowDialog();
    }

    void form_FormClosed(object sender, FormClosedEventArgs e)
    {
      lock (lockable)
      {
        active = false;
      }      
    }
  }
}
