using System;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Tasks3
{
  // No longer used - preserved only to enable upgrades.
  public class Category : Task
  {
    public string Code
    {
      get;
      set;
    }
  }
}
