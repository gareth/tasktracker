﻿namespace Tasks3
{
  partial class SummaryData
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
      System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
      this.SummaryChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
      this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
      this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
      this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
      ((System.ComponentModel.ISupportInitialize)(this.SummaryChart)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
      this.SuspendLayout();
      // 
      // SummaryChart
      // 
      this.SummaryChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      chartArea1.Area3DStyle.Enable3D = true;
      chartArea1.Area3DStyle.Inclination = 15;
      chartArea1.Area3DStyle.IsClustered = true;
      chartArea1.Area3DStyle.PointDepth = 1;
      chartArea1.Area3DStyle.PointGapDepth = 1;
      chartArea1.Area3DStyle.Rotation = 5;
      chartArea1.AxisX.IsStartedFromZero = false;
      chartArea1.AxisY.IsStartedFromZero = false;
      chartArea1.BackColor = System.Drawing.Color.White;
      chartArea1.Name = "Default";
      chartArea1.ShadowColor = System.Drawing.Color.White;
      this.SummaryChart.ChartAreas.Add(chartArea1);
      this.SummaryChart.Location = new System.Drawing.Point(12, 30);
      this.SummaryChart.Name = "SummaryChart";
      this.SummaryChart.Size = new System.Drawing.Size(435, 305);
      this.SummaryChart.TabIndex = 0;
      this.SummaryChart.Text = "chart1";
      // 
      // dateTimePicker1
      // 
      this.dateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.dateTimePicker1.CustomFormat = "dddd dd MMMM yyyy";
      this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dateTimePicker1.Location = new System.Drawing.Point(338, 4);
      this.dateTimePicker1.Name = "dateTimePicker1";
      this.dateTimePicker1.Size = new System.Drawing.Size(221, 20);
      this.dateTimePicker1.TabIndex = 2;
      this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
      // 
      // chart1
      // 
      this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      chartArea2.Area3DStyle.Enable3D = true;
      chartArea2.Area3DStyle.Inclination = 15;
      chartArea2.Area3DStyle.IsClustered = true;
      chartArea2.Area3DStyle.PointDepth = 1;
      chartArea2.Area3DStyle.PointGapDepth = 1;
      chartArea2.Area3DStyle.Rotation = 5;
      chartArea2.AxisX.IsStartedFromZero = false;
      chartArea2.AxisY.IsStartedFromZero = false;
      chartArea2.BackColor = System.Drawing.Color.White;
      chartArea2.Name = "Default";
      chartArea2.ShadowColor = System.Drawing.Color.White;
      this.chart1.ChartAreas.Add(chartArea2);
      this.chart1.Location = new System.Drawing.Point(453, 30);
      this.chart1.Name = "chart1";
      this.chart1.Size = new System.Drawing.Size(435, 305);
      this.chart1.TabIndex = 3;
      this.chart1.Text = "chart1";
      // 
      // SummaryData
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(898, 658);
      this.Controls.Add(this.chart1);
      this.Controls.Add(this.dateTimePicker1);
      this.Controls.Add(this.SummaryChart);
      this.Name = "SummaryData";
      this.Text = "SummaryData";
      this.Load += new System.EventHandler(this.SummaryData_Load);
      ((System.ComponentModel.ISupportInitialize)(this.SummaryChart)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataVisualization.Charting.Chart SummaryChart;
    private System.Windows.Forms.DateTimePicker dateTimePicker1;
    private System.Windows.Forms.ToolTip toolTip1;
    private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
  }
}