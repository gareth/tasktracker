using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Linq;

namespace Tasks3
{
  public class TimePeriod : IComparable<TimePeriod>
  {
    public TimePeriod() {}

    public TimePeriod(Task xiTask, DateTime xiStart, DateTime xiEnd)
    {
      mTask = xiTask;
      Start = xiStart;
      End = xiEnd;
    }

    private DateTime start = DateTime.MinValue;
    private DateTime end   = DateTime.MinValue;
    private Task mTask;

    public DateTime Start
    {
      get { return start; }
      set
      {
        start = value;
        if (Task != null)
        {
          Task.OnChange();
        }
      }
    }

    public DateTime End
    {
      get { return end; }
      set
      {
        end = value;
        if (Task != null)
        {
          Task.OnChange();
        }
      }
    }
    

    public int CompareTo(TimePeriod xiValue)
    {
      return Start.CompareTo(xiValue.Start);
    }

    [XmlIgnore]
    public TimeSpan Duration
    {
      get
      {
        return End - Start;
      }
    }

    [XmlIgnore]
    public Task Task
    {
      get { return mTask; }
      set 
      {
        var lOld = mTask;

        mTask = value;

        if (lOld != mTask && lOld != null)
        {
          lOld.DeleteTimePeriod(this);
        }

        if (mTask != null && !mTask.TimePeriods.Contains(this))
        {
          mTask.AddTimePeriod(this.start, this.end);
        }
      }
    }

    public static TimeSpan GetTimeSince(IList<TimePeriod> xiTimePeriods, DateTime xiTimeSince, DateTime xiTimeUntil)
    {
      return TimeSpan.FromSeconds(xiTimePeriods.Sum(TimePeriod => TimePeriod.GetDuration(xiTimeSince, xiTimeUntil).TotalSeconds));
    }

    public TimeSpan GetDuration(DateTime xiStart, DateTime xiEnd)
    {
      TimeSpan lSpan = (MinDate(End, xiEnd) - MaxDate(Start, xiStart));
      return lSpan.TotalSeconds > 0 ? lSpan : TimeSpan.Zero;
    }

    public static DateTime MinDate(DateTime xiDate1, DateTime xiDate2)
    {
      return xiDate1 < xiDate2 ? xiDate1 : xiDate2;
    }

    public static DateTime MaxDate(DateTime xiDate1, DateTime xiDate2)
    {
      return xiDate1 < xiDate2 ? xiDate2 : xiDate1;
    }
  }
}
