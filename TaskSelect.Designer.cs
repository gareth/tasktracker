﻿namespace Tasks3
{
    partial class TaskSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskSelect));
          this.imageList1 = new System.Windows.Forms.ImageList(this.components);
          this.ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
          this.SetActiveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.DetailsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.SetCompleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.DeleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.sendToTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.sendToBottomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.Cancel = new System.Windows.Forms.Button();
          this.BestMatchLabel = new System.Windows.Forms.Label();
          this.taskNameBox = new Tasks3.AutoCompleteTextBox();
          this.ArchivedCheckBox = new System.Windows.Forms.CheckBox();
          this.label1 = new System.Windows.Forms.Label();
          this.SortCombo = new System.Windows.Forms.ComboBox();
          this.NewTaskButton = new System.Windows.Forms.Button();
          this.ShowDeletedCheckBox = new System.Windows.Forms.CheckBox();
          this.ShowCompleteCheckbox = new System.Windows.Forms.CheckBox();
          this.TasksTree = new Tasks3.TaskSelect.CustomTreeView();
          this.ContextMenu.SuspendLayout();
          this.SuspendLayout();
          // 
          // imageList1
          // 
          this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
          this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
          this.imageList1.Images.SetKeyName(0, "NoTask.png");
          // 
          // ContextMenu
          // 
          this.ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SetActiveMenuItem,
            this.DetailsMenuItem,
            this.SetCompleteMenuItem,
            this.DeleteMenuItem,
            this.sendToTopToolStripMenuItem,
            this.sendToBottomToolStripMenuItem});
          this.ContextMenu.Name = "contextMenuStrip1";
          this.ContextMenu.ShowImageMargin = false;
          this.ContextMenu.Size = new System.Drawing.Size(146, 136);
          // 
          // SetActiveMenuItem
          // 
          this.SetActiveMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.SetActiveMenuItem.Name = "SetActiveMenuItem";
          this.SetActiveMenuItem.Size = new System.Drawing.Size(145, 22);
          this.SetActiveMenuItem.Text = "Set as active task";
          this.SetActiveMenuItem.Click += new System.EventHandler(this.SetActiveMenuItem_Click);
          // 
          // DetailsMenuItem
          // 
          this.DetailsMenuItem.Name = "DetailsMenuItem";
          this.DetailsMenuItem.Size = new System.Drawing.Size(145, 22);
          this.DetailsMenuItem.Text = "Details...";
          this.DetailsMenuItem.Click += new System.EventHandler(this.DetailsMenuItem_Click);
          // 
          // SetCompleteMenuItem
          // 
          this.SetCompleteMenuItem.Name = "SetCompleteMenuItem";
          this.SetCompleteMenuItem.Size = new System.Drawing.Size(145, 22);
          this.SetCompleteMenuItem.Text = "Toggle complete";
          this.SetCompleteMenuItem.Click += new System.EventHandler(this.SetCompleteMenuItem_Click);
          // 
          // DeleteMenuItem
          // 
          this.DeleteMenuItem.Name = "DeleteMenuItem";
          this.DeleteMenuItem.Size = new System.Drawing.Size(145, 22);
          this.DeleteMenuItem.Text = "Toggle deleted";
          this.DeleteMenuItem.Click += new System.EventHandler(this.DeleteMenuItem_Click);
          // 
          // sendToTopToolStripMenuItem
          // 
          this.sendToTopToolStripMenuItem.Name = "sendToTopToolStripMenuItem";
          this.sendToTopToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
          this.sendToTopToolStripMenuItem.Text = "Send to Top";
          this.sendToTopToolStripMenuItem.Click += new System.EventHandler(this.sendToTopToolStripMenuItem_Click);
          // 
          // sendToBottomToolStripMenuItem
          // 
          this.sendToBottomToolStripMenuItem.Name = "sendToBottomToolStripMenuItem";
          this.sendToBottomToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
          this.sendToBottomToolStripMenuItem.Text = "Send to Bottom";
          this.sendToBottomToolStripMenuItem.Click += new System.EventHandler(this.sendToBottomToolStripMenuItem_Click);
          // 
          // Cancel
          // 
          this.Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.Cancel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.Cancel.Location = new System.Drawing.Point(335, 462);
          this.Cancel.Name = "Cancel";
          this.Cancel.Size = new System.Drawing.Size(75, 23);
          this.Cancel.TabIndex = 6;
          this.Cancel.Text = "Cancel";
          this.Cancel.UseVisualStyleBackColor = true;
          this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
          // 
          // BestMatchLabel
          // 
          this.BestMatchLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.BestMatchLabel.Location = new System.Drawing.Point(8, 27);
          this.BestMatchLabel.Name = "BestMatchLabel";
          this.BestMatchLabel.Size = new System.Drawing.Size(410, 18);
          this.BestMatchLabel.TabIndex = 9;
          this.BestMatchLabel.Click += new System.EventHandler(this.BestMatchLabel_Click);
          // 
          // taskNameBox
          // 
          this.taskNameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.taskNameBox.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.taskNameBox.ForeColor = System.Drawing.SystemColors.GrayText;
          this.taskNameBox.Location = new System.Drawing.Point(3, 3);
          this.taskNameBox.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
          this.taskNameBox.Name = "taskNameBox";
          this.taskNameBox.Size = new System.Drawing.Size(415, 22);
          this.taskNameBox.TabIndex = 8;
          this.taskNameBox.Text = "Start typing the name of a new or existing task...";
          this.taskNameBox.TopForm = null;
          this.taskNameBox.ActionCompleted += new System.Action<bool>(this.taskNameBox_ActionCompleted);
          this.taskNameBox.BestMatchChanged += new System.Action<string>(this.taskNameBox_BestMatchChanged);
          this.taskNameBox.TextChanged += new System.EventHandler(this.taskNameBox_TextChanged);
          this.taskNameBox.Enter += new System.EventHandler(this.taskNameBox_Enter);
          // 
          // ArchivedCheckBox
          // 
          this.ArchivedCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.ArchivedCheckBox.AutoSize = true;
          this.ArchivedCheckBox.Location = new System.Drawing.Point(12, 474);
          this.ArchivedCheckBox.Name = "ArchivedCheckBox";
          this.ArchivedCheckBox.Size = new System.Drawing.Size(98, 17);
          this.ArchivedCheckBox.TabIndex = 7;
          this.ArchivedCheckBox.Text = "Show old tasks";
          this.ArchivedCheckBox.UseVisualStyleBackColor = true;
          this.ArchivedCheckBox.CheckedChanged += new System.EventHandler(this.UpdateView);
          // 
          // label1
          // 
          this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(162, 443);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(71, 13);
          this.label1.TabIndex = 5;
          this.label1.Text = "Sort tasks by:";
          // 
          // SortCombo
          // 
          this.SortCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.SortCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.SortCombo.FormattingEnabled = true;
          this.SortCombo.Items.AddRange(new object[] {
            "Description",
            "Date Added",
            "Priority"});
          this.SortCombo.Location = new System.Drawing.Point(165, 462);
          this.SortCombo.Name = "SortCombo";
          this.SortCombo.Size = new System.Drawing.Size(121, 21);
          this.SortCombo.TabIndex = 4;
          this.SortCombo.SelectedIndexChanged += new System.EventHandler(this.UpdateView);
          // 
          // NewTaskButton
          // 
          this.NewTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.NewTaskButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.NewTaskButton.Location = new System.Drawing.Point(335, 433);
          this.NewTaskButton.Name = "NewTaskButton";
          this.NewTaskButton.Size = new System.Drawing.Size(75, 23);
          this.NewTaskButton.TabIndex = 1;
          this.NewTaskButton.Text = "New Task";
          this.NewTaskButton.UseVisualStyleBackColor = true;
          this.NewTaskButton.Click += new System.EventHandler(this.NewTaskButton_Click);
          // 
          // ShowDeletedCheckBox
          // 
          this.ShowDeletedCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.ShowDeletedCheckBox.AutoSize = true;
          this.ShowDeletedCheckBox.Location = new System.Drawing.Point(12, 451);
          this.ShowDeletedCheckBox.Name = "ShowDeletedCheckBox";
          this.ShowDeletedCheckBox.Size = new System.Drawing.Size(119, 17);
          this.ShowDeletedCheckBox.TabIndex = 3;
          this.ShowDeletedCheckBox.Text = "Show deleted tasks";
          this.ShowDeletedCheckBox.UseVisualStyleBackColor = true;
          this.ShowDeletedCheckBox.CheckedChanged += new System.EventHandler(this.UpdateView);
          // 
          // ShowCompleteCheckbox
          // 
          this.ShowCompleteCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.ShowCompleteCheckbox.AutoSize = true;
          this.ShowCompleteCheckbox.Location = new System.Drawing.Point(12, 428);
          this.ShowCompleteCheckbox.Name = "ShowCompleteCheckbox";
          this.ShowCompleteCheckbox.Size = new System.Drawing.Size(133, 17);
          this.ShowCompleteCheckbox.TabIndex = 2;
          this.ShowCompleteCheckbox.Text = "Show completed tasks";
          this.ShowCompleteCheckbox.UseVisualStyleBackColor = true;
          this.ShowCompleteCheckbox.CheckedChanged += new System.EventHandler(this.UpdateView);
          // 
          // TasksTree
          // 
          this.TasksTree.AllowDrop = true;
          this.TasksTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.TasksTree.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.TasksTree.ImageIndex = 0;
          this.TasksTree.ImageList = this.imageList1;
          this.TasksTree.Location = new System.Drawing.Point(0, 47);
          this.TasksTree.Name = "TasksTree";
          this.TasksTree.SelectedImageIndex = 0;
          this.TasksTree.ShowNodeToolTips = true;
          this.TasksTree.Size = new System.Drawing.Size(422, 375);
          this.TasksTree.TabIndex = 0;
          this.TasksTree.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.TasksTree_AfterCollapse);
          this.TasksTree.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.TasksTree_AfterExpand);
          this.TasksTree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.TreeItemDrag);
          this.TasksTree.NodeMouseHover += new System.Windows.Forms.TreeNodeMouseHoverEventHandler(this.treeView1_NodeMouseHover);
          this.TasksTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
          this.TasksTree.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TasksTree_NodeMouseDoubleClick);
          this.TasksTree.DragDrop += new System.Windows.Forms.DragEventHandler(this.TreeDragDrop);
          this.TasksTree.DragEnter += new System.Windows.Forms.DragEventHandler(this.TreeDragEnter);
          this.TasksTree.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TasksTree_KeyPress);
          this.TasksTree.MouseLeave += new System.EventHandler(this.treeView1_MouseLeave);
          this.TasksTree.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TasksTree_MouseUp);
          // 
          // TaskSelect
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.CancelButton = this.Cancel;
          this.ClientSize = new System.Drawing.Size(422, 495);
          this.ControlBox = false;
          this.Controls.Add(this.BestMatchLabel);
          this.Controls.Add(this.taskNameBox);
          this.Controls.Add(this.ArchivedCheckBox);
          this.Controls.Add(this.Cancel);
          this.Controls.Add(this.label1);
          this.Controls.Add(this.SortCombo);
          this.Controls.Add(this.NewTaskButton);
          this.Controls.Add(this.ShowDeletedCheckBox);
          this.Controls.Add(this.ShowCompleteCheckbox);
          this.Controls.Add(this.TasksTree);
          this.MinimumSize = new System.Drawing.Size(400, 300);
          this.Name = "TaskSelect";
          this.ShowInTaskbar = false;
          this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
          this.TopMost = true;
          this.ContextMenu.ResumeLayout(false);
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private CustomTreeView TasksTree;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button NewTaskButton;
        private System.Windows.Forms.CheckBox ShowCompleteCheckbox;
        private System.Windows.Forms.CheckBox ShowDeletedCheckBox;
        private System.Windows.Forms.ComboBox SortCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.ContextMenuStrip ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem DetailsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SetCompleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SetActiveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendToTopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendToBottomToolStripMenuItem;
        private System.Windows.Forms.CheckBox ArchivedCheckBox;
        private AutoCompleteTextBox taskNameBox;
        private System.Windows.Forms.Label BestMatchLabel;
    }
}