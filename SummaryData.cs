﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Tasks3
{
  public partial class SummaryData : Form
  {
    public SummaryData()
    {
      InitializeComponent();

      dateTimePicker1.Value = new DateTime(2008, 03, 03);
      //SummaryChart.ChartAreas[0].AxisY.CustomLabels.Add(1, DateTimeIntervalType.Days, "dd MMM");
    }

    private Dictionary<Color, Series> mSeries = new Dictionary<Color, Series>();

    private Series GetSeries(Color xiColor)
    {
      if (mSeries.ContainsKey(xiColor))
      {
        return mSeries[xiColor];
      }
      else
      {
        Series lRet = new Series();
        lRet.YValuesPerPoint = 2;
        lRet.Color = xiColor;
        lRet["PointWidth"] = "0.2";
        lRet["DrawSideBySide"] = "false";
        lRet.ChartType = SeriesChartType.RangeBar;
        SummaryChart.Series.Add(lRet);
        mSeries.Add(xiColor, lRet);
        return lRet;
      }
    }
    

    private void SummaryData_Load(object sender, EventArgs e)
    {
      for (int i = 0; i < 5; ++i)
      {
        DateTime lDate = dateTimePicker1.Value.Date.AddDays(i);
        SummaryChart.ChartAreas[0].AxisX.CustomLabels.Add(lDate.ToOADate() - 5, lDate.ToOADate() + 5, lDate.ToShortDateString());

        IOrderedEnumerable<TimePeriod> lPeriods = TaskList.Instance.GetTimePeriods(lDate, lDate.AddDays(1)).OrderBy(T => T.Start);

        foreach (TimePeriod lPeriod in lPeriods)
        {
          DataPoint lPoint = new DataPoint();
          lPoint.SetValueXY(lPeriod.Start.Date, /* lDate.ToShortDateString(),*/ lPeriod.Start.TimeOfDay.TotalHours, lPeriod.End.TimeOfDay.TotalHours);
          lPoint.ToolTip = lPeriod.Task.Description;
          GetSeries(lPeriod.Task.Colour).Points.Add(lPoint);
        }
      }

   /*   SummaryChart.Series.Add("Tasks");
      SummaryChart.Series.Add("Progress");

      SummaryChart.Series["Tasks"].YValuesPerPoint = 2;
      SummaryChart.Series["Progress"].YValuesPerPoint = 2;

      // Populate series data
      DateTime currentData = DateTime.Now.Date;
      SummaryChart.Series["Tasks"].Points.AddXY(1, currentData, currentData.AddDays(5));
      SummaryChart.Series["Tasks"].Points.AddXY(2, currentData.AddDays(5), currentData.AddDays(7));
      SummaryChart.Series["Tasks"].Points.AddXY(3, currentData.AddDays(7), currentData.AddDays(10));
      SummaryChart.Series["Tasks"].Points.AddXY(1, currentData.AddDays(10), currentData.AddDays(15));
      SummaryChart.Series["Tasks"].Points.AddXY(4, currentData.AddDays(15), currentData.AddDays(20));
      SummaryChart.Series["Tasks"].Points.AddXY(2, currentData.AddDays(20), currentData.AddDays(27));

      SummaryChart.Series["Progress"].Points.AddXY(1, currentData, currentData.AddDays(5));
      SummaryChart.Series["Progress"].Points.AddXY(2, currentData.AddDays(5), currentData.AddDays(7));
      SummaryChart.Series["Progress"].Points.AddXY(3, currentData.AddDays(7), currentData.AddDays(10));
      SummaryChart.Series["Progress"].Points.AddXY(1, currentData.AddDays(10), currentData.AddDays(13));

      // Set axis labels
      SummaryChart.Series["Tasks"].Points[0].AxisLabel = "Task 1";
      SummaryChart.Series["Tasks"].Points[1].AxisLabel = "Task 2";
      SummaryChart.Series["Tasks"].Points[2].AxisLabel = "Task 3";
      SummaryChart.Series["Tasks"].Points[4].AxisLabel = "Task 4";

      // Set Range Bar chart type
      SummaryChart.Series["Tasks"].ChartType = SeriesChartType.RangeBar;
      SummaryChart.Series["Progress"].ChartType = SeriesChartType.RangeBar;

      // Set point width
      SummaryChart.Series["Tasks"]["PointWidth"] = "0.7";
      SummaryChart.Series["Progress"]["PointWidth"] = "0.2";

      // Draw series side-by-side
      SummaryChart.Series["Progress"]["DrawSideBySide"] = "false";

      // Set Y axis Minimum and Maximum
      SummaryChart.ChartAreas["Default"].AxisY.Minimum = currentData.AddDays(-1).ToOADate();
      SummaryChart.ChartAreas["Default"].AxisY.Maximum = currentData.AddDays(28).ToOADate();*/

      // Show as 3D
      //SummaryChart.ChartAreas["Default"].Area3DStyle.Enable3D = true;
    }

    private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
    {
      SummaryChart.Series.Clear();
      mSeries.Clear();
      SummaryData_Load(this, EventArgs.Empty);
    }
  }
}
