﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3
{
  public interface ITaskProvider
  {
    string GetBestMatch(string xiPrefix);
    void Init(Task taskList, IErrorLogger errorLogger);
    void Refresh();
    List<string> AllTasks { get; }
    void SubmitTaskData(DateTime date);
  }
}
