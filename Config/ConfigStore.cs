﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Tasks3.Config
{
  /// <summary>
  /// Responsibilities: 
  /// * Map a config identifier to a concrete instance of that config
  /// Collaborators:
  /// * ConfigItem
  /// * Base storage classes (registry accessors, type converters)
  /// </summary>
  public class ConfigStore : IConfigStore
  {
    public static Dictionary<Type, object> Accessors = new Dictionary<Type, object>();

    static ConfigStore()
    {
      RegisterAccessors(
        new TypeConvertingAccessor<SecureString, string>(new EncryptingConverter(), new RegistryStringAccessor()),
        new TypeConvertingAccessor<DateTime, string>(TypeConverters.DateTime, new RegistryStringAccessor()),
        new TypeConvertingAccessor<bool, int>(TypeConverters.Boolean, new RegistryIntAccessor()),
        new RegistryStringAccessor(),
        new RegistryIntAccessor());
    }

    private static void RegisterAccessors(params object[] accessors)
    {
      foreach (object accessor in accessors)
      {
        Accessors[accessor.GetType().GetInterface("IConfigAccessor`1").GetGenericArguments()[0]] = accessor;
      }      
    }  

    public ConfigItem<T> get<T>(eConfig item)
    {
      return new ConfigItemWithDefault<T>(item.ToString(), item.getDefault<T>(), getRegistryAccessor<T>());      
    }

    private static IConfigAccessor<T> getRegistryAccessor<T>()
    {
      return (IConfigAccessor<T>)Accessors[typeof(T)];
    }

    /*public ConfigItem<SecureString> get(eConfigEncryptedString item)
    {
      return new ConfigItemWithDefault<SecureString>(item.ToString(), getDefault<string>(item), new TypeConvertingAccessor<SecureString,string>(new EncryptingConverter(), new RegistryStringAccessor()));
    }*/

  }
}
