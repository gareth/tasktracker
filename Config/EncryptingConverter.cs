﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Security.Cryptography;

namespace Tasks3.Config
{
  public class EncryptingConverter : TypeConverter<SecureString, string>
  {
    private static byte[] entropy = Encoding.UTF8.GetBytes("Secret s4uce!");

    public EncryptingConverter()
      : base(s => encode(s), s => decode(s))
    {
    }

    private static string encode(SecureString input)
    {
      if (input == null)
      {
        return null;
      }

      byte[] data = UTF8Encoding.UTF8.GetBytes(input);
      byte[] encoded = ProtectedData.Protect(data, entropy, DataProtectionScope.CurrentUser);
      return Convert.ToBase64String(encoded);
    }

    private static string decode(string input)
    {
      if (input == null)
      {
        return null;
      }

      try
      {

        byte[] data = Convert.FromBase64String(input);
        byte[] encoded = ProtectedData.Unprotect(data, entropy, DataProtectionScope.CurrentUser);
        return Encoding.UTF8.GetString(encoded);
      }
      catch (Exception)
      {
        //todo log
        return null;
      }
    }

  }
}
