﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Win32;


namespace Tasks3.Config
{
  static class Registry
  {
    private const string REG_KEY = @"Software\Gareth\Tasks3\";

    internal static bool GetRegistryString(string name, out string value)
    {
      object rawValue = null;
      value = null;

      if (Key != null)
      {
        rawValue = Key.GetValue(name);
        value = rawValue as string;
        return value != null;
      }

      return false;
    }

    internal static bool GetRegistryInt(string name, out int value)
    {
      object rawValue = null;
      value = 0;

      if (Key != null)
      {
        rawValue = Key.GetValue(name);

        if (rawValue != null)
        {
          value = (int)rawValue;
          return true;
        }
      }

      return false;      
    }

    internal static void SetRegistryString(string valueName, string value)
    {
      if (value != null)
      {
        Key.SetValue(valueName, value);
      }
      else
      {
        Key.DeleteValue(valueName);
      }
    }

    internal static void SetRegistryInt(string valueName, int value)
    {
      Key.SetValue(valueName, value);
    }

    private static RegistryKey Key
    {
      get { return Microsoft.Win32.Registry.CurrentUser.CreateSubKey(REG_KEY); }
    }
  }
}
