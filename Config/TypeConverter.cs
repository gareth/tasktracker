﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  public class TypeConverter<S,T> 
  {
    private Func<S,T> left;
    private Func<T,S> right;

    public TypeConverter(Func<S, T> left, Func<T, S> right)
    {
      this.left = left;
      this.right = right;
    }

    public T convertLeft(S input) { return left(input); }
    public S convertRight(T input) { return right(input); }
  }

  public static class TypeConverters
  {
    public static TypeConverter<TimeSpan, int> Minutes = new TypeConverter<TimeSpan, int>(t => (int)t.TotalMinutes, i => TimeSpan.FromMinutes(i));
    public static TypeConverter<DateTime, string> DateTime = new TypeConverter<DateTime, string>(t => t.Ticks.ToString(), i => new DateTime(long.Parse(i)));
    public static TypeConverter<bool, int> Boolean = new TypeConverter<bool, int>(b => b ? 1 : 0, i => i != 0);
  }
}
