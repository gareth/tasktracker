﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  public class RegistryIntAccessor : Tasks3.Config.IConfigAccessor<int>
  {
    public void persist(string name, int value)
    {
      Registry.SetRegistryInt(name, value);
    }

    public bool retrieve(string name, out int value)
    {
      return Registry.GetRegistryInt(name, out value);
    }
  }
}
