﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  public class RegistryStringAccessor : Tasks3.Config.IConfigAccessor<string>
  {
    public void persist(string name, string value)
    {
      Registry.SetRegistryString(name, value);
    }

    public bool retrieve(string name, out string value)
    {
      return Registry.GetRegistryString(name, out value);
    }
  }
}
