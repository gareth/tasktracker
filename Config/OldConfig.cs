using System;
using System.IO;
using Microsoft.Win32;
using System.Net;

namespace Tasks3.Config
{
  public partial class Config 
  {
    private const string REG_KEY = @"Software\Gareth\Tasks3\";
    private const string OLD_REG_KEY = @"Software\Gareth\Tasks\";

    const string DIRNAME  = "tasks";
    const string SSDIRNAME  = "TaskTrackerScreenShots";
    const string FILENAME = "tasklist.tl3";

    public TaskList TaskList { get; set; }

    public DateTime VERSION
    {
      get { return new DateTime(2011, 02, 22); }
    }

    public TopForm TopForm { get; set; }

    public ICredentials TsaCredentials
    {
      get
      {
        string credentials = GetRegistryString("TSACredentials", null);

        if (credentials == null)
        {
          return CredentialCache.DefaultCredentials;
        }
        else
        {
          var parts = credentials.Split('/', 3);
          return new NetworkCredential(parts[1], parts[2], parts[0]);
        }
      }
    }

    public string TimesheetAppUrl
    {
        get
        {
            return GetRegistryString("TimesheetAppUrl", "http://vtimesheet/");
        }
        set
        {
            SetRegistryString("TimesheetAppUrl", value);
        }
    }

    public TimeSpan ArchiveInterval
    {
      get
      {
        return new TimeSpan(GetRegistryInt("ArchiveIntervalDays", 90), 0, 0, 0, 0);
      }
      set
      {
        SetRegistryInt("ArchiveIntervalDays", (int)value.TotalDays);
      }
    }

    public TimeSpan MinCountablePeriod
    {
      get
      {
        return new TimeSpan(0, 0, 0, GetRegistryInt("MinCountablePeriodSecs", 60), 0);
      }
      set
      {
        SetRegistryInt("MinCountablePeriodSecs", (int)value.TotalSeconds);
      }
    }

    public TimeSpan BugzillaImportFrequency
    {
      get
      {
        return new TimeSpan(0, 0, GetRegistryInt("BugzillaImportFrequency", 300));
      }
      set
      {
        SetRegistryInt("BugzillaImportFrequency", (int)value.TotalSeconds);
      }
    }

    public TimeSpan JiraImportFrequency
    {
      get
      {
        return new TimeSpan(0, 0, GetRegistryInt("JiraImportFrequency", 300));
      }
      set
      {
        SetRegistryInt("JiraImportFrequency", (int)value.TotalSeconds);
      }
    }

    public TimeSpan NoTaskReminderInterval
    {
      get
      {
        return new TimeSpan(0, 0, 0, GetRegistryInt("NoTaskReminderInveralSecs", 300), 0);
      }
      set
      {
        SetRegistryInt("NoTaskReminderInveralSecs", (int)value.TotalSeconds);
      }
    }

    public TimeSpan NagInterval
    {
      get
      {
        return new TimeSpan(0, 0, 0, GetRegistryInt("NagInveralSecs", 0), 0);
      }
      set
      {
        SetRegistryInt("NagInveralSecs", (int)value.TotalSeconds);
      }
    }

    public bool AllowNoTaskAssignment
    {
      get
      {
        return GetRegistryInt("AllowNoTask", 1) == 1;
      }
      set
      {
        SetRegistryInt("AllowNoTask", value ? 1 : 0);
      }
    }

    public TimeSpan AutoSaveInterval
    {
      get
      {
        return TimeSpan.FromSeconds(600);
      }
    }

    public TimeSpan ActivityCaptureInterval
    {
      get
      {
        return TimeSpan.FromSeconds(GetRegistryInt("ActivityCaptureIntervalSecs", 60));
      }
      set
      {
        SetRegistryInt("ActivityCaptureIntervalSecs", (int)value.TotalSeconds );
      }
    }

    public TimeSpan AutoPauseInterval
    {
      get
      {
       return TimeSpan.FromMinutes(GetRegistryInt("AutoPauseIntervalMins", 120));
      }
      set
      {
        SetRegistryInt("AutoPauseIntervalMins", (int)value.TotalMinutes);
      }
    }

    public TimeSpan ReminderInterval
    {
      get
      {
        return new TimeSpan(0, 0, GetRegistryInt("ReminderIntervalMins", 5), 0, 0);
      }
      set
      {
        SetRegistryInt("ReminderIntervalMins", (int)value.TotalMinutes);
      }
    }

    public int ToolTipUpdateInterval
    {
      get
      {
        return 10;
      }
    }

    public bool EnableReassignPrompt
    {
      get
      {
        return GetRegistryInt("PromptForReassign", 1) == 1;
      }
      set
      {
        SetRegistryInt("PromptForReassign", value ? 1 : 0);
      }
    }

    public bool ShowInTaskbar
    {
      get
      {
        return GetRegistryInt("ShowInTaskbar", IsWin7 ? 1 : 0 ) == 1;
      }
      set
      {
        SetRegistryInt("ShowInTaskbar", value ? 1 : 0);
        TopForm.ShowInTaskbar = value;
      }
    }

    private bool IsWin7
    {
      get
      {
        return Environment.OSVersion.Version.Major > 6 ||
          (Environment.OSVersion.Version.Major == 6 && Environment.OSVersion.Version.Minor > 0);
      }
    }


    public bool EnableBugzillaIntegration
    {
      get
      {
        return GetRegistryInt("BugzillaEnabled", 0) == 1;
      }
      set
      {
        SetRegistryInt("BugzillaEnabled", value ? 1 : 0);
      }
    }

    public bool EnableJiraIntegration
    {
      get
      {
        return GetRegistryInt("JiraEnabled", 0) == 1;
      }
      set
      {
        SetRegistryInt("JiraEnabled", value ? 1 : 0);
      }
    }

    public bool PauseWhenStationLocked
    {
      get
      {
        return GetRegistryInt("PauseOnLock", 0) == 1;
      }
      set
      {
        SetRegistryInt("PauseOnLock", value ? 1 : 0);
      }
    }

    public bool MinimiseToSystray
    {
      get
      {
        return GetRegistryInt("MinimiseToSystray", 0) == 1;
      }
      set
      {
        SetRegistryInt("MinimiseToSystray", value ? 1 : 0);
      }
    }

    public bool FocusRecentList
    {
      get
      {
        return GetRegistryInt("FocusRecentList", 1) == 1;
      }
      set
      {
        SetRegistryInt("FocusRecentList", value ? 1 : 0);
      }
    }

    public bool AutoHide
    {
      get
      {
        return GetRegistryInt("AutoHide", 1) == 1;
      }
      set
      {
        SetRegistryInt("AutoHide", value ? 1 : 0);
      }
    }

    public string BugzillaUser
    {
      get
      {
        return GetRegistryString("BugzillaUser", "");
      }
      set
      {
        SetRegistryString("BugzillaUser", value);
      }
    }

    public string BugzillaPassword
    {
      get
      {
        return GetRegistryString("BugzillaPassword", "");
      }
      set
      {
        SetRegistryString("BugzillaPassword", value);
      }
    }

    public string FileLocation
    {
      get
      {
        return @"c:\users\gre\desktop\dcc.tl3";

        string lOld = GetRegistryStringOld("TasksFilename", Path.Combine(GetDefaultDirectory(), FILENAME)).Replace(".tsk", ".tl3");
        string lRet = GetRegistryString("Tasks3Filename", lOld);
        
        if (!Directory.Exists(Path.GetDirectoryName(lRet)) && Path.GetDirectoryName(lRet) != "")
        {
          Directory.CreateDirectory(Path.GetDirectoryName(lRet));
        }

        return lRet;
      }
      set
      {
        if (FileLocation != value)
        {
          SetRegistryString("Tasks3Filename", value);
          TaskList.FileName = value;
        }
      }      
    }

    public string ScreenShotLocation
    {
      get
      {
        string lDir = Path.Combine(GetDefaultDirectory(), SSDIRNAME);
        lDir =  GetRegistryString("ScreenShotLocation", lDir);

        if (!Directory.Exists(lDir))
        {
          Directory.CreateDirectory(lDir);
        }

        return lDir;
      }
      set
      {
        if (FileLocation != value)
        {
          if (value.EndsWith("\\"))
          {
            value = value.Substring(0, value.Length - 1);
          }
          SetRegistryString("ScreenShotLocation", value);
        }
      }
    }

    public string VersionFileLocation
    {
      get
      {
        return GetRegistryString("VersionFile", @"\\stingray\software\TaskTracker\Latest\LatestVersion.txt");
      }
      set
      {
        SetRegistryString("VersionFile", value);
      }
    }

    private string GetDefaultDirectory()
    {
     return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DIRNAME);
    }

    private void SetRegistryString(string xiValueName, string xiValue)
    {
      if (xiValue != null)
      {
        Microsoft.Win32.Registry.CurrentUser.CreateSubKey(REG_KEY).SetValue(xiValueName, xiValue);
      }
      else
      {
        Microsoft.Win32.Registry.CurrentUser.CreateSubKey(REG_KEY).DeleteValue(xiValueName);
      }
    }

    private string GetRegistryString(string xiValue, string xiDefault)
    {
      RegistryKey lKey   = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(REG_KEY);
      object      lValue = null;

      if (lKey != null)
      {
        lValue = lKey.GetValue(xiValue);
      }

      return lValue == null ? GetRegistryStringOld(xiValue, xiDefault) : (string)lValue;
    }

    private string GetRegistryStringOld(string xiValue, string xiDefault)
    {
        RegistryKey lKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(OLD_REG_KEY);
        object lValue = null;

        if (lKey != null)
        {
            lValue = lKey.GetValue(xiValue);
        }

        return lValue == null ? xiDefault : (string)lValue;
    }


    private int GetRegistryInt(string xiValue, int xiDefault)
    {
      RegistryKey lKey   = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(REG_KEY);
      object      lValue = null;

      if (lKey != null)
      {
        lValue = lKey.GetValue(xiValue);
      }

      return lValue == null ? GetRegistryIntOld(xiValue, xiDefault) : (int)lValue;
    }

    private int GetRegistryIntOld(string xiValue, int xiDefault)
    {
      RegistryKey lKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(OLD_REG_KEY);
      object lValue = null;

      if (lKey != null)
      {
        lValue = lKey.GetValue(xiValue);
      }

      return lValue == null ? xiDefault : (int)lValue;
    }


    private void SetRegistryInt(string xiValueName, int xiValue)
    {
      Microsoft.Win32.Registry.CurrentUser.CreateSubKey(REG_KEY).SetValue(xiValueName, xiValue);
    }

  }
}