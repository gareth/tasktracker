﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  internal partial class ConfigOld : Tasks3.Config.Config
  {
    private ConfigItem<SecureString> jiraPassword =
        new ConfigItem<SecureString>(
          "JiraPassword",
          null,
          new TypeConvertingStore<SecureString, string>(new EncryptingConverter(), new RegistryStringAccessor()),
          "JIRA password",
          "The password for your JIRA account (will be stored securely)");

    public string JiraPassword
    {
      get { return jiraPassword.Value;  }
      set { ; }
    }
  }
}
