﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  public interface IConfigAccessor<T>
  {
    void persist(string name, T value);
    bool retrieve(string name, out T value);
  }
}
