﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  public class SecureString
  {
    private string backingString;

    public static implicit operator string (SecureString s) { return s == null ? null : s.backingString; }
    public static implicit operator SecureString(string s) { return s == null ? null : new SecureString() { backingString = s }; }
  }
}
