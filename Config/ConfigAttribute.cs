﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  [AttributeUsage(AttributeTargets.Field)]
  class ConfigAttribute : Attribute
  {
    public object Default {get; set; }
    public Type Type { get; set; }
    public string Label { get; set; }
    public string Description { get; set; }

    public ConfigAttribute()
    {
    }

    public ConfigAttribute(Type t, object defaultValue = null, string Label = null, string Description = null)
    {
      Default = defaultValue;
      this.Type = t;
      this.Label = Label;
      this.Description = Description;
    }
  }

  public static class ConfigAttributeHelper
  {
    private static ConfigAttribute GetAttribute(this eConfig config)
    {
      return config.GetAttributeOfType<ConfigAttribute>();
    }

    public static Type GetConfigType(this eConfig config)
    {
      return config.GetAttribute().Type;
    }

    public static string GetLabel(this eConfig config)
    {
      return config.GetAttribute().Label;
    }

    public static string GetConfigDescription(this eConfig config)
    {
      return config.GetAttribute().Description;
    }

    public static T getDefault<T>(this eConfig config)
    {
      object ret = config.GetAttribute().Default ?? default(T);

      if (typeof(T) != config.GetConfigType())
      {
        throw new Exception(string.Format("Config has type {0}, but was accessed as if of type {1}", config.GetType(), typeof(T)));
      }

      return (T) ret;
    }
  }
}
