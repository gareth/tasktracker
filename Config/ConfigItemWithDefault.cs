﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  public class ConfigItemWithDefault<T> : ConfigItem<T>
  {
    private T defaultValue;
    private Tasks3.Config.IConfigAccessor<T> store;

    private readonly string name;


    public ConfigItemWithDefault(string name, T defaultValue, Tasks3.Config.IConfigAccessor<T> store)
    {
      this.name = name;
      this.defaultValue = defaultValue;
      this.store = store;
    }

    public override T Value
    {
      get 
      {
        T value;
        if (store.retrieve(name, out value))
        {
          return value;
        }
        return defaultValue;
      }
      set { store.persist(name, value); }      
    }
  }
}
