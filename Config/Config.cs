﻿using System;
using System.Net;

namespace Tasks3.Config
{
  public enum eConfig
  {
    [Config(Type=typeof(string), Label="JIRA username", Description ="Login name for JIRA integration")]
    jiraUser,
    [Config(Type = typeof(SecureString), Label = "JIRA password", Description = "Password for JIRA integration (stored securely)")]
    jiraPassword,
    [Config(Type=typeof(DateTime))]
    lastKnownDatestamp,
    [Config(Type=typeof(bool), Label="Show recent tasks", Description ="Show recent list when selecting tasks", Default = true)]
    showRecentTasks,


    /*public TimeSpan ActivityCaptureInterval { get; set; }
    public bool AllowNoTaskAssignment { get; set; }
    public TimeSpan AutoPauseInterval { get; set; }
    public TimeSpan ArchiveInterval { get; set; }
    public TimeSpan AutoSaveInterval { get; set; }
    public TimeSpan BugzillaImportFrequency { get; set; }
    public TimeSpan JiraImportFrequency { get; set; }
    public string BugzillaPassword { get; set; }
    public string BugzillaUser { get; set; }
    public string JiraPassword { get; set; }
    public string JiraUser { get; set; }
    public bool EnableBugzillaIntegration { get; set; }
    public bool EnableJiraIntegration { get; set; }
    public bool EnableReassignPrompt { get; set; }
    public string FileLocation { get; set; }
    public bool FocusRecentList { get; set; }
    public TimeSpan MinCountablePeriod { get; set; }
    public bool MinimiseToSystray { get; set; }
    public TimeSpan NagInterval { get; set; }
    public TimeSpan NoTaskReminderInterval { get; set; }
    public bool PauseWhenStationLocked { get; set; }
    public TimeSpan ReminderInterval { get; set; }
    public string ScreenShotLocation { get; set; }
    public bool ShowInTaskbar { get; set; }
    public bool AutoHide { get; set; }
    public TimeSheetAppConnector.eIntegrationMode TimesheetAppIntegrationMode { get; set; }
    public string TimesheetAppUrl { get; set; }
    public ICredentials TsaCredentials { get; set; }
    public int ToolTipUpdateInterval { get; set;  }
    public DateTime VERSION { get; set; }
    public string VersionFileLocation { get; set; }
    public TopForm TopForm { get; set; }*/
  }  
}
