﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  class TypeConvertingAccessor<T,B> : Tasks3.Config.IConfigAccessor<T>
  {
    private TypeConverter<T, B> converter;
    private Tasks3.Config.IConfigAccessor<B> backingStore;

    public TypeConvertingAccessor(TypeConverter<T, B> converter, Tasks3.Config.IConfigAccessor<B> backingStore)
    {
      this.converter = converter;
      this.backingStore = backingStore;
    }

    public void persist(string name, T value)
    {
      backingStore.persist(name, converter.convertLeft(value));      
    }

    public bool retrieve(string name, out T value)
    {
      value = default(T);
      B unconverted;

      if (backingStore.retrieve(name, out unconverted))
      {
        value = converter.convertRight(unconverted);
        return true;
      }

      return false;
    }
  }
}
