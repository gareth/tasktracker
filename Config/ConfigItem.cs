﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks3.Config
{
  public abstract class ConfigItem<T> 
  {
    public abstract T Value
    {
      get; set;
    }

    public static implicit operator T(ConfigItem<T> i) { return i.Value; }
  }

}
