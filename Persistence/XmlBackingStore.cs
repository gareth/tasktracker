﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Tasks3.Persistence
{
  public class XmlBackingStore : TaskList.ITaskListPersist
  {

 
    public void Persist(TaskList tasklist)
    {
      using (StreamWriter lWriter = new StreamWriter(tasklist.FileName + ".tmp"))
      {
        XmlSerializer lSerializer = new XmlSerializer(typeof(TaskList));
        lSerializer.Serialize(lWriter, tasklist);
      }

      if (File.Exists(tasklist.FileName))
      {
        File.Delete(tasklist.FileName);
      }
      File.Move(tasklist.FileName + ".tmp", tasklist.FileName);

      File.Delete(tasklist.FileName + ".bak");

      var lBackup = tasklist.FileName + "." + DateTime.Today.ToString("yyyyMMdd") + ".autobackup";
      if (!File.Exists(lBackup))
      {
        File.Copy(tasklist.FileName, lBackup);
      }

      foreach (FileInfo lFile in new DirectoryInfo(Path.GetDirectoryName(tasklist.FileName)).GetFiles())
      {
        if (lFile.Name.EndsWith(".autobackup") && lFile.CreationTime.AddDays(14) < DateTime.Today)
        {
          lFile.Delete();
        }
      }
    }

    public TaskList Load(Config.Config config)
    {
      if (!File.Exists(config.FileLocation) && File.Exists(config.FileLocation + ".tmp"))
      {
        File.Copy(config.FileLocation + ".tmp", GetUniqueFileName(config.FileLocation + ".bak"), false);
        System.Windows.Forms.MessageBox.Show("Recovering after failed save. Some changes may have been lost");
        File.Move(config.FileLocation + ".tmp", config.FileLocation);
      }

      TaskList ret = null;

      using (StreamReader lReader = new StreamReader(config.FileLocation))
      {
        XmlSerializer lSerializer = new XmlSerializer(typeof(TaskList));
        ret = (TaskList)lSerializer.Deserialize(lReader);

        ret.SetParents(); //qq+ not needed?
      }

      return ret;
    }

    private static string GetUniqueFileName(string fileName)
    {
      int ii = 0;
      string ret = fileName;

      while (File.Exists(ret))
      {
        ret = fileName + (++ii).ToString();
      }

      return ret;
    }
  }
}
