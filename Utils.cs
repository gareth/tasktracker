using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace Tasks3
{
  public static class Utils
  {
    public static void FormatFromTask(this Control control, Task t, ToolTip tip)
    {
      control.Text = t.QualifiedName.Length > 40 ? t.Description : t.QualifiedName;
      control.BackColor = t.Colour;
      control.ForeColor = t.Colour.ComplementaryTextColor();
      control.Tag = t;
      
      if (t.Deleted)
      {
        control.Font = new Font(control.Font, FontStyle.Strikeout);
        control.BackColor = Color.Black;
        control.ForeColor = Color.Gray;
      }
      else if (t.Complete)
      {
        control.Font = new Font(control.Font, FontStyle.Strikeout);
      }
      else if (control.Font.Style != FontStyle.Regular)
      {
        control.Font = new Font(control.Font, FontStyle.Regular);
      }

      tip.SetToolTip(control, t.QualifiedName);
    }

    public static Color ComplementaryTextColor(this Color c)
    {
      return c.GetBrightness() > 0.45 ? Color.Black : Color.White;
    }

    public static string[] Split(this string xiThis, char c)
    {
      return xiThis.Split(new[] {c});
    }

    public static string[] Split(this string xiThis, char c, int max)
    {
      return xiThis.Split(new[] { c }, max);
    }

    public static T[] AsArray<T>(this T xiThis)
    {
      return new T[] {xiThis};
    }

    public static TimeSpan Sum(this IEnumerable<TimeSpan> xiElements)
    {
      return TimeSpan.FromTicks(xiElements.Sum(t => t.Ticks));
    }

    public static TimeSpan Sum<T>(this IEnumerable<T> xiThis, Func<T, TimeSpan> xiFunc)
    {
      return xiThis.Select(x => xiFunc(x)).Sum();
    }

    public static string Left(this string xiString, int xiLength, bool xiEllipsis)
    {
      xiString = xiString ?? "";
      return xiLength >= xiString.Length ? xiString : xiString.Substring(0, xiLength - 2) + "...";
    }

    public static bool IsValidScreenLocation(this Point xiPoint)
    {
      foreach (Screen lScreen in Screen.AllScreens)
      {
        if (lScreen.Bounds.Contains(xiPoint))
        {
          return true;
        }
      }
      return false;
    }

    public static string ToString(this TimeSpan xiTimeSpan, bool xiDecimal)
    {
      if (xiDecimal)
      {
        return xiTimeSpan.TotalHours.ToString("0.00");
      }
      else
      {
        return ToString(xiTimeSpan);
      }
    }

    public static string ToStringExcludeZero(this TimeSpan xiTimeSpan, bool xiDecimal)
    {
      if (xiTimeSpan <= TimeSpan.Zero)
      {
        return "-";
      }
      return xiTimeSpan.ToString(xiDecimal) + string.Format(" ({0:0.00} days)", xiTimeSpan.TotalHours / 7.5);
    }

    private static string ToString(TimeSpan xiTimeSpan)
    {
      return (xiTimeSpan.Days * 24 + xiTimeSpan.Hours).ToString("00") + ":"
        + xiTimeSpan.Minutes.ToString("00") + ":"
        + xiTimeSpan.Seconds.ToString("00");
    }

    public static void OnAutoCompleteKeyPress(object sender, KeyPressEventArgs e)
    {
      ComboBox lComboBox = (ComboBox)sender;
      if (!e.KeyChar.Equals((char)8))
      {
        SearchItems(lComboBox, ref e);
      }
    }

    /// <summary>
    /// Searches the combo box item list for a match and selects it.
    /// If no match is found, then selected index defaults to -1.
    /// </summary>
    /// 
    private static void SearchItems(ComboBox xiComboBox, ref KeyPressEventArgs e)
    {
      int lSelectionStart = xiComboBox.SelectionStart;
      int lSelectionLength = xiComboBox.SelectionLength;
      int lSelectionEnd = lSelectionStart + lSelectionLength;

      int lIndex = xiComboBox.FindString(
        xiComboBox.Text.Substring(0, lSelectionStart) +
        e.KeyChar.ToString() +
        xiComboBox.Text.Substring(lSelectionEnd));

      if (lIndex != -1)
      {
        xiComboBox.SelectedIndex = lIndex;
        xiComboBox.Select(lSelectionStart + 1, xiComboBox.Text.Length - (lSelectionStart + 1));
        e.Handled = true;
      }
    }

    public static void FollowLink(string xiLink)
    {
      try
      {
        string[] lLinkComponents = xiLink.Split(':');

        switch (lLinkComponents[0])
        {
          case "Outlook":
            /* qqOutlook._Application lApplication = new Outlook.ApplicationClass();
            ((Outlook.MailItem)lApplication.Session.GetItemFromID(lLinkComponents[1], lLinkComponents[2])).Display(false);
            break; */
          case "http":
          case "https":
            ProcessStartInfo lProcess = new ProcessStartInfo(xiLink);
            Process.Start(lProcess);
            break;
          default:
            throw new Exception("unrecognised link type");
        }
      }
      catch
      {
        MessageBox.Show("Unable to follow link");
      }
    }

    public static void LogError(Exception e)
    {
      try
      {
        using (StreamWriter lWriter = File.AppendText(Path.Combine(Directory.GetCurrentDirectory(), "ErrorLog.txt")))
        {
          lWriter.WriteLine("");
          lWriter.WriteLine(DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
          lWriter.WriteLine(e.ToString());
        }
      }
      catch
      {
      }
    }

    public static Point CheckLocation(Point xiDesiredLocation)
    {
      Rectangle lScreen = Screen.GetWorkingArea(xiDesiredLocation);

      if (xiDesiredLocation.X > lScreen.Right)
      {
        xiDesiredLocation.X = lScreen.Right - 50;
      }
      if (xiDesiredLocation.Y > lScreen.Bottom)
      {
        xiDesiredLocation.Y = lScreen.Bottom - 50;
      }

      return xiDesiredLocation;
    }

    public static Color RandomColor()
    {
      return Color.FromArgb(mRandom.Next(256), mRandom.Next(256), mRandom.Next(256));
    }

    public static DateTime GetStartOfWeek()
    {
      return GetStartOfWeek(DateTime.Now);
    }

    public static DateTime GetStartOfWeek(DateTime date)
    {
      return date.Date.AddDays(-1 * (((int)date.DayOfWeek + 6) % 7));
    }

    public static DateTime EffectiveDate(this DateTime datetime)
    {
      if (datetime > datetime.Date && datetime.Hour < Data.TimeLine.CUTOFF_HOUR)
      {
        return datetime.AddDays(-1).Date;
      }
      return datetime.Date;
    }

    public static Data.AdjustedTimeOfDay AdjustedTimeOfDay(this DateTime date)
    {
      return new Data.AdjustedTimeOfDay(date.TimeOfDay);
    }

    public static void DrawBlob(
          Color xiMainColor,
          Color xiTLColor,
          Color xiBRColor,
          Color xiBGColor,
          Graphics xiGraphics,
          Point xiOffset,
          Size xiSize)
    {
      Color lMidColor = Color.FromArgb(
        (xiBGColor.R + xiTLColor.R + xiMainColor.R) / 3,
        (xiBGColor.G + xiTLColor.G + xiMainColor.G) / 3,
        (xiBGColor.B + xiTLColor.B + xiMainColor.B) / 3);

      Point lTR = new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y);
      Point lBL = new Point(xiOffset.X, xiOffset.Y + xiSize.Height - 1);
      Point lBR = new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y + xiSize.Height - 1);

      Brush lBrush = new SolidBrush(xiMainColor);
      xiGraphics.FillRectangle(lBrush, new Rectangle(xiOffset + new Size(1, 1), xiSize - new Size(2, 2)));

      Pen lPen = new Pen(xiTLColor);
      xiGraphics.DrawLine(lPen, OffsetX(xiOffset, 1), OffsetX(lTR, -1));
      xiGraphics.DrawLine(lPen, OffsetY(xiOffset, 1), OffsetY(lBL, -1));
      //xiGraphics.DrawLine(lPen, xiOffset, lBR);
      xiGraphics.FillRectangle(new SolidBrush(xiTLColor), new Rectangle(Offset(xiOffset, 1, 1), new Size(1, 1)));
      xiGraphics.FillRectangle(new SolidBrush(lMidColor), new Rectangle(Offset(lTR, -1, 1), new Size(1, 1)));
      lPen = new Pen(xiBRColor);
      xiGraphics.DrawLine(lPen, new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y + 1), new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y + xiSize.Height - 2));
      xiGraphics.DrawLine(lPen, new Point(xiOffset.X + 1, xiOffset.Y + xiSize.Height - 1), new Point(xiOffset.X + xiSize.Width - 2, xiOffset.Y + xiSize.Height - 1));
      xiGraphics.FillRectangle(new SolidBrush(xiBRColor), new Rectangle(Offset(lBR, -1, -1), new Size(1, 1)));
      xiGraphics.FillRectangle(new SolidBrush(lMidColor), new Rectangle(Offset(lBL, 1, -1), new Size(1, 1)));




      /*lPen = new Pen(Color.Transparent);
      lBrush = new SolidBrush(Color.Transparent);
      xiGraphics.DrawLine(lPen, xiOffset, xiOffset);
      xiGraphics.DrawLine(lPen, lTR, OffsetX(lTR, -1));
      xiGraphics.DrawLine(lPen, lTR, OffsetY(lTR, 1));
      xiGraphics.FillRectangle(lBrush, new Rectangle(lBR, new Size(1, 1)));
      xiGraphics.DrawLine(lPen, lBL, OffsetX(lBL, 1));
      xiGraphics.DrawLine(lPen, lBL, OffsetY(lBL, -1));*/
    }

    public static DateTime Min(DateTime d1, DateTime d2)
    {
      return d1.Ticks < d2.Ticks ? d1 : d2;
    }

    public static DateTime Max(DateTime d1, DateTime d2)
    {
      return d1.Ticks >= d2.Ticks ? d1 : d2;
    }

    public static TimeSpan Min(TimeSpan d1, TimeSpan d2)
    {
      return d1.Ticks < d2.Ticks ? d1 : d2;
    }

    public static TimeSpan Max(TimeSpan d1, TimeSpan d2)
    {
      return d1.Ticks >= d2.Ticks ? d1 : d2;
    }

    private static Point OffsetX(Point xiPoint, int xiDx)
    {
      Point lRet = xiPoint;
      lRet.Offset(xiDx, 0);
      return lRet;
    }

    private static Point OffsetY(Point xiPoint, int xiDy)
    {
      Point lRet = xiPoint;
      lRet.Offset(0, xiDy);
      return lRet;
    }

    private static Point Offset(Point xiPoint, int xiDx, int xiDy)
    {
      Point lRet = xiPoint;
      lRet.Offset(xiDx, xiDy);
      return lRet;
    }

    public static string Ordinal(this int num)
    {
      var n = num.ToString();

      switch (num % 100)
      {
        case 11:
        case 12:
        case 13:
          return n + "th";
      }

      switch (num % 10)
      {
        case 1:
          return n + "st";
        case 2:
          return n + "nd";
        case 3:
          return n + "rd";
        default:
          return n + "th";
      }
    }

    public static string ToNiceDateString(this DateTime date)
    {
      return string.Format("{0} {1:MMMM yyyy}", date.Day.Ordinal(), date);
    }

    /*    public static void DrawBlob(
          Color    xiMainColor,
          Color    xiTLColor,
          Color    xiBRColor,
          Graphics xiGraphics,
          Point    xiOffset,
          Size     xiSize)
        {
          Pen lPen = new Pen(xiTLColor);
          xiGraphics.DrawLine(lPen, xiOffset, new Point(xiOffset.X, xiOffset.Y + xiSize.Height - 1));
          xiGraphics.DrawLine(lPen, xiOffset, new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y));
          lPen = new Pen(xiBRColor);
          xiGraphics.DrawLine(lPen, new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y + 1), new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y + xiSize.Height - 1));
          xiGraphics.DrawLine(lPen, new Point(xiOffset.X + 1, xiOffset.Y + xiSize.Height - 1), new Point(xiOffset.X + xiSize.Width - 1, xiOffset.Y + xiSize.Height - 1));

          Brush lBrush = new SolidBrush(xiMainColor);
          xiGraphics.FillRectangle(lBrush, new Rectangle(xiOffset + new Size(1,1), xiSize - new Size(2,2)));
        }
    */
    private static Random mRandom = new Random();

  }
}
      