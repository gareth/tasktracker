using System;
using System.Drawing;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Linq;

namespace Tasks3
{
  public class Task
  {
    #region Member data

    protected bool mBlocked;
    protected string mLinkId;
    protected string mNotes = "";
    protected string mDescription;
    protected bool mComplete;
    protected bool mDeleted;
    protected string[] mCategories = new string[0];
    protected List<TimePeriod> mTimePeriods = new List<TimePeriod>();
    protected Guid mGuid = Guid.NewGuid();
    protected string mSource;
    protected Dictionary<string, string> mProperties = new Dictionary<string, string>();
    protected Color mColour = Utils.RandomColor();
    protected List<Task> mChildren = new List<Task>();
    protected bool mTrackTime;
    protected bool mLockedForUpdate = false;
    protected bool mChangedWhileLocked = false;
    protected Task mParent;
    protected bool mInheritColour = false;
    protected long mPriority;

    #endregion

    public Task()
    {
      CreationDate = DateTime.Now;
      mPriority = DateTime.Now.Ticks;
    }

    protected virtual Tasks3.Config.Config Config
    {
      get { return Parent.Config; }
    }

    #region Properties

    private string mTimesheetCode;

    public string TimesheetCodePrefix
    {
      get
      {
        return TimesheetCode == null ? null :
          !TimesheetCode.Contains(":") ? TimesheetCode : TimesheetCode.Substring(0, TimesheetCode.IndexOf(':'));
      }
    }

    public string TimesheetCode
    {
      get
      {
        if (mTimesheetCode == null)
        {


          if (mDescription != null && (mDescription.StartsWith("Incident ") || mDescription.StartsWith("Bug ") || mDescription.StartsWith("Review ")))
          {
            mTimesheetCode += mDescription.Replace("Bug ", "BUG:").Replace("Incident ", "INC:").Replace("Review ", "BUG:");
          }
        }

          return mTimesheetCode;

      }
      set
      {
        mTimesheetCode = value;
      }
    }
    
    public long Priority
    {
      get { return mPriority; }
      set { mPriority = value; }
    }
    
    public bool ExpandInTreeView { get; set; }

    [XmlIgnore]
    public IEnumerable<Task> AllChildTasks
    {
      get
      {
        foreach (Task lTask in this.Children)
        {
          foreach (Task lChildTask in lTask.AllChildTasks)
          {
            yield return lChildTask;
          }

          yield return lTask;
        }
      }
    } 

    [XmlIgnore]
    public IEnumerable<Task> Ancestors
    {
      get
      {
        Task current = this;
        while (current.Parent != null)
        {
          current = current.Parent;
          yield return current;
        }
      }
    }   

    [XmlIgnore]
    public bool IsTopLevel
    {
      get
      {
        return Parent == null || Parent.Parent == null;
      }
    }

    // Returns the top level visible task - i.e. not the root of the tree
    [XmlIgnore]
    public Task TopLevelTask
    {
      get
      {
        Task lTask = this;

        while (!lTask.IsTopLevel)
        {
          lTask = lTask.Parent;
        }
        return lTask;
      }
    }

    [XmlIgnore]
    public TaskList TaskList
    {
      get
      {
        return TopNode as TaskList;
      }
    }

    [XmlIgnore]
    public Task Parent
    {
      get { return mParent; }
      set 
      {
        if (mParent != value)
        {
          if (mParent != null)
          {
            if (mParent.Children.Contains(this))
            {
              mParent.Children.Remove(this);
            }
          }
          mParent = value;
          if (!mParent.Children.Contains(this))
          {
            mParent.Children.Add(this);
          }
        }
      }
    }

    // No longer used - preserved only to enable upgrades.
    public string[] CategoryCodes
    {
      get
      {
        return mCategories;
      }
      set
      {
        mCategories = value;
      }
    }

    public DateTime CreationDate
    {
      get;
      set;
    }

    public Guid Guid
    {
      get { return mGuid; }
      set { mGuid = value; }
    }

    public string LinkId
    {
      get { return mLinkId; }
      set { mLinkId = value; }
    }

    public string QualifiedName
    {
      get
      {
        string lName = mDescription;
        Task lTask = this.Parent;

        while (lTask != null && lTask.Parent != null)
        {
          lName = lTask.Description.Left(20, true) + " - " + lName;
          lTask = lTask.Parent;
        }

        return lName;
      }
    }

    public string Description
    {
      get { return mDescription; }
      set
      {
        mDescription = value;
        OnChange(false);
      }
    }

    private bool? archived;

    public bool Archived
    {
      get
      {
        if (!archived.HasValue)
        {
          archived = !TimePeriods.Where(t => t.End + Config.ArchiveInterval > DateTime.Now).Any() && Children.All(c => c.Archived) && !Properties.ContainsKey("BugId");
        }
        return archived.Value;
      }
    }

    public bool Complete
    {
      get { return mComplete; }
      set
      {
        if (value != mComplete)
        {
          mComplete = value;
          OnChange(true);
        }
      }
    }

    public bool Deleted
    {
      get { return mDeleted; }
      set
      {
        if (value != mDeleted)
        {
          mDeleted = value;
          OnChange(true);
        }
      }
    }

    public string Notes
    {
      get { return mNotes; }
      set
      {
        mNotes = value;
        OnChange(false);
      }
    }

    public void BeginUpdate()
    {
      mLockedForUpdate = true;
      mChangedWhileLocked = false;
    }

    public void EndUpdate()
    {
      mLockedForUpdate = false;

      if (mChangedWhileLocked)
      {
        mChangedWhileLocked = false;
        OnChange(false); //qq+ why false
      }
    }

    public List<TimePeriod> TimePeriods
    {
      get { return mTimePeriods; }
      set 
      { 
        mTimePeriods = new List<TimePeriod>(value); 
        foreach (TimePeriod lPeriod in mTimePeriods)
        {
          lPeriod.Task = this;
        }
      }
    }

    public List<Task> Children
    {
      get { return mChildren; }
      set 
      {
        mChildren = new List<Task>(value.Count);
        foreach (Task lTask in value)
        {
          AddChild(lTask);
        }
      }
    }

    public string Source
    {
      get { return mSource; }
      set { mSource = value; }
    }

    public TaskProperty[] PropertyList
    {
      get { return PropertiesFromDictionary(); }
      set { PropertiesToDictionary(value); }
    }

    public int[] RGB
    {
      get { return new int[] { mColour.R, mColour.G, mColour.B }; }
      set { mColour = Color.FromArgb(value[0], value[1], value[2]); }
    }

    public bool InheritColour
    {
      get 
      {
        return !IsTopLevel;
        //return mInheritColour; 
      }
      set 
      { 
        //mInheritColour = value; 
      }
    }      

    [XmlIgnore]
    public IDictionary<string, string> Properties
    {
      get { return mProperties; }
    }

    [XmlIgnore]
    public System.Drawing.Color Colour
    {
      get
      {
        return InheritColour && Parent != null ? Parent.Colour : mColour;
      }
      set
      {
        mColour = value;
      }
    }

    [XmlIgnore]
    public Color LightColor
    {
      get
      {
        return Color.FromArgb(
          Math.Min((Colour.R + 80), 255),
          Math.Min((Colour.G + 80), 255),
          Math.Min((Colour.B + 80), 255));
      }
    }

    [XmlIgnore]
    public Color DarkColor
    {
      get
      {
        return Color.FromArgb(
          Math.Max(0, (Colour.R - 80)),
          Math.Max(0, (Colour.G - 80)),
          Math.Max(0, (Colour.B - 80)));
      }
    }

    protected virtual bool Initialized
    {
      get { return mParent.Initialized; }
    }

    #endregion

    #region Public methods

    public Task Find(Guid xiGuid)
    {
      if (xiGuid == this.Guid)
      {
        return this;
      }

      foreach (Task lChild in Children)
      {
        Task lFound = lChild.Find(xiGuid);

        if (lFound != null)
        {
          return lFound;
        }
      }

      return null;
    }

    public IEnumerable<Task> FindByDescription(string description, StringComparison compareMode)
    {
      if (Description != null && Description.Equals(description, compareMode))
      {
        yield return this;
      }

      foreach (Task lChild in Children)
      {
        foreach (Task lTask in lChild.FindByDescription(description, compareMode))
        {
          yield return lTask;
        }
      }      
    }

    public IEnumerable<Task> FindByDescription(string description)
    {
      return FindByDescription(description, StringComparison.InvariantCultureIgnoreCase);
    }

    public void AddChild(Task xiChild)
    {
      mChildren.Add(xiChild);
      xiChild.Parent = this;

      if (!xiChild.IsTSA)
      {
        TaskList.AddRecent(xiChild);
      }
    }

    public void SetParents()
    {
      foreach (Task lTask in mChildren)
      {
        lTask.Parent = this;
        lTask.SetParents();
      }
    }

    public string GetParentDescriptions()
    {
      string lRet = "";
      Task lTask = this.Parent;

      while (lTask != null && lTask.Parent != null)
      {
        if (lRet != "")
        {
          lRet = lTask.Description + ", " + lRet;
        }
        else
        {
          lRet = lTask.Description;
        }

        lTask = lTask.Parent;
      }

      return lRet;
    }

    public IList<TimePeriod> GetTimePeriods(DateTime xiStart, DateTime xiEnd)
    {      
      List<TimePeriod> lRet = new List<TimePeriod>();
      
      foreach (TimePeriod lTime in TimePeriods)
      {
        if (lTime.Start < xiEnd && lTime.End > xiStart)
        {
          lRet.Add(lTime);
        }
      }

      foreach (Task lTask in mChildren)
      {
        lRet.AddRange(lTask.GetTimePeriods(xiStart, xiEnd));
      }

      lRet.Sort();

      return lRet;
    }

   /* public IList<Task> GetTasks(FilterState xiFilterState)
    {
      return GetTasks(xiFilterState, null);
    }

    public IList<Task> GetTasks(FilterState xiFilterState, Task xiIncludeAlways)
    {
      List<Task> lRet = new List<Task>();

      foreach (Task lTask in mChildren)
      {
        if (lTask.MatchesFilter(xiFilterState) || lTask == xiIncludeAlways)
        {
          lRet.Add(lTask);
          lRet.AddRange(lTask.GetTasks(xiFilterState, xiIncludeAlways));
        }
      }

      return lRet;
    }

    public bool MatchesFilter(FilterState xiFilter)
    {
      return (
        ((xiFilter.FilterMode & eFilterMode.ShowDeleted) != 0) == mDeleted) &&
        (
          ((xiFilter.FilterMode & eFilterMode.ShowChecked) != 0 && Complete) 
          || ((xiFilter.FilterMode & eFilterMode.ShowUnchecked) != 0 && !Complete)
        ) &&
        ((xiFilter.FilterMode & eFilterMode.SpecifiedSource) == 0 || mSource == xiFilter.SourceFilter);
    }*/

    public TimePeriod AddTimePeriod(DateTime start, DateTime end)
    {
        // convert to proper task
        this.IsTSA = false;
       
      if ((end - start) > Config.MinCountablePeriod)
      {
        // Update the existing entry for this time period
        // Note that 
        //  * the "reassign recent" function means that we may pull the start time backwards
        //  * the autopause means we may pull the end time backwards
        //  * the timeline edit means we may add time in the past 
        if (mTimePeriods.Any() && start <= mTimePeriods.Last().End && end >= mTimePeriods.Last().Start)
        {
          var last = mTimePeriods.Last();
          last.End = end;
          last.Start = start;
        }
        else
        {
          mTimePeriods.Add(new TimePeriod(this, start, end));
        }

        return mTimePeriods.Last();
      }
      else
      {
        //=====================================================================
        // Remove any existing time period
        //=====================================================================
        if (mTimePeriods.Count > 0 && start == mTimePeriods.Last().Start && end >= mTimePeriods.Last().End)
        {
          mTimePeriods.RemoveAt(mTimePeriods.Count - 1);
        }

        return null;
      }
    }

    public TimeSpan TryRemoveTime(DateTime xiStart, DateTime xiEnd)
    {
      TimeSpan lRet = new TimeSpan(0);
      List<TimePeriod> lToDelete = new List<TimePeriod>();
      List<TimePeriod> lToAdd = new List<TimePeriod>();

      if (xiStart < xiEnd)
      {
        foreach (TimePeriod lPeriod in TimePeriods)
        {
          if (lPeriod.Start < xiEnd && lPeriod.End > xiStart)
          {
            if (lPeriod.End <= xiEnd)
            {
              if (lPeriod.Start < xiStart.AddMinutes(-1))
              {
                lRet += lPeriod.End - xiStart;
                lPeriod.End = xiStart;
              }
              else
              {
                lRet += lPeriod.Duration;
                lToDelete.Add(lPeriod);
              }
            }
            else
            {
              if (lPeriod.Start >= xiStart)
              {
                lRet += lPeriod.Duration;
                lPeriod.Start = xiEnd;
              }
              else
              {
                lRet += xiEnd - xiStart;

                TimePeriod lNew = new TimePeriod();
                lNew.Start = lPeriod.Start;
                lNew.End = xiStart;
                lToAdd.Add(lNew);

                lPeriod.Start = xiEnd;                
              }
            }
          }
        }
      }

      foreach (TimePeriod lPeriod in lToAdd)
      {
        AddTimePeriod(lPeriod.Start, lPeriod.End);
      }
      foreach (TimePeriod lPeriod in lToDelete)
      {
        DeleteTimePeriod(lPeriod);
      }

      return lRet;
    }

    public void DeleteTimePeriod(TimePeriod xiPeriod)
    {
      if (mTimePeriods.IndexOf(xiPeriod) >= 0)
      {
        mTimePeriods.Remove(xiPeriod);
      }
    }

    public TimeSpan GetTotalTime()
    {
      return TimePeriod.GetTimeSince(mTimePeriods, DateTime.MinValue, DateTime.MaxValue);
    }

    public TimeSpan GetTimeThisWeek()
    {
      return TimePeriod.GetTimeSince(mTimePeriods, Utils.GetStartOfWeek(), DateTime.MaxValue);
    }

    public TimeSpan GetTimeThisWeekWithChildren()
    {
      // TODO - doesn't handle 4am cutoff
      return GetTimeWithChildren(Utils.GetStartOfWeek(), DateTime.Now);
    }

    public TimeSpan GetTime(DateTime xiFrom, DateTime xiTo)
    {
      return TimePeriod.GetTimeSince(mTimePeriods, xiFrom, xiTo);
    }

    public TimeSpan GetTimeWithChildren(DateTime xiFrom, DateTime xiTo)
    {
      return GetTime(xiFrom, xiTo) + Children.Sum(c => c.GetTimeWithChildren(xiFrom, xiTo));
    }

    public Task FindBySource(string xiSource)
    {
      foreach (Task lTask in mChildren)
      {
        if (lTask.Source == xiSource)
        {
          return lTask;
        }
        else if (lTask.FindBySource(xiSource) != null)
        {
          return lTask.FindBySource(xiSource);
        }
      }
      return null;
    }

    public string GetSummary()
    {
      StringBuilder lRet = new StringBuilder();
      lRet.Append(Description + "\r\n");
      lRet.Append(GetParentDescriptions());

      
      if (Notes != null && Notes.Length > 0)
      {
        lRet.Append("\r\nNotes: " + GetToolTipNotes());
      }

      TimeSpan lDelta = new TimeSpan(0);

     /* qq+ incomplete
      * if (Tasks.Instance.SelectedTask == this)
      {
        lDelta = Tasks.Instance.Timer;
        lRet.AppendFormat("\r\nTime: {0}", Utils.ToString(lDelta));
      }
      * */

      TimeSpan lThisWeek = GetTimeThisWeek() + lDelta;
      if (lThisWeek.TotalMinutes > 0)
      {
        lRet.AppendFormat("\r\nThis week: {0}", lThisWeek.ToString(false));
      }

      TimeSpan lTotal = GetTotalTime() + lDelta;
      if (lTotal.TotalMinutes > 0)
      {
        lRet.AppendFormat("\r\nTotal: {0}", lTotal.ToString(false));
      }                                                          

      return lRet.ToString();
    }

    public string GetToolTipNotes()
    {
      int lEndIndex = 0;

      for (int ii = 0; ii < 30 && lEndIndex >= 0; ++ii)
      {
        lEndIndex = Notes.IndexOf("\n", lEndIndex + 1);
      }

      return lEndIndex < 0 ? Notes : Notes.Substring(0, lEndIndex);
    }

    public override string ToString()
    {
      return Description;
    }

    public void SwapWith(Task xiSwapWith)
    {
      lock (TopNode)
      {
        Task lParent = this.Parent;
        Task lSwapParent = xiSwapWith.Parent;

        if (lParent == lSwapParent)
        {
          int lIndex1 = lParent.mChildren.IndexOf(this);
          int lIndex2 = lParent.mChildren.IndexOf(xiSwapWith);

          lParent.mChildren[lIndex1] = xiSwapWith;
          lParent.mChildren[lIndex2] = this;
        }
        else
        {
          this.Parent = xiSwapWith.Parent;
        }
      }

      OnChange(true);
    }

    public Task TopNode
    {
      get
      {
        Task lTask = this;
        while (lTask.Parent != null)
        {
          lTask = lTask.Parent;
        }
        return lTask;
      }
    }


    public void SendToTop()
    {
      lock (TopNode)
      {
        long lMax = TopNode.AllChildTasks.Max(T => T.Priority);
        this.Priority = lMax + 1000;
      }

/*      lock (this)
      {
        int lTaskIndex = Parent.mChildren.IndexOf(this);

        if (lTaskIndex > 0)
        {
          for (int ii = lTaskIndex; ii > 0; --ii)
          {
            Parent.mChildren[ii] = Parent.mChildren[ii - 1];
          }
          Parent.mChildren[0] = this;
          OnChange(true);
        }
      }*/
    }

    public void SendToBottom()
    {
      lock (TopNode)
      {
        long lMin = TopNode.AllChildTasks.Min(T => T.Priority);
        this.Priority = lMin - 1000;
      }
 /*     lock (this)
      {
        int lTaskIndex = Parent.mChildren.IndexOf(this);

        if (lTaskIndex >= 0 && lTaskIndex < Parent.mChildren.Count - 1)
        {
          for (int ii = lTaskIndex; ii < Parent.mChildren.Count - 1; ++ii)
          {
            Parent.mChildren[ii] = Parent.mChildren[ii + 1];
          }
          Parent.mChildren[Parent.mChildren.Count - 1] = this;
          OnChange(true);
        }
      }*/
    }

    public bool IsTSA { get; set; }

    [XmlIgnore]
    public string SearchPriority
    {
        get
        {
            var type = IsTSA ? "3" : (Complete ? "2" : (Deleted ? "1" : "4"));
            var activity = TimePeriods.Any() ? TimePeriods.Max(t => t.End).ToString("yyyyMMddHHmm") : "";

            return (type + activity);
        }
    }

      // search for autocomplete etc.
    public bool Matches(string text)
    {
        return fragmentMatches(text, Description)
            || fragmentMatches(text, TimesheetCode);
    }

    private bool fragmentMatches(string searchTerm, string target)
    {
        if (target == null) return false;

        var parts = searchTerm.Split(' ');

        int startIndex = 0;

        foreach (string part in parts)
        {
            var start = target.IndexOf(part, startIndex, StringComparison.InvariantCultureIgnoreCase);

            if (start < 0)
            {
                return false;
            }

            startIndex = start + part.Length;
        }

        return true;
    }

    #endregion

    #region Private helper functions

    protected void TrimEmptyTsaTasks()
    {
      //Children.RemoveAll(t => t.IsTSA && !t.TimePeriods.Any());
    }

    protected bool LockedForUpdate
    {
      get { return mLockedForUpdate || (mParent != null && mParent.LockedForUpdate); }
    }

    public void OnChange()
    {
      OnChange(true);
    }

    public virtual void OnChange(bool xiRedraw)
    {
      if (Change != null)
      {
        if (LockedForUpdate)
        {
          mChangedWhileLocked = true;
        }
        else
        {
          Change(this, xiRedraw);
          if (Parent != null)
          {
            Parent.OnChange(xiRedraw);
          }
        }
      }
      archived = null;
    }

    private TaskProperty[] PropertiesFromDictionary()
    {
      int ii = 0;
      TaskProperty[] lRet = new TaskProperty[mProperties.Count];

      foreach (KeyValuePair<string, string> lEntry in mProperties)
      {
        lRet[ii] = new TaskProperty();
        lRet[ii].Name = lEntry.Key;
        lRet[ii++].Value = lEntry.Value;
      }

      return lRet;
    }

    private void PropertiesToDictionary(TaskProperty[] xiProperties)
    {
      mProperties = new Dictionary<string, string>();

      foreach (TaskProperty lProperty in xiProperties)
      {
        mProperties.Add(lProperty.Name, lProperty.Value);
      }
    }

    #endregion

    #region Events and delegates

    public delegate void ChangeDelegate(Task sender, bool xiRedraw);

    public event ChangeDelegate Change;

    #endregion

    #region Child classes

    public struct TaskProperty
    {
      public string Name;
      public string Value;
    }

    #endregion

  }

}